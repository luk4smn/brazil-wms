<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_list', function (Blueprint $table) {
            $table->id();
            $table->string('local');
            $table->unsignedTinyInteger('status')->default(\App\Entities\SupplyList::STATUS_PENDING);
            $table->unsignedBigInteger('separator_id')->nullable();
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supply_list', function (Blueprint $table) {
            //
        });
    }
}
