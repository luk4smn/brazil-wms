<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->string('moviment_order');
            $table->string('nfe');
            $table->string('cod_provider');
            $table->string('name_provider')->nullable();
            $table->string('status_argos')->nullable();
            $table->unsignedDouble('total')->nullable();
            $table->dateTime('input_date')->nullable();
            $table->unsignedBigInteger('recount_id')->nullable()->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
