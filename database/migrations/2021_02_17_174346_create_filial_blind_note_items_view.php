<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFilialBlindNoteItemsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW [dbo].[filial_blind_note_items] AS
            SELECT
                RTRIM(ORDEMMOVIMENTO) COLLATE Latin1_General_CI_AS AS ordem_movimento,
                RTRIM(CODIGOMATERIAL) COLLATE Latin1_General_CI_AS AS cod_mat,
                RTRIM(QUANTIDADE) COLLATE Latin1_General_CI_AS AS quantidade,
                RTRIM(QUANTIDADEFATURADA) COLLATE Latin1_General_CI_AS AS quantidade_faturada,
                RTRIM(UNIDADE) COLLATE Latin1_General_CI_AS AS unidade,
                FATOR AS fator
            FROM
                Brazil_Inform.dbo.MOVIMENTOITEM
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW [dbo].[filial_blind_note_items]');
    }
}
