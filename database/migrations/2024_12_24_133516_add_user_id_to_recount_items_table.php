<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToRecountItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('recount_items', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->after('recount_id');
        });

        Schema::table('filial_recount_items', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->after('recount_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('recount_items', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('filial_recount_items', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
