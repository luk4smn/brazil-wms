<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProductCodebarView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW [dbo].[product_codebar] AS
            SELECT
                cod.CODIGOMATERIAL COLLATE Latin1_General_CI_AS AS cod_mat,
                cod.CODIGOBARRA COLLATE Latin1_General_CI_AS AS cod_bar,
                cod.STATUS COLLATE Latin1_General_CI_AS AS status
            FROM
                Brazil_Inform.dbo.MATERIALCODIGOSBARRA AS cod"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::statement('DROP VIEW [dbo].[product_codebar]');
    }
}
