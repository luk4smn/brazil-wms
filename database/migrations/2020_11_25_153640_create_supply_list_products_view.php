<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSupplyListProductsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement("
            CREATE VIEW [dbo].[supply_list_products] AS
                SELECT
                    A.cod_aux + ', ' + CAST(A.supply_list_id AS varchar(10)) as id,
                    A.cod_aux,
                    SUM(A.quantity) as quantity,
                    A.supply_list_id,
                    MAX(A.id) as last_insert_id
                FROM
                    supply_list_items as A
                WHERE
                    A.deleted_at is null
                GROUP BY
                    A.cod_aux, A.supply_list_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::statement('DROP VIEW [dbo].[supply_list_products]');
    }
}
