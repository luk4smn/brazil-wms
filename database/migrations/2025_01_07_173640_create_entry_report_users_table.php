<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryReportUsersTable extends Migration
{
    public function up(): void
    {
        Schema::create('entry_report_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entry_report_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedTinyInteger('role');
            $table->timestamps();

            $table->foreign('entry_report_id')->references('id')->on('entry_reports')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('entry_report_users');
    }
}
