<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBlindNoteView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW [dbo].[blind_note] AS
            SELECT
               RTRIM(mov.ORDEMMOVIMENTO) COLLATE Latin1_General_CI_AS as ordem_movimento,
               RTRIM(mov.NUMEROMOVIMENTO) COLLATE Latin1_General_CI_AS as numero_movimento,
               mov.DATA as data_entrada,
               RTRIM(mov.STATUS) COLLATE Latin1_General_CI_AS as status_argos,
               RTRIM(mov.CODIGOFILIALCONTABIL) COLLATE Latin1_General_CI_AS as cod_filial,
               RTRIM(mov.CODIGOPARCEIRO) COLLATE Latin1_General_CI_AS as cod_fornecedor,
               RTRIM(CADPARCEIRO.DESCRICAO) COLLATE Latin1_General_CI_AS AS nome_fornecedor,
               RTRIM(CADOPERACAO.DESCRICAO) as operacao,
               RTRIM(mov.CAMPOLIVREA1) COLLATE Latin1_General_CI_AS as status_wms,
               mov.TOTALLIQUIDO as total
            FROM
               Brazil_Inform.dbo.MOVIMENTO as mov
                  LEFT JOIN Brazil_Inform.dbo.MOVIMENTONFE
                 ON mov.ORDEMMOVIMENTO = Brazil_Inform.dbo.MOVIMENTONFE.ORDEMMOVIMENTO
                  LEFT JOIN Brazil_Inform.dbo.MANIFESTOXML
                 ON Brazil_Inform.dbo.MOVIMENTONFE.CHAVE = Brazil_Inform.dbo.MANIFESTOXML.CHAVE
                  INNER JOIN Brazil_Inform.dbo.CADPARCEIRO
                 ON mov.CODIGOPARCEIRO = Brazil_Inform.dbo.CADPARCEIRO.CODIGOPARCEIRO
                  INNER JOIN Brazil_Inform.dbo.CADFILIALCONTABIL
                 ON mov.CODIGOFILIALCONTABIL = Brazil_Inform.dbo.CADFILIALCONTABIL.CODIGOFILIALCONTABIL
                  INNER JOIN Brazil_Inform.dbo.CADOPERACAO
                 ON mov.CODIGOOPERACAO = Brazil_Inform.dbo.CADOPERACAO.CODIGOOPERACAO
            WHERE
               (
               mov.CodigoOperacao  in (1104, 6104, 8104)
               AND
               mov.STATUS = 'N'
               )
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW [dbo].[blind_note]');
    }
}
