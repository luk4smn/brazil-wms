<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->string('table_name'); // Nome da tabela de origem
            $table->unsignedBigInteger('record_id'); // ID do registro alterado
            $table->string('action'); // Tipo de ação: create, update, delete
            $table->text('changes')->nullable(); // Alterações realizadas (JSON com valores antigos/novos)
            $table->string('obs')->nullable();
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('audits');
    }
}
