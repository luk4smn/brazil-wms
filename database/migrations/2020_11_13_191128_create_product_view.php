<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProductView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW [dbo].[product] AS
            SELECT
                RTRIM(mat.CODIGOMATERIAL) COLLATE Latin1_General_CI_AS AS cod_mat,
                RTRIM(mat.CODIGOAUXILIAR) COLLATE Latin1_General_CI_AS AS cod_aux,
                mat.DESCRICAO COLLATE Latin1_General_CI_AS AS descricao,
                RTRIM(mat.UNIDADEENTRADA) COLLATE Latin1_General_CI_AS AS unidade_entrada,
                RTRIM(mat.UNIDADESAIDA) COLLATE Latin1_General_CI_AS AS unidade_saida,
                COALESCE (une.FATOR / uns.FATOR, 1) AS fator_saida,
                RTRIM(mat.APLICACAO) COLLATE Latin1_General_CI_AS AS referencia,
                (SELECT TOP (1) CODIGOPARCEIRO FROM Brazil_Inform.dbo.MATERIALPARCEIRO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOPARCEIRO NOT IN ('1002615', '2000000', '1001455')) ORDER BY DATAULTIMAENTRADA DESC) AS cod_fornecedor,
                (SELECT TOP (1) A.DESCRICAO FROM Brazil_Inform.dbo.CADPARCEIRO AS A LEFT OUTER JOIN Brazil_Inform.dbo.MATERIALPARCEIRO AS B ON mat.CODIGOMATERIAL = B.CODIGOMATERIAL WHERE (B.CODIGOPARCEIRO = A.CODIGOPARCEIRO) AND (A.CODIGOPARCEIRO NOT IN ('1002615', '2000000', '1001455'))) AS nome_fornecedor,
                (SELECT MAX(A.DISPONIVEL1 / C.FATOR) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A INNER JOIN Brazil_Inform.dbo.CADMATERIAL AS B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN Brazil_Inform.dbo.CADUNIDADE AS C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE (A.CODIGOMATERIAL = mat.CODIGOMATERIAL)) AS estoque_total,
                (SELECT MAX(A.DISPONIVEL1 / C.FATOR) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A INNER JOIN Brazil_Inform.dbo.CADMATERIAL AS B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN Brazil_Inform.dbo.CADUNIDADE AS C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE (A.CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (A.CODIGOALMOXARIFADO = 00001)) AS estoque_matriz,
                (SELECT MAX(DISPONIVEL1) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOALMOXARIFADO = 00003)) AS estoque_inartel,
                (SELECT MAX(DISPONIVEL1) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOALMOXARIFADO IN (00011, 00012, 00015))) AS estoque_bayeux,
                (SELECT MAX(DISPONIVEL1) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOALMOXARIFADO IN (10001, 10010, 10019))) AS estoque_jp,
                (SELECT MAX(DISPONIVEL1) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOALMOXARIFADO IN (04002))) AS estoque_loja_avaria,
                (SELECT MAX(DISPONIVEL1) AS Expr1 FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO AS A WHERE (CODIGOMATERIAL = mat.CODIGOMATERIAL) AND (CODIGOALMOXARIFADO IN (10002))) AS estoque_bayeux_jp,
                (SELECT A.PRECOVENDA1 FROM Brazil_Inform.dbo.TABELAPRECOITEM AS A INNER JOIN Brazil_Inform.dbo.CADMATERIAL AS B ON A.CODIGOTABELAPRECOITEM = B.CODIGOTABELAPRECOITEM WHERE (A.CODIGOTABELAPRECO = '01001') AND (B.CODIGOMATERIAL = mat.CODIGOMATERIAL)) AS preco_venda,
                CAST(mat.CAMPOLIVREA1 AS VARCHAR(250)) AS user_field1,
                CAST(mat.CAMPOLIVREA2 AS VARCHAR(250)) AS user_field2,
                CAST(mat.campolivren1 AS VARCHAR(250)) AS user_field3
            FROM
                Brazil_Inform.dbo.CADMATERIAL AS mat
            LEFT OUTER JOIN
                Brazil_Inform.dbo.CADUNIDADE AS une ON une.NOMENCLATURA = mat.UNIDADEENTRADA
            LEFT OUTER JOIN
                 Brazil_Inform.dbo.CADUNIDADE AS uns ON uns.NOMENCLATURA = mat.UNIDADESAIDA
            WHERE
               (mat.NIVEL = 9)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::statement('DROP VIEW [dbo].[product]');
    }
}
