<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilialNoteItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filial_note_items', function (Blueprint $table) {
            $table->id();

            $table->string('moviment_order');
            $table->string('cod_mat');
            $table->string('unity');
            $table->unsignedBigInteger('quantity')->nullable();
            $table->unsignedBigInteger('quantity_nfe')->nullable();
            $table->unsignedMediumInteger('factor')->nullable();
            $table->unsignedBigInteger('note_id');

            $table->foreign('note_id')->references('id')->on('filial_notes');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filial_note_items');
    }
}
