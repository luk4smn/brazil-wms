<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('recount_id');
        });

        Schema::table('filial_notes', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('recount_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });

        Schema::table('filial_notes', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
