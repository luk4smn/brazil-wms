<?php

use App\Entities\EntryReportItems;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryReportItemsTable extends Migration
{
    public function up(): void
    {
        Schema::create('entry_report_items', function (Blueprint $table) {
            $table->id();
            $table->string('cod_mat');
            $table->string('cod_aux')->nullable();
            $table->unsignedTinyInteger('status')->default(EntryReportItems::STATUS_NOT_FOUND);
            $table->string('local')->nullable();
            $table->unsignedBigInteger('entry_report_id');
            $table->unsignedBigInteger('separator_id')->nullable();
            $table->unsignedBigInteger('receiver_id')->nullable();
            $table->timestamps();

            $table->foreign('entry_report_id')->references('id')->on('entry_reports')->onDelete('cascade');
            $table->foreign('separator_id')->references('id')->on('users')->onDelete('NO ACTION');
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('NO ACTION');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('entry_report_items');
    }
}
