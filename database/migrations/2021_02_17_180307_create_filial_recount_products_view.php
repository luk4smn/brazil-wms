<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFilialRecountProductsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW [dbo].[filial_recount_products] AS
            SELECT
                A.cod_mat + ', ' + CAST(B.recount_id AS varchar(10)) as id,
                A.cod_mat,
                C.cod_aux,
                SUM(A.quantity)  as quantity,
                B.recount_id,
                C.descricao as description,
                C.referencia as reference,
                A.unity,
                C.unidade_saida as unity_exit,
                C.estoque_total,
                C.estoque_matriz,
                C.estoque_inartel,
                C.estoque_bayeux,
                C.estoque_bayeux_jp,
                C.estoque_loja_avaria,
                C.estoque_jp,
                (
                    SELECT
                        SUM(D.quantity_confer)
                    FROM
                        filial_recount_items D
                    WHERE
                        D.recount_id = B.recount_id
                    AND
                        D.cod_mat = A.cod_mat
                ) AS total_quantity_confer,
                (
                    SELECT
                        SUM(D.quantity_damage)
                    FROM
                        filial_recount_items D
                    WHERE
                        D.recount_id = B.recount_id
                    AND
                        D.cod_mat = A.cod_mat
                ) AS total_quantity_damage,
                (
                    SELECT
                        SUM(D.volumes)
                    FROM
                        filial_recount_items D
                    WHERE
                        D.recount_id = B.recount_id
                    AND
                        D.cod_mat = A.cod_mat
                ) AS total_volumes,
                (
				    SELECT
				    STUFF((
				        SELECT DISTINCT ', ' + CAST(D.user_id AS NVARCHAR(MAX))
				        FROM filial_recount_items D
				        WHERE D.recount_id = B.recount_id AND D.cod_mat = A.cod_mat
				        FOR XML PATH(''), TYPE
				    ).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
				) AS user_ids
            FROM filial_note_items as A
                left join filial_notes as B on A.note_id = B.id
                inner join filial_recounts on B.recount_id = filial_recounts.id
                left outer join product as C on A.cod_mat = C.cod_mat
            WHERE
                A.deleted_at is null
            GROUP BY
                A.cod_mat,
                C.cod_aux,
                B.recount_id,
                C.descricao,
                C.referencia,
                A.unity,
                C.unidade_saida,
                C.estoque_total,
                C.estoque_matriz,
                C.estoque_inartel,
                C.estoque_bayeux,
                C.estoque_bayeux_jp,
                C.estoque_loja_avaria,
                C.estoque_jp
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::statement('DROP VIEW [dbo].[filial_recount_products]');
    }
}
