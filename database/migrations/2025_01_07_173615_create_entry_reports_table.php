<?php

use App\Entities\EntryReport;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryReportsTable extends Migration
{
    public function up(): void
    {
        Schema::create('entry_reports', function (Blueprint $table) {
            $table->id();
            $table->string('table');
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->unsignedTinyInteger('status')->default(EntryReport::STATUS_PENDING);
            $table->text('observation')->nullable();
            $table->unsignedTinyInteger('type');
            $table->timestamps();

            $table->index(['table', 'reference_id'], 'entry_reports_table_reference_index');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('entry_reports');
    }
}
