<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToSupplyListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('supply_list', function (Blueprint $table) {
            $table->string('notes')->nullable()->after('status');
            $table->timestamp('initiated_at')->nullable()->after('updated_at');
            $table->timestamp('completed_at')->nullable()->after('initiated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('supply_list', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->dropColumn('initiated_at');
            $table->dropColumn('completed_at');
        });
    }
}
