<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFilialRecountUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Criar a tabela de relacionamento
        Schema::create('filial_recount_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recount_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('recount_id')->references('id')->on('filial_recounts')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        // Migrar dados da coluna user_id da tabela filial_recounts para a nova tabela
        // Transformar os dados em um formato adequado para a inserção
        // Inserir os dados de 1000 em 1000 na nova tabela
        $recounts = DB::table('filial_recounts')->select('id as recount_id', 'user_id')->get();
        $data = [];
        foreach ($recounts as $recount) {
            $data[] = [
                'recount_id' => $recount->recount_id,
                'user_id' => $recount->user_id,
            ];
        }
        $batchSize = 1000;
        foreach (array_chunk($data, $batchSize) as $batch) {
            DB::table('filial_recount_users')->insert($batch);
        }

        //Criar Gatilho
        DB::statement("CREATE TRIGGER trg_AfterInsert_FilialRecounts
            ON [brazilwms].[dbo].[filial_recounts]
            AFTER INSERT
            AS
            BEGIN
                INSERT INTO brazilwms.dbo.filial_recount_users (recount_id, user_id)
                SELECT id, user_id FROM inserted;
            END
        ");
    }

    public function down()
    {
        Schema::dropIfExists('filial_recount_users');

        // Dropar o gatilho (trigger)
        DB::statement('DROP TRIGGER IF EXISTS trg_AfterInsert_FilialRecounts');
    }
}
