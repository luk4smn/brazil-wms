<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecountItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recount_items', function (Blueprint $table) {
            $table->id();

            $table->string('cod_mat');
            $table->unsignedMediumInteger('volumes')->nullable();
            $table->unsignedBigInteger('quantity_confer')->nullable();
            $table->unsignedBigInteger('quantity_damage')->nullable();
            $table->unsignedBigInteger('recount_id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recount_items');
    }
}
