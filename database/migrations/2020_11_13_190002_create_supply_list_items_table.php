<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyListItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_list_items', function (Blueprint $table) {
            $table->id();
            $table->string('cod_aux');
            $table->unsignedBigInteger('quantity');
            $table->unsignedBigInteger('supply_list_id');

            $table->foreign('supply_list_id')->references('id')->on('supply_list');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_list_items');
    }
}
