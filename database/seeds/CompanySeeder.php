<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name'          => 'WL COMERCIO E IMPORTACAO LTDA',
            'fantasy_name'  => 'BRAZIL ATACADO',
            'cnpj'          => '01630115000122',
            'created_at'    => now()
        ]);

        DB::table('companies')->insert([
            'name'          => 'BRAZIL JP COMERCIO E IMPORTACAO DE ARTIG',
            'fantasy_name'  => 'BRAZIL ATACADO JP',
            'cnpj'          => '41103608000129',
            'created_at'    => now()
        ]);
    }

}
