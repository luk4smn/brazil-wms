<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'Lucas Nunes',
            'email'         => 'ti@cirne.com.br',
            'password'      => bcrypt('.brazil..'),
            'local_login'   => 'admin',
            'local_password' => 'admin',
            'admin'         => true,
            'created_at'    => now()
        ]);
    }
}
