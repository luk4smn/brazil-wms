1 - Ajuste view de produtos
2 - Exporte as migrations (php artisan migrate)
3 - Mude no .env o QUEUE_CONNECTION para database
4 - Criar o cron para o scheduler do laravel
    * * * * * php /caminho/para/sua/aplicacao/artisan schedule:run >> /dev/null 2>&1


Verificar ou editar Cron Jobs para Todos os Usuários
sudo cat /etc/crontab

Editar o Crontab do Usuário
crontab -e

5 - Verificar a formatação de data, se for o caso remover da Classe de User e da Classe Entity
6 - Trocar apache por Nginx
7 - Verificar cron do projeto CMS
