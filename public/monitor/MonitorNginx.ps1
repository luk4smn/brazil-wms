# Caminho do log do monitoramento
$monitorLogPath = "C:\laragon\logs\monitor.log"
# Caminho do log de erros
$errorLogPath = "C:\laragon\logs\error.log"
# �ltima data de limpeza do monitor.log (escopo do script)
$lastLogCleanDate = $null
# Intervalo de verificacao em segundos
$interval = 60
# URL para testar a API
$testUrl = "http://localhost/estoque/api"
# Contagem de falhas consecutivas
$failureCount = 0
# Limite máximo de falhas consecutivas antes de interromper o script
$maxFailures = 100

# Função para garantir que o diretório de logs exista
function Ensure-LogDirectory {
    $logDirectory = [System.IO.Path]::GetDirectoryName($monitorLogPath)
    if (-not (Test-Path -Path $logDirectory)) {
        Write-Host "Diretório de logs não encontrado. Criando: $logDirectory"
        New-Item -Path $logDirectory -ItemType Directory -Force
    }
}

# Função para registrar logs
function Write-Log {
    param ([string]$message)
    Ensure-LogDirectory
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    "$timestamp - $message" | Out-File -FilePath $monitorLogPath -Append
}

# Função para registrar erros
function Write-ErrorLog {
    param ([string]$errorMessage)
    Ensure-LogDirectory
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    "$timestamp - ERROR: $errorMessage" | Out-File -FilePath $errorLogPath -Append
}

# Fun��o para limpar o monitor.log diariamente
function Clean-MonitorLog {
    $currentDate = Get-Date -Format "yyyy-MM-dd"

    if ($lastLogCleanDate -ne $currentDate) {
        try {
            Write-Log "Limpando o arquivo de log: $monitorLogPath"

            # Fecha o arquivo se estiver aberto por algum processo
            $openProcesses = Get-Process | Where-Object {
                $_.Modules | Where-Object { $_.FileName -eq $monitorLogPath } -ErrorAction SilentlyContinue
            }

            foreach ($process in $openProcesses) {
                Stop-Process -Id $process.Id -Force -ErrorAction SilentlyContinue
                Write-Log "Processo ${process.ProcessName} (PID: $($process.Id)) que estava usando o arquivo foi finalizado."
            }

            # Recria o arquivo vazio
            Remove-Item -Path $monitorLogPath -Force -ErrorAction SilentlyContinue
            New-Item -Path $monitorLogPath -ItemType File -Force | Out-Null

            $script:lastLogCleanDate = $currentDate
            Write-Log "Arquivo de log limpo e recriado com sucesso."
        } catch {
            Write-ErrorLog "Erro ao limpar o arquivo de log: $_.Exception.Message"
        }
    }
}

# Função para verificar a API
function Check-API {
    param ([string]$url)
    try {
        Write-Log "Iniciando verificação da API em $url"
        $response = Invoke-WebRequest -Uri $url -UseBasicParsing -TimeoutSec 5
        if ($response.StatusCode -eq 200) {
            Write-Log "API respondeu com sucesso: $($response.StatusCode)"
            return $true
        } else {
            Write-Log "API respondeu com erro: $($response.StatusCode)"
            return $false
        }
    } catch {
        $errorMsg = $_.Exception.Message
        Write-Log "Erro ao verificar a API: ${errorMsg}"
        Write-ErrorLog "Erro ao verificar a API: ${errorMsg}"
        return $false
    }
}

# Função para verificar e iniciar o MySQL se necessário
function Check-MySQL {
    $mysqlPort = 3306

    try {
        # Verifica se a porta está em uso
        $portInUse = netstat -ano | Select-String ":$mysqlPort"
        if ($portInUse) {
            Write-Log "MySQL está em execução na porta ${mysqlPort}."
        } else {
            Write-Log "MySQL não está em execução. Iniciando serviço..."

            & "C:\laragon\laragon.exe" reload mysql
            Write-Log "MySQL iniciado com sucesso."
        }
    } catch {
        $errorMsg = $_.Exception.Message
        Write-Log "Erro ao verificar ou iniciar o MySQL: ${errorMsg}"
        Write-ErrorLog "Erro ao verificar ou iniciar o MySQL: ${errorMsg}"
    }
}

# Função para reiniciar os serviços e registrar PIDs
function Restart-Services {
    $services = @("laragon", "nginx")

    foreach ($service in $services) {
        try {
            Write-Log "Verificando processos em execução para $service..."

            # Identifica e finaliza os processos em execução
            $processes = Get-Process -ErrorAction SilentlyContinue | Where-Object { $_.Name -eq $service }
            foreach ($process in $processes) {
                Stop-Process -Id $process.Id -Force
                Write-Log "Processo $service (PID: $($process.Id)) finalizado."
            }

            if($service -eq "nginx"){
                # Reinicia o serviço diretamente
                Write-Log "Iniciando $service..."

                & "C:\laragon\laragon.exe" reload $service

                # Verifica e registra o novo PID do serviço
                Start-Sleep -Seconds 2
                $newProcess = Get-Process -ErrorAction SilentlyContinue | Where-Object { $_.Name -eq $service }
                foreach ($process in $newProcess) {
                    Write-Log "Novo processo $service iniciado com PID: $($process.Id)"
                }
            }


        } catch {
            $errorMsg = $_.Exception.Message
            Write-Log "Erro ao reiniciar ${service}: ${errorMsg}"
            Write-ErrorLog "Erro ao reiniciar ${service}: ${errorMsg}"
        }
    }
}

# Verificação contínua
try {
    Write-Log "Monitoramento iniciado. Verificando o status da API em ${testUrl}..."

    while ($true) {
		# Limpa o monitor.log diariamente
        Clean-MonitorLog

		# Chamada de verificação da API
        Write-Log "Iniciando nova verificação da API..."
        $apiStatus = Check-API -url $testUrl
        if (-not $apiStatus) {
            $failureCount++
            Write-Log "Falha consecutiva na API: $failureCount de $maxFailures"

            if ($failureCount -ge $maxFailures) {
                Write-ErrorLog "A API continua indisponível após $failureCount tentativas consecutivas. Verificação manual necessária."
                break
            }

            Write-Log "API indisponível. Executando reinicialização..."
            Restart-Services
            Check-MySQL
        } else {
            $failureCount = 0
            Write-Log "API está funcionando corretamente."
        }

        Start-Sleep -Seconds $interval
    }
} catch {
    $errorMsg = $_.Exception.Message
    Write-ErrorLog "Erro fatal no script: ${errorMsg}"
    Write-Log "Monitoramento encerrado devido a erro fatal."
}
