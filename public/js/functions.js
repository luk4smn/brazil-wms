//  JQUERY FUNCTIONS BY LUK4SMN
$(document).ready(function () {

    //ACAO DE LOGOUT
    $('.logout').on('click', function (e) {
        e.preventDefault();
        swal({
            title               : "Deseja fazer logout ?",
            type                : "question",
            confirmButtonColor  : "red",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                document.getElementById('logout-form').submit();
            }
        }.bind(this));
    });

    //IMPORTAR NFE DE ENTRADA
    $('.do-request').on('click', function (e) {
        e.preventDefault();

        let message = $(this).attr('data-message')
        let url = $(this).attr('data-action')
        let requestType = $(this).attr('data-request-type') ?? "DELETE"
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : message,
            type                : "question",
            confirmButtonColor  : "blue",
            confirmButtonText   : "Sim",
            cancelButtonColor   : "red",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: requestType,
                    dataType: "JSON",
                    beforeSend: function() {
                        swal.fire({
                            html: '<h5>Aguarde...</h5>',
                            showConfirmButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        });
                    },
                    success: function (response)
                    {
                        swal({
                            title   : response.data.message,
                            text    : response.data.info,
                            type    : response.data.status,
                            timer   : 2000,
                            showConfirmButton: false
                        },setTimeout(function() {
                            window.location.href = redirect;
                        }, 2000))
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                        swal({
                            title   : "Occoreu um erro realizar ação",
                            // text    : xhr.responseText,
                            type    : "error",
                            timer   : 2000,
                            showConfirmButton: false
                        });
                    }
                });

            }
        }.bind(this));
    });

    //GERAR LISTA A PARTIR DE MULTIPLA SELECAO
    $('.mult-list').on('click', function (e) {
        e.preventDefault();
        var checkedIds = [];

        $.each($("input[name='multiples[]']:checked"), function(){
            checkedIds.push($(this).val());
        });

        if(checkedIds.length > 1){
            let server = $(this).attr('data-server')
            let url = $(this).attr('data-action')
            let redirect = $(this).attr('data-redirect')

            swal({
                title               : "Gerar PDF das listas selecionadas?",
                type                : "question",
                confirmButtonColor  : "blue",
                confirmButtonText   : "Sim",
                cancelButtonColor   : "red",
                cancelButtonText    : "Não",
                showCancelButton    : true,
            }).then(function(result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {'data':checkedIds},
                        dataType: "JSON",
                        beforeSend: function() {
                            swal.fire({
                                html: '<h5>Gerando...</h5>',
                                showConfirmButton: false,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                            });
                        },
                        success: function (response)
                        {
                            //window.open(response.data.file_url, "_blank");

                            swal({
                                title   : response.data.message,
                                text    : response.data.info,
                                type    : response.data.status,
                                timer   : 2000,
                                showConfirmButton: false
                            },setTimeout(function() {
                                //window.location.href = response.data.file_url;
                                const iframe = document.getElementById("pdfViewer");
                                iframe.src = response.data.file_url;

                                // Exibe o modal
                                document.getElementById("pdfModal").style.display = "block";
                            }, 2000))

                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                            swal({
                                title   : "Occoreu um erro ao gerar",
                                // text    : xhr.responseText,
                                type    : "error",
                                timer   : 2000,
                                showConfirmButton: false
                            });
                        }
                    });

                }
            }.bind(this));
        }
    });

    //DOWNLOAD DE PDF P/ LISTAS E CONFERFENCIAS COM VARIOS ITENS
    $('.download-big-pdf').on('click', function (e) {
        e.preventDefault();

        let server = $(this).attr('data-server')
        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')
        let request = $(this).attr('data-values')
        let filename = $(this).attr('data-filename')

        swal({
            title               : "Arquivo muito grande para gerar. Deseja fazer download?",
            type                : "question",
            confirmButtonColor  : "blue",
            confirmButtonText   : "Sim",
            cancelButtonColor   : "red",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {'data':request, 'filename':filename},
                    dataType: "JSON",
                    responseType: 'arraybuffer',
                    beforeSend: function() {
                        swal.fire({
                            html: '<h5>Preparando PDF...</h5>',
                            showConfirmButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        });
                    },
                    success: function (response)
                    {
                        //window.open(response.data.file_url, "_blank");

                        // Baixar o arquivo automaticamente
                        const pdfUrl = response.data.file_url;
                        const fileName = filename+".pdf"; // Nome do arquivo baixado
                        const downloadLink = document.createElement("a");
                        downloadLink.setAttribute("href", pdfUrl);
                        downloadLink.setAttribute("download", fileName);

                        // Adiciona o link ao corpo temporariamente, dispara o clique e remove o link
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);

                        swal({
                             title   : response.data.message,
                             text    : response.data.info,
                             type    : response.data.status,
                             timer   : 2000,
                             showConfirmButton: false
                         },setTimeout(function() {
                             //window.location.href = response.data.file_url;

                            const iframe = document.getElementById("pdfViewer");
                            iframe.src = response.data.file_url;

                            // Exibe o modal
                            document.getElementById("pdfModal").style.display = "block";
                         }, 2000))
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                        swal({
                            title   : "Occoreu um erro ao gerar",
                            text    : xhr.responseText,
                            type    : "error",
                            timer   : 2000,
                            showConfirmButton: false
                        });
                    }
                });

            }
        }.bind(this));
    });

    //ACAO DINAMICA P/ BAIXAR CSV
    $('.download-csv').on('click', function (e) {
        e.preventDefault();
        let url = $(this).attr('data-action')
        let type = $(this).attr('data-type')

        if(type === 'list'){
            e.preventDefault();
            let checkedIds = [];

            $.each($("input[name='multiples[]']:checked"), function(){
                checkedIds.push($(this).val());
            });

            if(checkedIds.length > 0 && checkedIds.length < 6){
                swal({
                    title               : "Gerar CSV de listas selecionadas?",
                    type                : "question",
                    confirmButtonColor  : "blue",
                    confirmButtonText   : "Sim",
                    cancelButtonColor   : "red",
                    cancelButtonText    : "Não",
                    showCancelButton    : true,
                }).then(function(result) {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {'data':checkedIds},
                            dataType: "JSON",
                            beforeSend: function() {
                                swal.fire({
                                    html: '<h5>Gerando...</h5>',
                                    showConfirmButton: false,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                });
                            }, error: function(data){
                                if(data.status === 200){
                                    let forma = "data:text/csv;charset=utf-8,";
                                    forma += data.responseText;

                                    let encoda = encodeURI(forma);
                                    let baixa = document.createElement("a");
                                    let fileName = checkedIds.join('-');

                                    baixa.setAttribute("href", encoda);
                                    baixa.setAttribute("id", "downloadcsv");
                                    baixa.setAttribute("download", "Lista "+fileName+".csv");
                                    document.body.appendChild(baixa);
                                    baixa.click();

                                    swal({
                                        title   : "Arquivo gerado",
                                        text    : "Download com sucesso",
                                        type    : "success",
                                        timer   : 2000,
                                        showConfirmButton: false
                                    },setTimeout(function() {
                                    }, 2000))
                                }
                                else{
                                    console.log(data);
                                    swal({
                                        title   : "Occoreu um erro ao gerar",
                                        type    : "error",
                                        timer   : 2000,
                                        showConfirmButton: false
                                    });
                                }
                            }
                        });

                    }
                }.bind(this));
            }
        }else{
            let frame = document.getElementById("download-iframe");
            frame.src = url;
        }
    });

    //CLASSE P/ DESABILITAR ELEMENTOS
    $('.not-active').prop("disabled", true);


    // Quando o botão com 'show-modal-info' for clicado
    $('.show-modal-info').on('click', function(e) {
        e.preventDefault(); // Impede o comportamento padrão

        // Pega os dados dos atributos 'data-*' do botão
        let title = $(this).attr('data-title');
        let identifier = $(this).attr('data-identifier');
        let info = $(this).attr('data-info');

        // Preenche as informações
        $('#infoModalLabel').text(title);
        $('#modalIdentifierText').text(identifier);
        $('#modalInfoText').text(info);

        // Exibe o modal
        $('#infoModal').modal('show');
    });


    // Chamar a função para atualizar os dados
    obterStatusFilas();
});
