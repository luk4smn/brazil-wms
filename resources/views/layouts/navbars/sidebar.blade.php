@php use App\Entities\EntryReport; @endphp
<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            <img src="{{ asset('assets/img/now-logo.png') }}" alt="" width="30" height="30">
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            {{ env('APP_NAME') ?? "Brazil-WMS" }}
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="@if ($activePage == 'home') active @endif">
                <a href="{{ route('home') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>

            <li class="@if ($activePage == 'supply') active @endif">
                <a href="{{ route('supply.index')}}">
                    <i class="now-ui-icons shopping_basket"></i>
                    <p>{{ __('Abastecimento') }}</p>
                </a>
            </li>

            <li class="nav-item dropdown @if ($activePage == 'recounts') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                    <p>{{ __('Conferências') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item"><b>Fornecedor</b></a>
                    <a class="dropdown-item" href="{{ route('notes.index') }}">Notas de Entrada</a>
                    <a class="dropdown-item" href="{{ route('recounts.index') }}">Controle de Conferências</a>
                    <hr>
                    <a class="dropdown-item"><b>Entre Filiais</b></a>
                    <a class="dropdown-item" href="{{ route('filial-notes.index') }}">Notas de Entrada</a>
                    <a class="dropdown-item" href="{{ route('filial-recounts.index') }}">Controle de Conferências</a>
                </div>
            </li>

            <li class="nav-item dropdown @if ($activePage == 'entry_report') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="now-ui-icons shopping_box"></i>
                    <p>{{ __('Relatórios de Entrada') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item"><b>Tipos de relatórios</b></a>
                    <a class="dropdown-item"
                       href="{{ route('entry-reports.index', ['type' => EntryReport::TYPE_ESTOQUE]) }}">Separação de
                        estoque</a>
                    <a class="dropdown-item"
                       href="{{ route('entry-reports.index', ['type' => EntryReport::TYPE_LOJA]) }}">Recebimento
                        em loja</a>
                </div>
            </li>

            <li class="@if ($activePage == 'audits') active @endif">
                <a href="{{ route('audits.index')}}">
                    <i class="now-ui-icons business_badge"></i>
                    <p>{{ __('Auditoria') }}</p>
                </a>
            </li>

            @if(auth()->user()->isAdmin() ?? false)
                <li class="@if ($activePage == 'users') active @endif">
                    <a href="{{ route('users.index') }}">
                        <i class="now-ui-icons users_circle-08"></i>
                        <p>{{ __('Usuários') }}</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
