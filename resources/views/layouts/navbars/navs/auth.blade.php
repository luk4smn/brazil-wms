<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
  <div class="container-fluid">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navigation">
      <form action="{{ $searchRoute ?? route('home') }}">
          @csrf
        <div class="input-group no-border">
          <input type="text" name="q" class="form-control" placeholder="{{ 'Buscar...' }}">
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="now-ui-icons ui-1_zoom-bold"></i>
            </div>
          </div>
        </div>
      </form>

        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bell"></i>
                    <span class="badge rounded-pill badge-notification bg-danger" id="total_queue_count">0</span>
                </a>

                <div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton" id="dynamicDropdown">
                    <a class="dropdown-item" href="#"></a>
                </div>
            </li>

        </ul>


      <ul class="navbar-nav">
        <li class="nav-item dropdown">
            @if($agent->isMobile())
                @include('layouts.navbars.navs.sidebar-mobile')

            @else
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>
                        <span class="d-lg-none d-md-block">{{ __("Account") }}</span>
                    </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('profile.edit') }}">
                        {{ __("Meu perfil") }}
                    </a>
                    <a class="logout dropdown-item" href="#">
                        {{ __('Logout') }}
                    </a>
                </div>
            @endif
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->

<script>
    let lastUpdateTimestamp = 0;

    function obterStatusFilas() {
        const url = "{{ route('queue.updates') }}";
        const dropdown = document.getElementById('dynamicDropdown');
        let totalCount = 0;

        $.get(url, { last_update: lastUpdateTimestamp }, function(response) {
            dropdown.innerHTML = ''; // Limpar o dropdown se houve atualização

            $.each(response.data, function(index, fila) {
                let statusText = (fila.count === 0) ? 'Fila vazia' : 'Em processamento: ' + fila.count;
                const menuItem = document.createElement('li');
                menuItem.innerHTML = `<a class="dropdown-item" href="#"><b>${fila.queue}</b> - ${statusText}</a>`;
                dropdown.appendChild(menuItem);

                totalCount += fila.count;
            });

            document.getElementById('total_queue_count').textContent = totalCount;
            lastUpdateTimestamp = response.timestamp; // Atualizar o timestamp

            // Repetir a chamada para manter o long polling ativo após 10 minutos
            setTimeout(obterStatusFilas, 600000);
        }).fail(function() {
            // Tentar novamente em caso de falha após 1 minuto
            setTimeout(obterStatusFilas, 60000);
        });
    }
</script>
