@php use Jenssegers\Agent\Agent; @endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>
        {{ $namePage ?? 'Dashboard' }} | {{ env('APP_NAME') }}
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Files -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/now-ui-dashboard.css') }}?v=1.3.0" rel="stylesheet"/>
    <link href="{{ asset('assets/css/fileinput.min.css') }}?v=1.3.0" rel="stylesheet"/>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    {{--    <!-- APP CSS -->     --}}
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    {{------------------------ meta for vue js forms -------------------------------------}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet"/>
</head>

<body class="{{ $class ?? '' }}">
{{--  resolution agent  --}}
@php
    $agent = new Agent();
@endphp

<div class="wrapper">
    @auth
        @include('layouts.page_template.auth')
    @endauth
    @guest
        @include('layouts.page_template.guest')
    @endguest
    @include('sweetalert::alert')
</div>

<iframe id="download-iframe" style="display:none"></iframe>

{{--  substituir em caso de erro--}}
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
{{--<script src="{{ asset('assets/demo/demo.js') }}"></script>--}}
{{--  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--}}
{{--  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>--}}
<!--   Core JS Files   -->
<script src="{{ asset('assets/js/core/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
{{--  <script src="{{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>--}}

<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('assets/js/now-ui-dashboard.min.js') }}?v=1.3.0" type="text/javascript"></script>

{{-- Funções adicionais --}}
<script src="{{ asset('js/functions.js') }}"></script>
<script src="{{ asset('js/charts.js') }}"></script>
<script src="{{ asset('js/background.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert.all.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('assets/js/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

{{--  <script src="{{ asset('assets/js/app.js') }}"></script>--}}

@stack('js')
</body>

</html>
