<footer class="footer navbar-fixed-bottom">
  <div class=" container-fluid ">
    <div class="copyright" id="copyright">
      &copy;
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>, {{__(" Designed and coded by")}}
      <a href="https://www.linkedin.com/in/luk4smn/" target="_blank">{{__(" luk4smn")}}</a>
    </div>
  </div>
</footer>
