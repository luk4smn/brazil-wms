@php use App\Entities\Filial\FilialRecount; @endphp
@extends('layouts.app', [
    'namePage' => 'Controle de Conferências entre Filiais',
    'class' => 'sidebar-mini',
    'activePage' => 'recounts',
    'activeNav' => '',
    'searchRoute' => route('filial-recounts.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Conferências - Filiais</h2>
            <p class="category">Controle de Conferência</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('filial-recounts.index') }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('filial-recounts.index', ['status' => FilialRecount::STATUS_PENDING]) }}">
                            Pendentes
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('filial-recounts.index', ['status' => FilialRecount::STATUS_IN_PROGRESS]) }}">
                            Conferindo
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('filial-recounts.index', ['status' => FilialRecount::STATUS_COMPLETED]) }}">
                            Concluído
                        </a>

                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="table-responsive">
                            <table id="datatable" class="table" cellspacing="0" width="100%">
                                <thead class=" text-primary">
                                <tr class="text-center">
                                    <th><b>#</b></th>
                                    <th><b>Conferente</b></th>
                                    <th><b>Fornecedor</b></th>
                                    <th><b>Empresa</b></th>
                                    <th><b>Itens</b></th>
                                    <th><b>Criado</b></th>
                                    <th><b>Atualizado</b></th>
                                    <th><b>Status</b></th>
                                    <th class="disabled-sorting text-left"><b>{{ __('Ações') }}</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($checks ?? [] as $recount)
                                    <tr class="text-center">
                                        <td style="font-size: small"><b>{{ $recount->id }}</b></td>
                                        <td style="max-width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">{{ $recount->usernames }}</td>
                                        <td style="max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">{{ $recount->provider_names ?? '' }}</td>
                                        <td style="font-size: xx-small">{{ $recount->user->company->fantasy_name ?? '' }}</td>
                                        <td style="font-size: xx-small">{{ $recount->products_count ?? '' }}</td>
                                        <td style="font-size: x-small">{{ $recount->created_at->format('d/m/Y H:i') ?? '' }}</td>
                                        <td style="font-size: x-small">{{ $recount->updated_at ? $recount->updated_at->format('d/m/Y H:i') : "" }}</td>
                                        <td style="font-size: x-small">
                                            @if($recount->status == FilialRecount::STATUS_PENDING)
                                                <span class="alert alert-danger">{{ $recount->status_label }}</span>
                                            @elseif($recount->status == FilialRecount::STATUS_IN_PROGRESS)
                                                <span class="alert alert-warning">{{ $recount->status_label }}</span>
                                            @else
                                                <span class="alert alert-success">{{ $recount->status_label }}</span>
                                            @endif
                                        </td>
                                        <td id="bread-actions" class="pull-left">
                                            @if($recount->status == FilialRecount::STATUS_COMPLETED)
                                                <form class="pull-left"
                                                      action="{{ route('pdf.generate', ['view' => 'filial_recount']) }}"
                                                      method="post" target="_blank">
                                                    @csrf()
                                                    @if($recount->products_count >= 100 )
                                                        <a title="Download"
                                                           class="btn btn-icon btn-sm btn-warning a-center download-big-pdf"
                                                           href="#"
                                                           data-server="{{ env('SERVER_URL' ?? '192.168.0.17').env('SERVER_IRL' ?? '/estoque') }}"
                                                           data-action="{{ route('pdf.download', ['view' => 'filial_recount']) }}"
                                                           data-redirect="{{ route('filial-recounts.index') }}"
                                                           data-filename="{{ 'filial_recount' }}"
                                                           data-values='{{ $recount->id }}'
                                                        >
                                                            {!! '🖨️' !!}
                                                        </a>
                                                    @else
                                                        <input id="data" name="data" type="hidden" value='{{ $recount->id }}'>

                                                        <button title="Gerar PDF" class="btn btn-icon btn-sm btn-warning"
                                                                type="submit">
                                                            {!! '🖨️' !!}
                                                        </button>
                                                    @endif
                                                    <span style="color: white; font-size: xx-small">.</span>
                                                </form>

                                                <!-- Botão para abrir o modal -->
                                                <a href="#" class="btn btn-dark btn-icon btn-sm a-center show-modal-info"
                                                   data-toggle="modal"
                                                   data-target="#infoModal"
                                                   data-identifier="{{ "# $recount->id" }}"
                                                   data-info="{{ "Nota(s) :\n $recount->nfes" }}"
                                                   data-title="Informações Extras da Conferência - (Filiais)">
                                                    {!! '🔍️' !!}
                                                </a>

                                                <a title="CSV" class="btn btn-success btn-icon btn-sm a-center download-csv"
                                                   href="#"
                                                   data-action="{{ route('csv.export-recount', ['type' => 'filial-recount', 'id' => $recount->id]) }}"
                                                   data-type="recount"
                                                >
                                                    {!! '📉' !!}
                                                </a>
                                                @if(is_null($recount->entryReport))
                                                    <a title="Transformar" class="btn btn-info btn-icon btn-sm a-center do-request"
                                                       href="#"
                                                       data-message="{{ "Transformar registro em relatório de entrada?" }}"
                                                       data-redirect="{{ route('filial-recounts.index') }}"
                                                       data-request-type="{{ "POST" }}"
                                                       data-action="{{ route('entry-reports.store', ['table' => 'filial_recounts', 'reference_id' => $recount->id]) }}"
                                                    >
                                                        {!! '🔁' !!}
                                                    </a>
                                                @endif
                                            @endif
                                            @if(auth()->user()->isAdmin())
                                                <button title="Delete"
                                                        class="btn btn-icon btn-sm btn-danger pull-center do-request"
                                                        href="#"
                                                        data-action="{{ route('filial-recounts.destroy', $recount->id) }}"
                                                        data-redirect="{{ route('filial-recounts.index') }}"
                                                >
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" align="center">Nenhum dado cadastrado até o momento</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $checks->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <note-items></note-items>
    @include('pdf.iframe')

    @include('info.recount_notes')

@endsection
