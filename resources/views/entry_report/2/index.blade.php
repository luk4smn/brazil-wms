@php use App\Entities\EntryReport;use App\Entities\Recount; @endphp
@extends('layouts.app', [
    'namePage' => 'Relatórios de Entrada',
    'class' => 'sidebar-mini',
    'activePage' => 'entry_report',
    'activeNav' => '',
    'searchRoute' => route('entry-reports.index', ['type' => request()->route('type')]),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Relatórios de Entrada</h2>
            <p class="category">Recebimento em loja</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('entry-reports.index', ['type' => request()->route('type')]) }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('entry-reports.index', ['type' => request()->route('type'), 'status' => EntryReport::STATUS_RECEIVING]) }}">
                            Recebendo
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('entry-reports.index', ['type' => request()->route('type'), 'status' => EntryReport::STATUS_COMPLETED]) }}">
                            Concluído
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('entry-reports.index', ['type' => request()->route('type'), 'status' => EntryReport::STATUS_COMPLETED_WITH_ISSUES]) }}">
                            Concluído c/ pendências
                        </a>

                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="table-responsive">
                            <table id="datatable" class="table">
                                <thead class="text-primary">
                                <tr class="text-center">
                                    <th><b>#</b></th>
                                    <th><b>Conferência</b></th>
                                    <th><b>Tipo</b></th>
                                    <th><b>Fornecedor</b></th>
                                    <th><b>NF-e(s)</b></th>
                                    <th><b>Empresa</b></th>
                                    <th><b>Criado</b></th>
                                    <th><b>Atualizado</b></th>
                                    <th><b>Produtos</b></th>
                                    <th><b>Status</b></th>
                                    <th class="disabled-sorting text-left"><b>{{ __('Ações') }}</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($entryReports ?? [] as $entryReport)
                                    <tr class="text-center">
                                        <td><b>{{ $entryReport->id }}</b></td>
                                        <td>{{ $entryReport->reference_id }}</td>
                                        <td>{{ $entryReport->table_label }}</td>
                                        <td style="max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">{{ $entryReport->related->provider_names ?? '' }}</td>
                                        <td style="font-size: xx-small">{{ $entryReport->related->nfes ?? '' }}</td>
                                        <td style="font-size: xx-small">{{ $entryReport->related->user->company->fantasy_name ?? '' }}</td>
                                        <td style="font-size: x-small">{{ $entryReport->created_at->format('d/m/Y H:i') }}</td>
                                        <td style="font-size: x-small">{{ $entryReport->updated_at ? $entryReport->updated_at->format('d/m/Y H:i') : "" }}</td>
                                        <td style="font-size: x-small">{{ $entryReport->items_count }}</td>
                                        <td style="font-size: x-small">
                                            @if($entryReport->status == EntryReport::STATUS_RECEIVING)
                                                <span
                                                    class="alert alert-info">{{ $entryReport->status_label }}</span>
                                            @elseif($entryReport->status == EntryReport::STATUS_COMPLETED_WITH_ISSUES)
                                                <span
                                                    class="alert alert-danger">{{ $entryReport->status_label }}</span>
                                            @else
                                                <span
                                                    class="alert alert-success">{{ $entryReport->status_label }}</span>
                                            @endif
                                        </td>
                                        <td id="bread-actions" class="pull-left">
                                            @if($entryReport->status >= EntryReport::STATUS_FORWARD)
                                                <form class="pull-left"
                                                      action="{{ route('pdf.generate', ['view' => 'entry_report']) }}"
                                                      method="post" target="_blank">
                                                    @csrf()
                                                    @if($entryReport->items_count >= 100 )
                                                        <a title="Download"
                                                           class="btn btn-icon btn-sm btn-warning a-center download-big-pdf"
                                                           href="#"
                                                           data-server="{{ env('SERVER_URL' ?? '192.168.0.17').env('SERVER_IRL' ?? '/estoque') }}"
                                                           data-action="{{ route('pdf.download', ['view' => 'entry_report']) }}"
                                                           data-redirect="{{ request()->route('type') }}"
                                                           data-filename="{{ 'entry_report' }}"
                                                           data-values='{{ $entryReport->id }}'
                                                        >
                                                            {!! '🖨️' !!}
                                                        </a>
                                                    @else
                                                        <input id="data" name="data" type="hidden" value='{{ $entryReport->id }}'>

                                                        <button title="Gerar PDF" class="btn btn-icon btn-sm btn-warning"
                                                                type="submit">
                                                            {!! '🖨️' !!}
                                                        </button>
                                                    @endif
                                                    <span style="color: white; font-size: xx-small">.</span>
                                                </form>
                                            @endif
                                            @if(auth()->user()->isAdmin())
                                                @if($entryReport->status >= EntryReport::STATUS_COMPLETED)
                                                    <button title="Delete"
                                                            class="btn btn-icon btn-sm btn-dark pull-center do-request"
                                                            href="#"
                                                            data-message="{{ "Deseja voltar o status do Relatório ?" }}"
                                                            data-action="{{ route('entry-reports.rollback', $entryReport->id) }}"
                                                            data-redirect="{{ route('entry-reports.index', ['type' => request()->route('type')]) }}"
                                                    >
                                                        <i class="now-ui-icons arrows-1_minimal-left"></i>
                                                    </button>
                                                @endif

                                                <button title="Delete"
                                                        class="btn btn-icon btn-sm btn-danger pull-center do-request"
                                                        href="#"
                                                        data-message="{{ "Deseja confirmar exclusão ?" }}"
                                                        data-action="{{ route('entry-reports.destroy', $entryReport->id) }}"
                                                        data-redirect="{{ route('entry-reports.index', ['type' => request()->route('type')]) }}"
                                                >
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="11">Nenhum dado cadastrado até o momento</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{ $entryReports->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

@endsection
