@extends('layouts.app', [
    'namePage' => 'Dashboard',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'home',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
    <div class="panel-header panel-header-sd">
        <div class="header text-center">
            <h2 class="title">Bem vindo, {{ auth()->user()->name }}.</h2>
            <p class="category">Dashboard</p>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body all-icons">

                        <div class="content">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="card card-chart">
                                        <div class="card-header">
                                            <h5 class="card-category">Últimos 12 Meses</h5>
                                            <h4 class="card-title">Listas de Abastecimento Concluídas</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart-area">
                                                <canvas id="lineChart"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            {{--<div class="stats">
                                                <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="card card-chart">
                                        <div class="card-header">
                                            <h5 class="card-category">Última Semana</h5>
                                            <h4 class="card-title">Entrada de NFe</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart-area">
                                                <canvas id="lineChartWithNumbersAndGrid"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            {{--<div class="stats">
                                                <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="card card-chart">
                                        <div class="card-header">
                                            <h5 class="card-category">Últimos 12 Meses</h5>
                                            <h4 class="card-title">Conferências Concluídas</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart-area">
                                                <canvas id="barChartSimpleGradientsNumbers"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            {{--<div class="stats">
                                                <i class="now-ui-icons ui-2_time-alarm"></i> Last 7 days
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="card card-chart">
                                        <div class="card-header">
                                            <h5 class="card-category">Última Semana</h5>
                                            <h4 class="card-title">Entrada de NFe (Filiais)</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart-area">
                                                <canvas id="lineChartWithNumbersAndGrid2"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            {{--<div class="stats">
                                                <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        @php
            $datachartSupply = \App\Entities\Reports::getSupplyCompleted();
            $datachartNotes = \App\Entities\Reports::getNotesThisWeek();
            $datachartFilialNotes = \App\Entities\Reports::getFilialNotesThisWeek();
            $datachartRecount = \App\Entities\Reports::getRecountCompleted();
        @endphp

        $(document).ready(function() {
            var datachartSupply = {!! json_encode($datachartSupply) !!};
            var datachartNotes = {!! json_encode($datachartNotes) !!};
            var datachartFilialNotes = {!! json_encode($datachartFilialNotes) !!};
            var datachartRecount = {!! json_encode($datachartRecount) !!};

            // Javascript method's body can be found in assets/js/demos.js
            chart.chartSupply(datachartSupply['labels'], datachartSupply['data']);
            chart.chartNotes(datachartNotes['labels'], datachartNotes['data']);
            chart.chartFilialNotes(datachartFilialNotes['labels'], datachartFilialNotes['data']);
            chart.chartRecount(datachartRecount['labels'], datachartRecount['data']);
        });
    </script>
@endpush
