@php use App\Entities\Recount; @endphp
@extends('layouts.app', [
    'namePage' => 'Notas para conferencia',
    'class' => 'sidebar-mini',
    'activePage' => 'recounts',
    'activeNav' => '',
    'searchRoute' => route('notes.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">{{ "Notas de entrada - Fornecedor" }}</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('notes.index') }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('notes.index', ['status' => Recount::STATUS_PENDING]) }}">
                            Pendentes
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('notes.index', ['status' => Recount::STATUS_IN_PROGRESS]) }}">
                            Conferindo
                        </a>
                        <a class="btn btn-primary btn-round text-white"
                           href="{{ route('notes.index', ['status' => Recount::STATUS_COMPLETED]) }}">
                            Concluído
                        </a>

                        <a class="btn btn-primary btn-round text-white pull-right do-request"
                           href="#"
                           data-request-type="{{ "GET" }}"
                           data-message="{{ "Deseja importar notas de entrada?" }}"
                           data-action="{{ route('notes.import') }}"
                           data-redirect="{{ route('notes.index') }}"
                        >
                            {{ 'Importar' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="table-responsive">
                            <table id="datatable" class="table">
                                <thead class="text-primary">
                                <tr class="text-center">
                                    <th><b>#</b></th>
                                    <th><b>NF-E</b></th>
                                    <th><b>Fornecedor</b></th>
                                    <th><b>Empresa</b></th>
                                    <th><b>Dt. Argos</b></th>
                                    <th><b>Atualizado</b></th>
                                    <th><b>Produtos</b></th>
                                    <th><b>Status</b></th>
                                    <th class="disabled-sorting text-left"><b>{{ __('Ações') }}</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($notes ?? [] as $note)
                                    <tr class="text-center">
                                        <td><b>{{ $note->id }}</b></td>
                                        <td>{{ $note->nfe ?? '' }}</td>
                                        <td style="max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">
                                            {{ $note->name_provider ?? '' }}
                                        </td>
                                        <td style="font-size: xx-small">{{ $note->company->fantasy_name ?? '' }}</td>
                                        <td style="font-size: x-small">{{ $note->input_date->format('d/m/Y') ?? '' }}</td>
                                        <td style="font-size: x-small">{{ $note->updated_at ? $note->updated_at->format('d/m/Y H:i') : "" }}</td>
                                        <td style="font-size: x-small">{{ $note->items_count }}</td>
                                        <td style="font-size: x-small">
                                            @if($note->status == Recount::STATUS_PENDING)
                                                <span class="alert alert-danger">{{ $note->status_label }}</span>
                                            @elseif($note->status == Recount::STATUS_IN_PROGRESS)
                                                <span class="alert alert-warning">{{ $note->status_label }}</span>
                                            @else
                                                <span class="alert alert-success">{{ $note->status_label }}</span>
                                            @endif
                                        </td>
                                        <td id="bread-actions" class="pull-left">

                                            <button title="Produtos"
                                                    class="btn btn-info btn-icon btn-sm pull-center openModal"
                                                    data-toggle="modal"
                                                    data-target="#formModal"
                                                    data-title="{{ "Itens da NF-e: #$note->nfe" }}"
                                                    data-retrieve-items="{{ route('notes.items', $note->id) }}"
                                            >
                                                {!! '📦' !!}
                                            </button>

                                            @if(auth()->user()->isAdmin())
                                                <button title="Delete"
                                                        class="btn btn-icon btn-sm btn-danger pull-center do-request"
                                                        href="#"
                                                        data-message="{{ "Deseja confirmar exclusão ?" }}"
                                                        data-action="{{ route('notes.destroy', $note->id) }}"
                                                        data-redirect="{{ route('notes.index') }}"
                                                >
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="9">Nenhum dado cadastrado até o momento</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{ $notes->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <note-items></note-items>

@endsection
