@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Perfil de Usuário',
    'activePage' => 'profile',
    'activeNav' => '',
    'searchRoute' => route('users.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="col-md-12">
                        <div class="card card-user">
                            <div class="image">
                                <img src="{{asset('assets/img/bg5.jpg')}}" alt="...">
                            </div>
                            <div class="card-body">
                                <div class="author">
                                    <a href="#">
                                        <img class="avatar border-gray" src="{{ $user->avatar ?? asset('assets/img/default-avatar.png') }}" alt="...">
                                        <h5 class="title">{{ $user->name }}</h5>
                                    </a>
                                    <p class="description">
                                        {{ $user->email }}
                                    </p>
                                </div>
                            </div>
                            <hr>
                            <div class="button-container">
                                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                                    <i class="fab fa-facebook-square"></i>
                                </button>
                                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                                    <i class="fab fa-twitter"></i>
                                </button>
                                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                                    <i class="fab fa-google-plus-square"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
