@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Criar Usuário',
    'activePage' => 'user',
    'activeNav' => '',
    'searchRoute' => route('users.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">{{ __('Gerenciamento de usuários') }}</h2>
            <p class="category">Criar dados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do usuário') }}</h6>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('users.index') }}" class="btn btn-primary btn-round">{{ __('Voltar para lista') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('users.store') }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-name">{{ __('Nome') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('name') }}" required autofocus>

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-email">{{ __('Email') }}</label>
                                    <input type="email" name="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email') }}">

                                    @include('alerts.feedback', ['field' => 'email'])
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-password">{{ __('Senha') }}</label>
                                    <input type="password" name="password" id="input-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha') }}" value="">

                                    @include('alerts.feedback', ['field' => 'password'])
                                </div>
                                <div class="form-group">
                                    <label class="text-primary form-control-label" for="input-password-confirmation">{{ __('Confirmar Senha') }}</label>
                                    <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control" placeholder="{{ __('Confirmar Senha') }}" value="">
                                </div>

                                <br>
                                <hr>
                                <br>

                                <div class="form-group{{ $errors->has('company_id') ? ' has-danger' : '' }}">
                                    <label class=" text-primary" for="company_id">Empresa</label>

                                    <select id="company_id" name="company_id" class="form-control" required>
                                        <option selected>Selecione Uma opção</option>
                                        @foreach($companies as $key => $name)
                                            <option value="{{$key}}">{{$name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group{{ $errors->has('local_login') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-local_login">{{ __('Login Coletor') }}</label>
                                    <input type="text" name="local_login" id="input-local_login" class="form-control{{ $errors->has('local_login') ? ' is-invalid' : '' }}" placeholder="{{ __('Login Coletor') }}" value="{{ old('local_login') }}" required>

                                    @include('alerts.feedback', ['field' => 'local_login'])
                                </div>
                                <div class="form-group{{ $errors->has('local_password') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-local_password">{{ __('Senha Coletor') }}</label>
                                    <input type="password" name="local_password" id="input-local_password" class="form-control{{ $errors->has('local_password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha Coletor') }}" value="" required>

                                    @include('alerts.feedback', ['field' => 'local_password'])
                                </div>


                                <div class="form-group{{ $errors->has('admin') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-admin">{{ __('Permissão de administrador') }}</label>
                                    <input type="checkbox" data-toggle="toggle" data-style="slow" name="admin" data-on="Sim" data-off="Não">
                                </div>

                                <div class="form-group{{ $errors->has('admin') ? ' has-danger' : '' }}">
                                    <label class="text-primary form-control-label" for="input-separator">{{ __('Permissão de separador') }}</label>
                                    <input type="checkbox" data-toggle="toggle" data-style="slow" name="separator" data-on="Sim" data-off="Não">
                                </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn btn-primary btn-round mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
