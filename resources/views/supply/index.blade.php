@php use App\Entities\SupplyList; @endphp
@extends('layouts.app', [
    'namePage' => 'Abastecimento',
    'class' => 'sidebar-mini',
    'activePage' => 'supply',
    'activeNav' => '',
    'searchRoute' => route('supply.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Abastecimento</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('supply.index') }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('supply.index', ['status' => SupplyList::STATUS_TYPING]) }}">
                            Digitando
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('supply.index', ['status' => SupplyList::STATUS_PENDING]) }}">
                            Pendentes de Separação
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('supply.index', ['status' => SupplyList::STATUS_IN_PROGRESS]) }}">
                            Em Separação
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('supply.index', ['status' => SupplyList::STATUS_COMPLETED]) }}">
                            Concluído
                        </a>

                        <a class="btn btn-primary btn-round text-white pull-right mult-list"
                            href="#"
                            data-server="{{ env('SERVER_URL' ?? '192.168.0.17').env('SERVER_IRL' ?? '/estoque') }}"
                            data-action="{{ route('supply.multi') }}"
                            data-redirect="{{ route('supply.index') }}"
                        >
                            {{ 'PDF' }}
                        </a>

                        <a class="btn btn-primary btn-round text-white pull-right download-csv"
                           href="#"
                           data-action="{{ route('csv.export-list') }}"
                           data-type="list"
                        >
                            {{ 'CSV' }}
                        </a>

                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="table-responsive">
                            <table id="datatable" class="table" cellspacing="0" width="100%">
                                <thead class=" text-primary">
                                <tr class="text-center">
                                    <th><b></b></th>
                                    <th><b>#</b></th>
                                    <th><b>Tipo</b></th>
                                    <th><b>Local</b></th>
                                    <th><b>Usuário</b></th>
                                    <th><b>Separador</b></th>
                                    <th><b>Itens</b></th>
                                    <th><b>Criado</b></th>
                                    <th><b>Min. Sep.</b></th>
                                    <th><b>Status</b></th>
                                    <th class="disabled-sorting text-left"><b>{{ __('Ações') }}</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($supplyLists ?? [] as $list)
                                    <tr class="text-center">
                                        @if($list->status <> SupplyList::STATUS_TYPING)
                                            <td style="font-size: small"><b><input type="checkbox" class="multiples-lists" name="multiples[]" value="{{ $list->id }}"></b></td>
                                        @else
                                            <td style="font-size: small"><b></b></td>
                                        @endif
                                        <td style="font-size: small"><b>{{ $list->id }}</b></td>
                                        <td style="font-size: small">{{ $list->type }}</td>
                                        <td style="font-size: small">{{ $list->local }}</td>
                                        <td style="max-width: 70px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">{{ $list->user->name }}</td>
                                        <td style="max-width: 60px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: small">{{ $list->separator->name ?? '' }}</td>
                                        <td style="font-size: small">{{ $list->products_count ?? '' }}</td>
                                        <td style="font-size: small">{{ $list->created_at->format('d/m/Y H:i') }}</td>
                                        <td style="font-size: small">{{ $list->getWorktime() }}</td>
                                        <td style="font-size: xx-small">
                                            @if($list->status == SupplyList::STATUS_TYPING)
                                                <span class="alert alert-info">{{ $list->status_label }}</span>
                                            @elseif($list->status == SupplyList::STATUS_PENDING)
                                                <span class="alert alert-danger">{{ $list->status_label }}</span>
                                            @elseif($list->status == SupplyList::STATUS_IN_PROGRESS)
                                                <span class="alert alert-warning">{{ $list->status_label }}</span>
                                            @else
                                                <span class="alert alert-success">{{ $list->status_label }}</span>
                                            @endif
                                        </td>
                                        <td id="bread-actions" class="pull-left">

                                            <form class="pull-center" action="{{  route('pdf.generate', ['view' => 'supply_list_by_local']) }}" method="post" target="_blank">
                                                @if($list->status <> SupplyList::STATUS_TYPING)
                                                    @csrf()
                                                    @if($list->products_count >= 100 )
                                                        <a title="Download" class="btn btn-icon btn-sm btn-warning a-center download-big-pdf"
                                                           href="#"
                                                           data-server="{{ env('SERVER_URL' ?? '192.168.0.17').env('SERVER_IRL' ?? '/estoque') }}"
                                                           data-action ="{{ route('pdf.download', ['view' => 'supply_list_by_local']) }}"
                                                           data-redirect ="{{ route('supply.index') }}"
                                                           data-filename="{{ 'supply' }}"
                                                           data-values = '{{ $list->id }}'
                                                        >
                                                            {!! '🖨️' !!}
                                                        </a>
                                                    @else
                                                        <input id="data" name="data" type="hidden" value='{{ $list->id }}'>
                                                        <button title="Gerar PDF" class="btn btn-icon btn-sm btn-warning" type="submit">
                                                            {!! '🖨️' !!}
                                                        </button>
                                                    @endif
                                                @endif

                                                <a title="Editar" class="btn btn-info btn-icon btn-sm a-center openModal"
                                                   data-toggle="modal"
                                                   data-target="#formModal"
                                                   data-action="{{ route('supply.update', $list->id) }}"
                                                   data-method="{{ 'PUT' }}"
                                                   data-title="{{ "Editar Lista de abastecimento: #$list->id" }}"
                                                   data-retrieve="{{ route('supply.edit', $list->id) }}"
                                                   data-retrieve-separators="{{ route('users.separators') }}"
                                                   data-retrieve-status="{{ route('supply.status') }}"
                                                >
                                                    {!! '📝' !!}
                                                </a>

                                                @if(auth()->user()->isAdmin())
                                                    <a title="Delete" class="btn btn-icon btn-sm btn-danger pull-center do-request"
                                                       href="#"
                                                       data-message="{{ "Deseja confirmar exclusão ?" }}"
                                                       data-action="{{ route('supply.destroy', $list->id) }}"
                                                       data-redirect="{{ route('supply.index') }}"
                                                    >
                                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                                    </a>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10" align="center">Nenhum dado cadastrado até o momento</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{ $supplyLists->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <supply></supply>
    @include('pdf.iframe')
@endsection
