@php use App\Helper\Helper; @endphp
@extends('layouts.app', [
    'namePage' => 'Relatório de Auditoria',
    'class' => 'sidebar-mini',
    'activePage' => 'audits',
    'activeNav' => '',
    'searchRoute' => route('audits.index', request()->query()),
])

@section('content')
    <div class="panel-header panel-header-md bg-gradient-primary">
        <div class="header text-center text-white">
            <h2 class="title">Relatório de Auditoria</h2>
            <p class="category">Acompanhe alterações realizadas no sistema</p>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-lg">
                    <!-- Filtros -->
                    <div class="card-header">

                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{ route('audits.index') }}">
                            <div class="row g-3">
                                <div class="col-md-4">
                                    <label class="text-primary" for="table_name">Tabela</label>
                                    <select name="table_name" id="table_name" class="form-control">
                                        <option value="">Todas</option>
                                        @foreach($tableNames as $tableName)
                                            <option value="{{ $tableName }}" {{ request('table_name') == $tableName ? 'selected' : '' }}>
                                                {{ Helper::getTranslatedTableName($tableName) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="text-primary" for="user_id">Usuário</label>
                                    <select name="user_id" id="user_id" class="form-control">
                                        <option value="">Todos</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" {{ request('user_id') == $user->id ? 'selected' : '' }}>
                                                {{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="text-primary" for="action">Ação</label>
                                    <select name="action" id="action" class="form-control">
                                        <option value="">Todas</option>
                                        @foreach($actions as $key => $action)
                                            <option value="{{ $key }}" {{ request('action') == $key ? 'selected' : '' }}>
                                                {{ ucfirst($action) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row g-3 mt-3">
                                <div class="col-md-6">
                                    <label class="text-primary" for="date_from">Data Inicial</label>
                                    <input type="date" name="date_from" id="date_from" class="form-control"
                                           value="{{ request('date_from') }}">
                                </div>
                                <div class="col-md-6">
                                    <label class="text-primary" for="date_to">Data Final</label>
                                    <input type="date" name="date_to" id="date_to" class="form-control"
                                           value="{{ request('date_to') }}">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12 text-end">
                                    <button type="submit" class="btn btn-primary btn-round">
                                        <i class="fas fa-filter"></i> Filtrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <hr>
                    <!-- Tabela de Auditoria -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table align-middle">
                                <thead class="bg-primary text-white">
                                <tr>
                                    <th>ID</th>
                                    <th>Descrição</th>
                                    <th>Tabela</th>
                                    <th>ID Registro</th>
                                    <th>Ação</th>
                                    <th>Usuário</th>
                                    <th>Empresa</th>
                                    <th>Data</th>
                                    <th>Detalhes</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($audits as $audit)
                                    <tr>
                                        <td>{{ $audit->id }}</td>
                                        <td>{{ $audit->description }}</td>
                                        <td>{{ Helper::getTranslatedTableName($audit->table_name) }}</td>
                                        <td>{{ $audit->record_id }}</td>
                                        <td>{{ $audit->translated_action }}</td>
                                        <td>{{ $audit->user->name ?? 'Desconhecido' }}</td>
                                        <td>{{ $audit->user->company->fantasy_name ?? '' }}</td>
                                        <td>{{ $audit->created_at->format('d/m/Y H:i:s') }}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm toggle-details" data-toggle="collapse"
                                                    data-target="#details-{{ $audit->id }}">
                                                <i class="fas fa-arrow-down"></i> Expandir
                                            </button>
                                        </td>
                                    </tr>
                                    <tr id="details-{{ $audit->id }}" class="collapse bg-light">
                                        <td colspan="9">
                                            <div class="row">
                                                @php
                                                    // Decodificar as alterações em JSON
                                                    $changes = json_decode($audit->changes, true);
                                                @endphp

                                                @if(!empty($changes))
                                                    <div class="col-md-12">
                                                        <h6 class="text-primary">Alterações Registradas</h6>
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Campo</th>
                                                                <th>Valor Anterior</th>
                                                                <th>Valor Novo</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($changes as $field => $change)
                                                                @if(is_array($change) && isset($change['old'], $change['new']))
                                                                    <!-- Situação 1: Campo com valores 'old' e 'new' -->
                                                                    <tr>
                                                                        <td>{{ $field }}</td>
                                                                        <td class="text-danger">
                                                                            {{ is_array($change['old']) ? json_encode($change['old']) : ($change['old'] ?? 'N/A') }}
                                                                        </td>
                                                                        <td class="text-success">
                                                                            {{ is_array($change['new']) ? json_encode($change['new']) : ($change['new'] ?? 'N/A') }}
                                                                        </td>
                                                                    </tr>
                                                                @elseif($field === 'new' && is_array($change))
                                                                    <!-- Situação 2: Novo registro (apenas 'new') -->
                                                                    @foreach($change as $key => $newValue)
                                                                        <tr>
                                                                            <td>{{ $key }}</td>
                                                                            <td class="text-danger">N/A</td>
                                                                            <td class="text-success">
                                                                                {{ is_array($newValue) ? json_encode($newValue) : $newValue }}
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($field === 'old' && (is_string($changes['old']) || $changes['old'] == null) && isset($changes['new']))
                                                                    <!-- Situação 3: Strings 'old' e 'new' -->
                                                                    <tr>
                                                                        <td>{{ 'N/A' }}</td>
                                                                        <td class="text-danger">
                                                                            {!! Helper::highlightDifferences($changes['old'], $changes['new'], 'text-decoration: underline; font-weight: bold;') !!}
                                                                        </td>
                                                                        <td class="text-success">
                                                                            {!! Helper::highlightDifferences($changes['new'], $changes['old'], 'text-decoration: underline; font-weight: bold;') !!}
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @else
                                                    <div class="col-md-12 text-center">
                                                        <p class="text-muted">Nenhuma alteração registrada.</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" class="text-center">Nenhum registro encontrado.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        <!-- Paginação -->
                        <div class="d-flex justify-content-center mt-3">
                            {{ $audits->withQueryString()->links('pagination::bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
