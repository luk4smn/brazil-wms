@php use App\Entities\Company;use App\Entities\Product; @endphp
    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        .text-small {
            font-size: x-small;
        }

    </style>

    <title>LISTA DE ABASTECIMENTO COMBINADA #</title>
</head>
<body>

<h6 align="center"><b>LISTA DE ABASTECIMENTO COMBINADA</b></h6>
<br>
<div class="clearfix">
    <div class="text-small"><b>Listas: </b>{{ $ids }}</div>
</div>
<br>

@php $i=1 @endphp

<div class="table-responsive">
    <table class="table table-striped table-sm text-small">
        <thead class="thead-dark">
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">ID</th>
            <th style="text-align: center">COD. BARRAS</th>
            <th style="text-align: center">DESCRIÇÃO</th>
            <th style="text-align: center">REF.</th>
            <th style="text-align: center">FORNECEDOR</th>
            <th style="text-align: center">QTD</th>
            <th style="text-align: center">UND</th>
            <th style="text-align: center">LOJA</th>
            <th style="text-align: center">INARTEL</th>
            <th style="text-align: center">BAYEUX CG</th>
            <th style="text-align: center">BAYEUX JP</th>
            <th style="text-align: center">LOCAL</th>
            <th style="text-align: center">ATENDIDO?</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $key => $item)
            <tr>
                <td align="center">{{ $i++ }}</td>
                <td align="center">{{ $item['cod_aux'] ?? '' }}</td>
                <td align="center">{{ $item['product']['codebar']['cod_bar'] ?? '' }}</td>
                <td align="left">{{ $item['product']['descricao'] ?? '' }}</td>
                <td align="center">{{ $item['product']['referencia'] ?? '' }}</td>
                <td align="center">{{ $item['product']['nome_fornecedor'] ?? '' }}</td>
                <td align="center"><b>{{ $item['quantity'] ?? '' }}</b></td>
                <td align="center">{{ $item['product']['unidade_saida'] ?? '' }}</td>
                <td align="center">
                    @if(auth()->user()->company_id == Company::WL_MATRIZ)
                        {{ round(($item['product']['estoque_matriz'] ?? 0),0) }}
                    @elseif(auth()->user()->company_id == Company::BRAZIL_JP)
                        {{ round(($item['product']['estoque_jp'] ?? 0),0) }}
                    @else
                        {{ '' }}
                    @endif
                </td>
                <td align="center">{{ round(($item['product']['estoque_inartel'] ?? 0),0) }}</td>
                <td align="center">{{ round(($item['product']['estoque_bayeux'] ?? 0),0) }}</td>
                <td align="center">{{ round(($item['product']['estoque_bayeux_jp'] ?? 0),0) }}</td>
                <td align="left">
                    {{ Product::getLocalByCompany($item['product']['cod_mat'], auth()->user()->company_id, $item['product']) }}
                </td>
                <td align="center">
                    <div class="form-check" align="center">
                        {{-- <input class="form-check-input" type="checkbox"> --}}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot align="right">

        </tfoot>
    </table>
</div>
</body>
</html>
