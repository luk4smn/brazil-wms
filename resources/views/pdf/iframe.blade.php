<div id="pdfModal" style="display:none; position: fixed; top: 5%; left: 5%; width: 90%; height: 90%; background: rgba(255, 255, 255, 0.95); border-radius: 8px; box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1); z-index: 9999; overflow: hidden;">
    <div style="position: absolute; top: 10px; right: 10px;">
        <button onclick="document.getElementById('pdfModal').style.display='none';"
                style="background: #ff5f5f; color: white; border: none; padding: 10px 15px; border-radius: 50%; cursor: pointer; font-size: 16px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);">
            &times;
        </button>
    </div>
    <iframe id="pdfViewer" style="width: 100%; height: 100%; border: none; overflow: auto;"></iframe>
</div>
