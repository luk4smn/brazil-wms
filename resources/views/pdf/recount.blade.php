@php use App\Entities\Company;
use App\Entities\Product;use App\Entities\Recount;use App\Helper\Helper;use Carbon\Carbon; @endphp
    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        .parent {
            white-space: nowrap;
            width: 100%;
        }

        .parent > div {
            display: inline-block;
            vertical-align: top;
            white-space: normal;
            width: 50%;
        }

        .text-small {
            font-size: xx-small;
        }

        @page {
            size: A4 landscape;
            margin: 5mm; /* Altere conforme necessário */
        }
    </style>

    <title>CONFERÊNCIA #{{$id}}</title>
</head>
<body>
<div class="col-12 text-small parent">
    <div class="col-6 text-small">
        <h6><b>{{ $provider_names }}</b></h6>
        <br>
        <table class="table table-sm table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">NF-e</th>
                <th scope="col">Data Entrada</th>
                <th scope="col">Itens NFe</th>
                <th scope="col">Total</th>
                @if(auth()->user()->company_id == Company::BRAZIL_JP)
                    <th scope="col">NF Origem</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($notes as $note)
                <tr>
                    <td>{{ $note['nfe'] }}</td>
                    <td>{{ Carbon::parse($note['input_date'])->format("d/m/Y") }}</td>
                    <td>{{ $note['items_count'] ?? '0' }}</td>
                    <td>@money( $note['total'] )</td>
                    @if(auth()->user()->company_id == Company::BRAZIL_JP)
                        <th scope="col">{{ $note['origin_moviment'] ?? "" }}</th>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-6 text-small" style="text-align: left;">
        <span class="text-small" style="display: block;"><b>Conferência Nº:</b> {{ $id }}</span>
        <span class="text-small" style="display: block;"><b>Conferente(s):</b> {{ $usernames }}</span>
        <span class="text-small" style="display: block;"><b>Impressão: </b>{{ now()->format("d/m/Y H:i") }}</span>
        <span class="text-small" style="display: block;"><b>Última Atualização:</b>{{ Carbon::parse($updated_at)->timezone(Config::get('app.timezone'))->format("d/m/Y H:i") }}</span>
    </div>
</div>

<br>
{{--    ITENS DIVERGENTES    --}}
<div class="col-12 text-small">
    <h6 align="center"><b>ITENS DIVERGENTES</b></h6>
    <table class="table table-sm table-bordered ">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 15px;">#</th>
            <th scope="col" style="width: 90px;">Cód</th>
            <th scope="col" style="width: 200px;">Produto</th>
            <th scope="col" style="width: 50px;">Und NFe</th>
            <th scope="col" style="width: 30px;">Vols.</th>
            <th scope="col" style="width: 50px;">Est. CG</th>
            <th scope="col" style="width: 50px;">Est. JP</th>
            <th scope="col" style="width: 55px;">Und Venda</th>
            <th scope="col" style="width: 60px;">Qtd NFe</th>
            <th scope="col" style="width: 60px;">Qtd Confer.</th>
            <th scope="col">Difer.</th>
            <th scope="col" style="width: 30px;">Avaria</th>
            <th scope="col" style="width: 80px;">Obs.</th>
            <th scope="col" style="width: 80px;">Local</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i = 1;
        @endphp

        @forelse($products as $item)
            @if($item['quantity'] <>  $item['total_quantity_confer'])
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ Recount::getCodebar($item['cod_mat']) }}</td>
                    <td>{{ $item['description'] ?? ''}}</td>
                    <td>{{ $item['unity'] ?? '' }}</td>
                    <td>{{ $item['total_volumes'] ?? '' }}</td>
                    <td>{{ round($item['estoque_matriz'] ?? 0, 2)}}</td>
                    <td>{{ round($item['estoque_jp'] ?? 0, 2) }}</td>
                    <td>{{ $item['unity_exit'] ?? '' }}</td>
                    <td>{{ $item['quantity'] ?? '' }}</td>
                    <td>{{ $item['total_quantity_confer'] ?? 0 }}</td>
                    <td>{{ $item['total_quantity_confer'] - $item['quantity'] }}</td>
                    <td>{{ $item['total_quantity_damage'] }}</td>
                    <td>
                        @if(($item['total_quantity_confer'] - $item['quantity']) > 0 && ($item['total_quantity_damage'] == 0))
                            Sobra
                        @elseif(($item['total_quantity_confer'] - $item['quantity']) < 0 && ($item['total_quantity_damage'] == 0))
                            Falta
                        @elseif(($item['total_quantity_confer'] - $item['quantity']) > 0 && ($item['total_quantity_damage'] > 0))
                            Sobra e Avaria
                        @elseif(($item['total_quantity_confer'] - $item['quantity']) < 0 && ($item['total_quantity_damage'] > 0) && (abs($item['total_quantity_confer'] - $item['quantity']) <> $item['total_quantity_damage']))
                            Falta e Avaria
                        @elseif(($item['total_quantity_confer'] - $item['quantity']) <> 0 && (abs($item['total_quantity_confer'] - $item['quantity']) == $item['total_quantity_damage']))
                            Avaria
                        @else
                            {{ '' }}
                        @endif
                    </td>
                    <td align="left">
                        {{ Product::getLocalByCompany($item['cod_mat'], $user['company_id']) }}
                    </td>
                </tr>
            @endif
        @empty
            <tr>
                <td colspan="14">Nenhum item divergente.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>

<br>
{{--    ITENS CONFORMES    --}}
<div class="col-12 text-small">
    <h6 align="center"><b>ITENS CONFORMES</b></h6>
    <table class="table table-sm table-bordered ">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 15px;">#</th>
            <th scope="col" style="width: 90px;">Cód</th>
            <th scope="col" style="width: 200px;">Produto</th>
            <th scope="col" style="width: 50px;">Und NFe</th>
            <th scope="col" style="width: 50px;">Vols.</th>
            <th scope="col" style="width: 50px;">Est. CG</th>
            <th scope="col" style="width: 50px;">Est. JP</th>
            <th scope="col" style="width: 55px;">Und Venda</th>
            <th scope="col" style="width: 60px;">Qtd NFe</th>
            <th scope="col" style="width: 60px;">Qtd Confer.</th>
            <th scope="col" style="width: 30px;">Avaria</th>
            <th scope="col" style="width: 100px;">Local</th>
        </tr>
        </thead>
        <tbody>
        @php
            $j = 1;
        @endphp
        @forelse($products as $key => $item)
            @if($item['quantity'] ==  $item['total_quantity_confer'])
                <tr>
                    <td>{{ $j++ }}</td>
                    <td>{{ Recount::getCodebar($item['cod_mat'] ?? '') }}</td>
                    <td>{{ $item['description'] ?? '' }}</td>
                    <td>{{ $item['unity'] ?? '' }}</td>
                    <td>{{ $item['total_volumes'] ?? '' }}</td>
                    <td>{{ round($item['estoque_matriz'] ?? 0, 2)}}</td>
                    <td>{{ round($item['estoque_jp'] ?? 0, 2) }}</td>
                    <td>{{ $item['unity_exit'] ?? '' }}</td>
                    <td>{{ $item['quantity'] }}</td>
                    <td>{{ $item['total_quantity_confer'] ?? ''}}</td>
                    <td>{{ $item['total_quantity_damage'] ?? ''}}</td>
                    <td align="left">
                        {{ Product::getLocalByCompany($item['cod_mat'], Company::WL_MATRIZ) }}
                    </td>
                </tr>
                {{-- Exibe os conferentes abaixo do item --}}
                @if(count($users) > 1 && count($names = Helper::getRecountUsersNames($item)) > 0)
                    <tr>
                        <td colspan="12">
                            <b>Conferente(s) do item:</b> {{ implode(', ', $names) }}
                        </td>
                    </tr>
                @endif
            @endif
        @empty
            <tr>
                <td colspan="12">Nenhum item em conformidade.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>
