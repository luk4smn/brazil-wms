@php use App\Entities\Company;
use App\Entities\EntryReportItems;use App\Entities\Product;use App\Entities\Recount;use App\Helper\Helper;use Carbon\Carbon; @endphp
    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        .parent {
            white-space: nowrap;
            width: 100%;
        }

        .parent > div {
            display: inline-block;
            vertical-align: top;
            white-space: normal;
            width: 50%;
        }

        .text-small {
            font-size: xx-small;
        }

        @page {
            size: A4 landscape;
            margin: 5mm; /* Altere conforme necessário */
        }

        h1 {
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            margin-bottom: 20px;
        }
    </style>

    <title>RELATÓRIO DE ENTRADA #{{$id}}</title>
</head>
<body>
<h1>RELATÓRIO DE ENTRADA #{{$id}}</h1>

<div class="col-12 text-small parent">
    <div class="col-6 text-small">
        <h6><b>{{ $related['provider_names'] }}</b></h6>
        <br>
        <table class="table table-sm table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">NF-e(s)</th>
                <th scope="col">Data</th>
                <th scope="col">Itens</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $related['nfes'] }}</td>
                <td>{{ Carbon::parse($created_at)->timezone(Config::get('app.timezone'))->format("d/m/Y") }}</td>
                <td>{{ $items_count ?? '0' }}</td>
                <td>{{ $status_label }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-6 text-small" style="text-align: left;">
        <span class="text-small" style="display: block;"><b>Separador(es) | Recebedor(es):</b> {{ $usernames }}</span>
        <span class="text-small" style="display: block;"><b>Impressão:</b> {{ now()->format("d/m/Y H:i") }}</span>
        <span class="text-small" style="display: block;"><b>Última Atualização:</b> {{ Carbon::parse($updated_at)->timezone(Config::get('app.timezone'))->format("d/m/Y H:i") }}</span>
    </div>
</div>

<br>
{{--    ITENS DIVERGENTES    --}}
<div class="col-12 text-small">
    <h6 align="center"><b>ITENS DIVERGENTES</b></h6>
    <table class="table table-sm table-bordered ">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 15px;">#</th>
            <th scope="col" style="width: 90px;">Cód</th>
            <th scope="col" style="width: 250px;">Produto</th>
            @if($users[0]['company_id'] ?? null == Company::WL_MATRIZ)
                <th scope="col" style="width: 50px;">Est. CG</th>
            @else
                <th scope="col" style="width: 50px;">Est. JP</th>
            @endif
            <th scope="col" style="width: 55px;">Und Venda</th>
            <th scope="col" style="width: 55px;">Preço Venda</th>
            <th scope="col" style="width: 80px;">Local</th>
            <th scope="col" style="width: 80px;">Status Item</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i = 1;
            $collected = Product::collectProductsByLocal($items);
        @endphp

        @forelse($collected as $item)
            @if($item['status'] == EntryReportItems::STATUS_NOT_FOUND || $item['status'] == EntryReportItems::STATUS_NOT_RECEIVED)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ Recount::getCodebar($item['product']['cod_mat']) }}</td>
                    <td>{{ $item['product']['descricao'] ?? ''}}</td>
                    <td>@if($user['company_id'] ?? null == Company::WL_MATRIZ)
                            {{ round($item['product']['estoque_matriz'] ?? 0, 2)}}
                        @else
                            {{ round($item['product']['estoque_jp'] ?? 0, 2) }}
                        @endif</td>
                    <td>{{ $item['product']['unidade_saida'] ?? '' }}</td>
                    <td>@money($item['product']['preco_venda'])</td>
                    <td align="left">
                        {{ Product::getLocalByCompany($item['cod_mat'], $users[0]['company_id'], $item['product']) }}
                    </td>
                    <td>{{ $item['status_label'] ?? '' }}</td>
                </tr>
            @endif
        @empty
            <tr>
                <td colspan="14">Nenhum item divergente.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>

<br>
{{--    ITENS CONFORMES    --}}
<div class="col-12 text-small">
    <h6 align="center"><b>ITENS CONFORMES</b></h6>
    <table class="table table-sm table-bordered ">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 15px;">#</th>
            <th scope="col" style="width: 90px;">Cód</th>
            <th scope="col" style="width: 250px;">Produto</th>
            @if($user['company_id'] ?? null == Company::WL_MATRIZ)
                <th scope="col" style="width: 50px;">Est. CG</th>
            @else
                <th scope="col" style="width: 50px;">Est. JP</th>
            @endif
            <th scope="col" style="width: 55px;">Und Venda</th>
            <th scope="col" style="width: 55px;">Preço Venda</th>
            <th scope="col" style="width: 80px;">Local</th>
            <th scope="col" style="width: 80px;">Separador</th>
            <th scope="col" style="width: 80px;">Recebedor</th>
            <th scope="col" style="width: 80px;">Status Item</th>
        </tr>
        </thead>
        <tbody>
        @php
            $j = 1;
        @endphp
        @forelse($collected as $item)
            @if($item['status'] != EntryReportItems::STATUS_NOT_FOUND && $item['status'] != EntryReportItems::STATUS_NOT_RECEIVED)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ Recount::getCodebar($item['product']['cod_mat']) }}</td>
                    <td>{{ $item['product']['descricao'] ?? ''}}</td>
                    <td>@if($user['company_id'] ?? null == Company::WL_MATRIZ)
                            {{ round($item['product']['estoque_matriz'] ?? 0, 2)}}
                        @else
                            {{ round($item['product']['estoque_jp'] ?? 0, 2) }}
                        @endif</td>
                    <td>{{ $item['product']['unidade_saida'] ?? '' }}</td>
                    <td>@money($item['product']['preco_venda'])</td>
                    <td align="left">
                        {{ Product::getLocalByCompany($item['cod_mat'], $users[0]['company_id']) }}
                    </td>
                    <td>{{ $item['separator']['name'] ?? '' }}</td>
                    <td>{{ $item['receiver']['name'] ?? '' }}</td>
                    <td>{{ $item['status_label'] ?? '' }}</td>
                </tr>
            @endif
        @empty
            <tr>
                <td colspan="12">Nenhum item em conformidade.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>
