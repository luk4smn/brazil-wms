<!-- Modal Estilizado -->
<div class="modal fade" id="infoModal" data-keyboard="false" data-backdrop="static" tabindex="-1" aria-labelledby="infoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Cabeçalho do Modal -->
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="infoModalLabel">Informações da Conferência</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Corpo do Modal -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <span id="modalIdentifierText"><b>Carregando...</b></span>
                        <p id="modalInfoText">Carregando...</p>
                    </div>
                </div>
            </div>
            <!-- Rodapé do Modal -->
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
