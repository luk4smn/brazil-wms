<?php

namespace App\Listeners;

use App\Events\AtualizaNotaCegaEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Throwable;

class AtualizaNotaCegaListener implements ShouldQueue
{
    use InteractsWithQueue;


    /**
     * Número máximo de tentativas.
     *
     * @var int
     */
    public int $tries = 2;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AtualizaNotaCegaEvent $event
     * @return void
     * @throws Throwable
     */
    public function handle(AtualizaNotaCegaEvent $event): void
    {
        try {
            Log::info("Iniciando processamento do evento AtualizaNotaCegaEvent");

            $event->notes->each(function ($note) use ($event) {
                try {
                    $class = new $event->eventClass();
                    Log::info("Processando movimento", ['moviment_order' => $note->moviment_order]);

                    $blindNote = $class::where('ordem_movimento', $note->moviment_order)
                        ->whereNotIn('status_wms', [$event->status])
                        ->first();

                    if ($blindNote) {
                        Log::info("Atualizando status da nota cega", ['status' => $event->status]);
                        $blindNote->updateBlindNoteConferenceStatus($event->status);
                    } else {
                        Log::warning("Nota cega não encontrada", [
                            'moviment_order' => $note->moviment_order,
                            'status' => $event->status,
                            'class' =>  $event->eventClass]
                        );
                    }
                } catch (Throwable $e) {
                    Log::error("Erro ao processar nota", ['moviment_order' => $note->moviment_order, 'error' => $e->getMessage(),]);
                    throw $e; // Lança exceção para que o sistema realize o retry
                }
            });

            Log::info("Evento AtualizaNotaCegaEvent processado com sucesso.");
        } catch (Throwable $e) {
            Log::critical("Erro no listener AtualizaNotaCegaListener", [
                'eventClass' => $event->eventClass,
                'error' => $e->getMessage(),
            ]);
            throw $e; // Lança exceção para que o sistema realize o retry
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AtualizaNotaCegaEvent $event
     * @param Throwable $exception
     * @return void
     */
    public function failed(AtualizaNotaCegaEvent $event, Throwable $exception): void
    {
        Log::critical("Falha definitiva ao processar evento AtualizaNotaCegaEvent", [
            'eventClass' => $event->eventClass,
            'error' => $exception->getMessage(),
        ]);
    }
}
