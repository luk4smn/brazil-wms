<?php

namespace App;

use App\Entities\Company;
use App\Entities\Filial\FilialRecount;
use App\Entities\Recount;
use App\Entities\SupplyList;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes, Searchable;

    //protected $dateFormat = 'd-m-Y H:i:s';

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $fillable = [
        'name',
        'email',
        'password',
        'local_login',
        'local_password',
        'admin',
        'separator',
        'company_id'
    ];

    protected $hidden = [
        'local_password',
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin(){
        return $this->attributes['admin'];
    }

    public function isSeparator(){
        return $this->attributes['separator'];
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function supplyLists(): HasMany
    {
        return $this->hasMany(SupplyList::class, 'user_id', 'id');
    }

    public function recounts(): BelongsToMany
    {
        return $this->belongsToMany(Recount::class, 'recount_users', 'user_id', 'recount_id');
    }

    public function filialRecounts(): BelongsToMany
    {
        return $this->belongsToMany(FilialRecount::class, 'filial_recount_users', 'user_id', 'recount_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims(): array
    {
        return [];
    }

}
