<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AtualizaNotaCegaEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $notes;
    public $eventClass;
    public $status;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($notes, $eventClass, $status)
    {
        $this->notes = $notes;
        $this->eventClass = $eventClass;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn(): Channel|PrivateChannel|array
    {
        return new PrivateChannel('default');
    }
}
