<?php

namespace App\Jobs;

use App\Entities\Audit;
use App\Entities\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RemoveLocalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $codMat;
    protected $local;
    protected $realField;
    protected $viewField;
    protected $user;

    public function __construct($codMat, $local, $realField, $viewField, $user)
    {
        $this->codMat    = $codMat;
        $this->local     = $local;
        $this->realField = $realField;
        $this->viewField = $viewField;
        $this->user      = $user;
    }

    public function handle(): void
    {
        Log::info('Job de atualização de local: '. $this->local);

        Product::whereLike($this->viewField, $this->local)
            ->orderBy('descricao', 'asc')
            ->get()
            ->filter(function ($product) {
                return preg_match("/\b{$this->local}\b/", $product->{$this->viewField});
            })
            ->map(function($product) {
                self::updateRealField($this->local, $product, $this->realField, $this->viewField, $this->user);
            });
    }

    public static function updateRealField($localRequest, $product, $realField, $viewField, $user): void
    {
        $localRequest = strtoupper(str_replace(' ', '', $localRequest));
        $locals = str_replace(' ', '', ($product[$viewField]));
        $locals = str_replace(array("\r\n", "\n"), '', $locals);
        $locals = explode(",", $locals);
        // Remove duplicados, valores inválidos e ordena
        $locals = array_filter(array_unique($locals), function ($local) {
            return !empty(trim($local));
        });
        asort($locals);

        if(count($locals) > 0){
            foreach ($locals as $key => $productLocal){
                $productLocal = strtoupper($productLocal);

                if($productLocal == ',' || $productLocal == ', '|| $productLocal == ' ,'|| $productLocal == "" || $productLocal == " ")
                    unset($locals[$key]);

                if($productLocal == $localRequest)
                    unset($locals[$key]);

            }

            $finalString = implode(", ", $locals);
            $sql = "UPDATE Brazil_Inform.dbo.CADMATERIAL SET {$realField}='{$finalString}' WHERE CODIGOMATERIAL='{$product['cod_mat']}'";
            DB::connection('sqlsrv')->statement($sql);

            Audit::create([
                'description' => "Local de estoque removido",
                'table_name'  => $product->getTable(),
                'record_id'   => $product['cod_aux'],
                'action'      => 'update',
                'changes'     => json_encode(['old' => $product[$viewField], 'new' => $finalString]),
                'user_id'     => $user?->id,
            ]);
        }


    }
}
