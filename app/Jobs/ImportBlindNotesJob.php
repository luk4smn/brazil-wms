<?php

namespace App\Jobs;

use App\Entities\BlindNote;
use App\Entities\Company;
use App\Entities\Note;
use App\Entities\NoteItems;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ImportBlindNotesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        {
            Log::info('Comando de importação de notas de entrada iniciado');

            try {
                $blindNotes = BlindNote::all();

                if ($blindNotes) {
                    $blindNotes->each(function ($blindNote) {
                        $note = Note::updateOrCreate(
                            ['cod_provider' => $blindNote->cod_fornecedor, 'nfe' => $blindNote->numero_movimento],
                            [
                                'input_date'    => $blindNote->data_entrada,
                                'status_argos'  => $blindNote->status_argos,
                                'name_provider' => $blindNote->nome_fornecedor,
                                'moviment_order' => $blindNote->ordem_movimento,
                                'total'         => $blindNote->total,
                                'company_id'    => Company::getCompanyCode($blindNote->cod_filial),
                                'origin_moviment' => $blindNote->movimento_origem
                            ]
                        );

                        $blindNoteItems = $blindNote->items();
                        $collected = $blindNoteItems->groupBy('cod_mat')->map(function ($item) {
                            return $item->sum('quantidade');
                        });
                        $blindNoteItems = $blindNoteItems->unique('cod_mat');

                        $blindNoteItems->transform(function ($data) use ($collected) {
                            if ($collected[$data->cod_mat]) {
                                $data->quantidade = $collected[$data->cod_mat];
                            }
                            return $data;
                        });

                        $blindNoteItems->each(function ($blindNoteItem) use ($note) {
                            NoteItems::updateOrCreate(
                                ['note_id' => $note->id, 'cod_mat' => $blindNoteItem->cod_mat],
                                [
                                    'moviment_order' => $blindNoteItem->ordem_movimento,
                                    'quantity'       => round(($blindNoteItem->quantidade ?? 0), 0),
                                    'quantity_nfe'   => round(($blindNoteItem->quantidade_faturada ?? 0), 0),
                                    'factor'         => (int) ($blindNoteItem->fator ?? 1),
                                    'unity'          => $blindNoteItem->unidade,
                                ]
                            );
                        });

                        $blindNote->updateBlindNoteConferenceStatus(BlindNote::STATUS_NAO_CONFERIDO);
                    });
                }
                Log::info('Comando de importação de notas  - secesso');

            } catch (\Exception $ex) {
                Log::error('Comando de importação de notas  - falha: ' . $ex->getMessage());
            }
        }
    }
}
