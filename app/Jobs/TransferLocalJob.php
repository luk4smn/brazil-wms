<?php

namespace App\Jobs;

use App\Entities\Audit;
use App\Entities\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransferLocalJob  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $oldLocal;
    protected $newLocal;
    protected $realField;
    protected $viewField;
    protected $user;

    public function __construct($oldLocal, $newLocal, $realField, $viewField, $user)
    {
        $this->oldLocal  = $oldLocal;
        $this->newLocal  = $newLocal;
        $this->realField = $realField;
        $this->viewField = $viewField;
        $this->user      = $user;
    }

    public function handle(): void
    {
        Log::info('Job de transferencia de local: '. $this->oldLocal .' para: ' . $this->newLocal);

        Product::whereLike($this->viewField, $this->oldLocal)
            ->orderBy('descricao', 'asc')
            ->get()
            ->filter(function ($product) {
                return preg_match("/\b{$this->oldLocal}\b/", $product->{$this->viewField});
            })
            ->map(function ($product) {
                self::updateRealField($this->oldLocal, $this->newLocal, $product, $this->realField, $this->viewField, $this->user);
            });
    }

    public static function updateRealField($oldLocal, $newLocal, $product, $realField, $viewField, $user): void
    {
        $oldLocal = strtoupper(str_replace(' ', '', $oldLocal));
        $newLocal = strtoupper(str_replace(' ', '', $newLocal));

        $locals = str_replace(' ', '', ("{$product[$viewField]}, {$newLocal}"));
        $locals = str_replace(array("\r\n", "\n"), '', $locals);
        $locals = explode(",", $locals);

        $locals = array_filter(array_unique($locals), function ($local) {
            return !empty(trim($local));
        });
        asort($locals);

        if(count($locals) > 0){
            foreach ($locals as $key => $productLocal){
                $productLocal = strtoupper($productLocal);

                if($productLocal == ',' || $productLocal == ', '|| $productLocal == ' ,'|| $productLocal == "" || $productLocal == " ")
                    unset($locals[$key]);

                if($productLocal == $oldLocal)
                    unset($locals[$key]);
            }

            $finalString = implode(", ", $locals);
            $sql = "UPDATE Brazil_Inform.dbo.CADMATERIAL SET {$realField}='{$finalString}' WHERE CODIGOMATERIAL='{$product['cod_mat']}'";
            DB::connection('sqlsrv')->statement($sql);

            Audit::create([
                'description' => "Local de estoque transferido",
                'table_name'  => $product->getTable(),
                'record_id'   => $product['cod_aux'],
                'action'      => 'update',
                'changes'     => json_encode(['old' => $product[$viewField], 'new' => $finalString]),
                'user_id'     => $user?->id,
            ]);
        }
    }

}
