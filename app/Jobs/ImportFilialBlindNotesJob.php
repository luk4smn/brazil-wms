<?php

namespace App\Jobs;

use App\Entities\Company;
use App\Entities\Filial\FilialBlindNote;
use App\Entities\Filial\FilialNote;
use App\Entities\Filial\FilialNoteItems;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ImportFilialBlindNotesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        Log::info('Comando de importação de notas de entrada iniciado - Filiais');

        try {
            $blindNotes = FilialBlindNote::all();

            if($blindNotes) {
                $blindNotes->each(function ($blindNote) {
                    $note = FilialNote::updateOrCreate(
                        ['cod_provider'  => $blindNote->cod_fornecedor, 'nfe' => $blindNote->numero_movimento],
                        [
                            'input_date'    => $blindNote->data_entrada,
                            'status_argos'  => $blindNote->status_argos,
                            'name_provider' => ($blindNote->nome_fornecedor <> $blindNote->nome_almoxarifado ? $blindNote->nome_almoxarifado : $blindNote->nome_fornecedor),
                            'moviment_order' => $blindNote->ordem_movimento,
                            'operation'     => $blindNote->operacao,
                            'total'         => $blindNote->total,
                            'company_id'    => Company::getCompanyCode($blindNote->cod_filial)
                        ]
                    );

                    // ==== aleteração para caso de notas com itens repetidos em diferentes linhas
                    $blindNoteItems = $blindNote->items();

                    //Coleta os codigos de materias e soma caso sejam repetidos
                    $collected = $blindNoteItems->groupBy(function ($item, $key) {
                        return $item->cod_mat;
                    })->map(function ($item, $key) {
                        return $item->sum('quantidade');
                    });

                    $blindNoteItems = $blindNoteItems->unique('cod_mat');

                    //altera as quantidades se houver soma com outras linhas, para evitar problemas com o updateOrCreate abaixo
                    $blindNoteItems->transform(function ($data, $key) use ($collected) {
                        if($collected[$data->cod_mat]){
                            $data->quantidade = $collected[$data->cod_mat];
                        }
                        return $data;
                    });
                    // ===============

                    $blindNoteItems->each(function ($blindNoteItem) use ($note) {

                        $item = FilialNoteItems::updateOrCreate(
                            ['note_id' => $note->id, 'cod_mat' => $blindNoteItem->cod_mat],
                            [
                                'moviment_order' => $blindNoteItem->ordem_movimento,
                                'quantity'       => (float) $blindNoteItem->quantidade,
                                'quantity_nfe'   => (float) $blindNoteItem->quantidade_faturada,
                                'factor'         => (int) $blindNoteItem->fator,
                                'unity'          => $blindNoteItem->unidade,
                            ]
                        );

                    });

                    //UPDATE DE STATUS DA CONFERENCIA NO ARGOS:
                    $blindNote->updateBlindNoteConferenceStatus(FilialBlindNote::STATUS_NAO_CONFERIDO);
                });
            }

            Log::info('Comando de importação de notas - Filiais  - secesso');

        } catch (\Exception $ex) {
            Log::error('Comando de importação de notas - Filiais - falha: ' . $ex->getMessage());
        }
    }
}
