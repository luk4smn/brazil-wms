<?php


namespace App\Entities;


use App\Entities\Filial\FilialNote;
use App\Entities\Filial\FilialRecount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reports extends Model
{

    public static function getSupplyCompleted(): array
    {
        $collection = [];
        $collection['labels'] = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
        $collection['data']   = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


        $data = SupplyList::selectRaw('COUNT(id) as value, MONTH(created_at) as month')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->where('status', SupplyList::STATUS_COMPLETED)
            ->whereYear('created_at','=', now()->year)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        foreach ($data as $item){
            $collection['data'][($item->month -1)] = $item->value;
        }

        return $collection;
    }

    public static function getNotesThisWeek(): array
    {
        $collection = [];
        $collection['labels'] = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"];
        $collection['data']   = [0, 0, 0, 0, 0, 0, 0];


        $data = Note::selectRaw('COUNT(id) as value, DatePart(WEEKDAY, input_date) as day')
            ->where('company_id', auth()->user()->company_id)
            ->whereDate('input_date','>', now()->subWeek())
            ->whereDate('input_date','<=', now())
            ->groupBy(DB::raw('DatePart(WEEKDAY, input_date)'))
            ->get();

        foreach ($data as $item){
            if($item->day <= (now()->dayOfWeek +1)){
                $collection['data'][($item->day -1)] = $item->value;
            }
        }

        return $collection;
    }

    public static function getFilialNotesThisWeek(): array
    {
        $collection = [];
        $collection['labels'] = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"];
        $collection['data']   = [0, 0, 0, 0, 0, 0, 0];


        $data = FilialNote::selectRaw('COUNT(id) as value, DatePart(WEEKDAY, input_date) as day')
            ->where('company_id', auth()->user()->company_id)
            ->whereDate('input_date','>', now()->subWeek())
            ->whereDate('input_date','<=', now())
            ->groupBy(DB::raw('DatePart(WEEKDAY, input_date)'))
            ->get();



        foreach ($data as $item){
            if($item->day <= (now()->dayOfWeek +1)){
                $collection['data'][($item->day -1)] = $item->value;
            }
        }

        return $collection;
    }

    public static function getRecountCompleted(): array
    {
        $collection = [];
        $collection['labels'] = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
        $collection['data']   = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


        $data = Recount::selectRaw('COUNT(id) as value, MONTH(created_at) as month')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->where('status', Recount::STATUS_COMPLETED)
            ->whereYear('created_at','=', now()->year)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        $filialData = FilialRecount::selectRaw('COUNT(id) as value, MONTH(created_at) as month')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->where('status', FilialRecount::STATUS_COMPLETED)
            ->whereYear('created_at','=', now()->year)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();


        foreach ($data as $item){
            $collection['data'][($item->month -1)] = $item->value;
        }

        foreach ($filialData as $item){
            $collection['data'][($item->month -1)] += $item->value;
        }

        return $collection;
    }

}
