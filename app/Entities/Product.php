<?php


namespace App\Entities;


use App\Helper\Helper;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Product extends Model
{
    protected $table = 'product';

    protected $appends = ['product_local'];

    protected $fillable = [
        'cod_mat',
        'cod_aux',
        'codebar',
        'descricao',
        'unidade_entrada',
        'unidade_saida',
        'fator_saida',
        'referencia',
        'cod_fornecedor',
        'nome_fornecedor',
        'estoque_total',
        'estoque_matriz',
        'estoque_inartel',
        'estoque_bayeux',
        'estoque_bayeux_jp',
        'estoque_jp',
        'estoque_loja_avaria',
        'preco_venda',
        'user_field1',
        'user_field2',
        'user_field3',
        'cost',
        'product_local'
    ];

    public function getCodebarAttribute()
    {
        return $this->productCodeBar->where('status', 'A')->first();
    }

    public function productCodeBar(): HasMany
    {
        return $this->hasMany(ProductCodeBar::class, 'cod_mat', 'cod_mat');
    }

    public function getProductLocalAttribute()
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        $local = match ((int) $user->company_id) {
            Company::WL_MATRIZ => $this->user_field1,
            Company::BRAZIL_JP => $this->user_field2,
            default => throw new Exception('Unexpected value'),
        };

        return $local ?? '';
    }

    public static function getLocalFieldName($companyId): string
    {
        return match ((int) $companyId) {
            Company::WL_MATRIZ => 'user_field1',
            Company::BRAZIL_JP => 'user_field2',
            default => throw new Exception('Unexpected value'),
        };
    }

    public static function getRealFieldName($companyId): string
    {
        return match ((int) $companyId) {
            Company::WL_MATRIZ => 'CAMPOLIVREA1',
            Company::BRAZIL_JP => 'CAMPOLIVREA2',
            default => throw new Exception('Unexpected value'),
        };
    }

    public function getCostAttribute()
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        $codAlmox = match ((int) $user->company_id) {
            Company::WL_MATRIZ => Company::CODIGOALMOXARIFADO['matriz'],
            Company::BRAZIL_JP => Company::CODIGOALMOXARIFADO['jp'],
            default => throw new Exception('Unexpected value'),
        };

        $sql = "SELECT top 1 ULTIMOCUSTO
	        FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO A
	        INNER JOIN Brazil_Inform.dbo.CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL
	        WHERE CODIGOALMOXARIFADO IN {$codAlmox}
	        AND B.CODIGOMATERIAL = '{$this->cod_mat}'";

        $data = DB::select($sql);

        return collect($data)->first()->ULTIMOCUSTO ?? 0.00;
    }

    public function getProductQuantityRoleAttribute(): float|int
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        $codAlmox = match ((int) $user->company_id) {
            Company::WL_MATRIZ => Company::CODIGOALMOXARIFADO['matriz_isolated'],
            Company::BRAZIL_JP => Company::CODIGOALMOXARIFADO['jp'],
            default => throw new Exception('Unexpected value'),
        };

        $sql = "
            SELECT
            (
                SELECT ISNULL(SUM(MOVIMENTOSALDO.QUANTIDADE * MOVIMENTOSALDO.FATOR), 0)
                FROM Brazil_Inform.dbo.MOVIMENTOSALDO
                WHERE
                    CODIGOMATERIAL = '{$this->cod_mat}'
                    AND CODIGOALMOXARIFADO IN {$codAlmox}
                    AND saldo = '1'
                    AND ACAO = '1'
                    AND data = (
                        SELECT TOP 1 *
                        FROM (
                            SELECT TOP 2 data
                            FROM Brazil_Inform.dbo.MOVIMENTOSALDO
                            WHERE
                                CODIGOMATERIAL = '{$this->cod_mat}'
                                AND CODIGOALMOXARIFADO IN {$codAlmox}
                                AND saldo = '1'
                                AND ACAO = '1'
                            ORDER BY DATA DESC
                        ) x
                        ORDER BY data ASC
                    )
            ) -
            (
                SELECT ISNULL(SUM(MOVIMENTOSALDO.QUANTIDADE * MOVIMENTOSALDO.FATOR), 0)
                FROM Brazil_Inform.dbo.MOVIMENTOSALDO
                WHERE
                    CODIGOMATERIAL = '{$this->cod_mat}'
                    AND CODIGOALMOXARIFADO IN {$codAlmox}
                    AND saldo = '1'
                    AND ACAO = '-1'
                    AND data = (
                        SELECT TOP 1 *
                        FROM (
                            SELECT TOP 2 data
                            FROM Brazil_Inform.dbo.MOVIMENTOSALDO
                            WHERE
                                CODIGOMATERIAL = '{$this->cod_mat}'
                                AND CODIGOALMOXARIFADO IN {$codAlmox}
                                AND saldo = '1'
                                AND ACAO = '-1'
                            ORDER BY DATA DESC
                        ) x
                        ORDER BY data ASC
                    )
            ) AS QTD";

        // Realiza a consulta com bindings para evitar injeção de SQL
        $data = DB::select($sql);

        // Retorna o valor calculado ou 0 se não houver resultado
        return (float) (collect($data)->first()->QTD ?? 0);
    }

    public static function getLocalByCompany($codMat, $companyId, $product = null)
    {
        $field = self::getLocalFieldName($companyId ?? auth()->user()->company_id);

        if ($product) {
            return $product[$field] ?? null;
        } else {
            $result = Product::where('cod_mat', $codMat)->first($field);
            $result = $result ? $result->toArray() : null;
            return $result ? $result[$field] : null;
        }
    }

    public static function collectProductsByLocal(array $items): Collection
    {
        return collect($items)->map(fn($item, $key) => $item)->sortBy('product_local');
    }

    public function getFormattedResponse(): array
    {
        return [
            'cod_mat'           => $this->cod_mat ?? '',
            'cod_aux'           => $this->cod_aux ?? '',
            'descricao'         => Helper::formatString($this->descricao),
            'unidade_entrada'   => Helper::formatString($this->unidade_entrada),
            'unidade_saida'     => Helper::formatString($this->unidade_saida),
            'fator_saida'       => Helper::formatFloat($this->fator_saida),
            'referencia'        => $this->referencia ?? '',
            'cod_fornecedor'    => $this->cod_fornecedor ?? '',
            'nome_fornecedor'   => Helper::formatString($this->nome_fornecedor),
            'estoque_total'     => Helper::formatFloat($this->estoque_total),
            'estoque_matriz'    => Helper::formatFloat($this->estoque_matriz),
            'estoque_inartel'   => Helper::formatFloat($this->estoque_inartel),
            'estoque_bayeux'    => Helper::formatFloat($this->estoque_bayeux),
            'estoque_bayeux_jp' => Helper::formatFloat($this->estoque_bayeux_jp),
            'estoque_jp'        => Helper::formatFloat($this->estoque_jp),
            'estoque_loja_avaria' => Helper::formatFloat($this->estoque_loja_avaria),
            'preco_venda'       => Helper::formatFloat($this->preco_venda),
            'user_field1'       => str_replace(array("\r\n", "\n"), '', $this->user_field1 ?? ''),
            'user_field2'       => str_replace(array("\r\n", "\n"), '', $this->user_field2 ?? ''),
            'user_field3'       => Helper::formatFloat($this->user_field3),
            'codebar'           => $this->codebar->cod_bar ?? '',
            'cost'              => Helper::formatFloat($this->cost),
            'product_local'     => str_replace(array("\r\n", "\n"), '', $this->product_local ?? ''),
            'product_quantity_role' => Helper::formatFloat($this->product_quantity_role),
            'entry_report_item' => $this->entry_report_item ?? null,
            'product_code_bar'  => Helper::formatArray($this->productCodeBar , 'cod_bar')
        ];
    }

    public function mergeWithItem($item, $keyName): self
    {
        $this->{$keyName} = $item->makeHidden(['product', 'cod_mat', 'cod_aux']);
        return $this;
    }

}
