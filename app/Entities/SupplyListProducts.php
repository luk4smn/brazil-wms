<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class SupplyListProducts extends Model
{
    protected $table = 'supply_list_products';

    protected $fillable = [
        'last_insert_id',
        'supply_list_id',
        'cod_aux',
        'quantity',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'cod_aux', 'cod_aux');
    }

    public static function collectProductsByProvider($items): Collection
    {
        $items = collect($items)->map(function ($item, $key) {
            $item['product'] = Product::where('cod_aux', $item['cod_aux'])->first();

            $item['local'] = match ((int) auth()->user()->company_id) {
                Company::WL_MATRIZ => $item['product']->user_field1,
                Company::BRAZIL_JP => $item['product']->user_field2,
                default => $item['product']->user_field1,
            };

            $item['provider'] = $item['product']['nome_fornecedor'];
            return $item;
        })->sortBy('local');

        return $items->groupBy('provider');
    }

    public static function collectProductsByLocal($items): Collection
    {
        return collect($items)->map(function ($item, $key) {
            $item['product'] = Product::where('cod_aux', $item['cod_aux'])->first();

            $item['local'] = match ((int) auth()->user()->company_id) {
                Company::WL_MATRIZ => $item['product']->user_field1 ?? '',
                Company::BRAZIL_JP => $item['product']->user_field2 ?? '',
                default => $item['product']->user_field1 ?? '',
            };

            return $item;
        })->sortBy('local');
    }

    public static function multiListCollectProductsByLocal($ids)
    {
        $items = SupplyListProducts::whereIn('supply_list_id', $ids)->get();

        return $items->map(function ($item, $key) {
            $output = $item->toArray();

            $output['local'] = match ((int) auth()->user()->company_id) {
                Company::WL_MATRIZ => $item->product->user_field1 ?? "",
                Company::BRAZIL_JP => $item->product->user_field2 ?? "",
                default => $item->product->user_field1 ?? "",
            };

            $output['product'] = $item->product ?? "";
            !$item->product ?: $output['product']['codebar'] = $item->product->codebar['cod_bar'] ?? '';

            return $output;
        })->sortBy('local');
    }
}
