<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class BlindNoteItems extends Model
{
    protected $table = 'blind_note_items';

    protected $fillable = [
        'ordem_movimento',
        'cod_mat',
        'quantidade',
        'quantidade_faturada',
        'unidade',
        'fator'
    ];

    public function blindNote()
    {
        return BlindNote::where('ordem_movimento', $this['ordem_movimento'])->get();
    }

}
