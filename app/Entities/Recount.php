<?php


namespace App\Entities;


use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Recount extends Entity
{
    protected $table = 'recounts';

    protected $appends = ['usernames', 'provider_names', 'nfes'];

    protected $fillable = [
        'user_id',
        'status',
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_COMPLETED   = 3;
    protected static $status = [
        self::STATUS_PENDING     => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Conferindo',
        self::STATUS_COMPLETED   => 'Concluído'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'recount_users', 'recount_id', 'user_id');
    }

    public function entryReport(): HasOne
    {
        return $this->hasOne(EntryReport::class, 'reference_id', 'id')
            ->where('table', self::class);
    }

    public function notes(): HasMany
    {
        return $this->hasMany(Note::class, 'recount_id');
    }

    public function totalNoteItems(): HasManyThrough
    {
        return $this->hasManyThrough(NoteItems::class, Note::class, 'recount_id', 'note_id', 'id', 'id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(RecountProducts::class, 'recount_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(RecountItems::class, 'recount_id');
    }

    public function getUsernamesAttribute(): string
    {
        $userNames = $this->users->pluck('name')->toArray();
        return implode(', ', $userNames) ?? '';
    }

    public function getProviderNamesAttribute(): string
    {
        $names = $this->notes
            ->pluck('name_provider')  // Extração direta de valores
            ->unique()                // Remove duplicados
            ->filter();               // Remove valores nulos ou vazios (opcional)

        // Junta os nomes com o separador " - " diretamente
        return $names->implode(' - ');
    }

    public function getNfesAttribute(): string
    {
        $nfes = $this->notes
            ->pluck('nfe')            // Extração direta de valores
            ->unique()                // Remove duplicados
            ->filter();               // Remove valores nulos ou vazios (opcional)

        // Junta os nomes com o separador " - " diretamente
        return $nfes->implode(', ');
    }

    public static function getStatusLabel(int $status): string
    {
        return self::$status[$status] ?? 'Desconhecido';
    }

    public function getStatusLabelAttribute(): string
    {
        return self::getStatusLabel((int) $this->status);
    }

    public static function getCodebar($cod_mat)
    {
        return ProductCodebar::where('cod_mat', $cod_mat)
            ->value('cod_bar') ?? $cod_mat;
    }

}
