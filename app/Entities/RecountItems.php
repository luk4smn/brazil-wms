<?php


namespace App\Entities;


class RecountItems extends Entity
{
    protected $table='recount_items';

    protected $fillable=[
        'cod_mat',
        'volumes',
        'quantity_confer',
        'quantity_damage',
        'recount_id',
        'user_id'
    ];

}
