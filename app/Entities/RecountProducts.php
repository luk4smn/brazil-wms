<?php


namespace App\Entities;


use App\Helper\Helper;
use App\User;

class RecountProducts extends Product
{
    protected $table = 'recount_products';

    protected $fillable = [
        'cod_mat',
        'cod_aux',
        'codebar',
        'unity',
        'quantity',
        'recount_id',
        'description',
        'reference',
        'unity_exit',
        'total_quantity_confer',
        'total_quantity_damage',
        'total_volumes',
        'estoque_matriz',
        'estoque_inartel',
        'estoque_bayeux',
        'estoque_bayeux_jp',
        'estoque_loja_avaria',
        'estoque_jp',
        'user_ids'
    ];

    public function getFormattedResponse(): array
    {
        return [
            'id'            => $this->id,
            'cod_mat'       => $this->cod_mat ?? '',
            'cod_aux'       => $this->cod_aux ?? '',
            'quantity'      => (int) $this->quantity ?? 0,
            'description'   => Helper::formatString($this->description),
            'reference'     => $this->reference ?? '',
            'unity'         => Helper::formatString($this->unity),
            'unity_exit'    => Helper::formatString($this->unity_exit),
            'estoque_total'       => Helper::formatFloat($this->estoque_total),
            'estoque_matriz'      => Helper::formatFloat($this->estoque_matriz),
            'estoque_inartel'     => Helper::formatFloat($this->estoque_inartel),
            'estoque_bayeux'      => Helper::formatFloat($this->estoque_bayeux),
            'estoque_bayeux_jp'   => Helper::formatFloat($this->estoque_bayeux_jp),
            'estoque_jp'          => Helper::formatFloat($this->estoque_jp),
            'estoque_loja_avaria' => Helper::formatFloat($this->estoque_loja_avaria),
            'total_quantity_confer' => Helper::formatFloat($this->total_quantity_confer),
            'total_quantity_damage' => Helper::formatFloat($this->total_quantity_damage),
            'total_volumes'         => Helper::formatFloat($this->total_volumes),
            'product_code_bar'  => Helper::formatArray($this->productCodeBar , 'cod_bar')
        ];
    }

    public static function getUsersNames($item): array
    {
        if (empty($item['user_ids'])) {
            return [];
        }

        // Explode a string de IDs e remove duplicatas
        $userIds = explode(', ', $item['user_ids']);
        $uniqueUserIds = array_unique($userIds);

        // Buscar os nomes dos usuários no banco
        $userNames = User::whereIn('id', $uniqueUserIds)->pluck('name')->toArray();

        // Garantir que os nomes sejam únicos e ordenados
        $uniqueUserNames = array_unique($userNames);
        sort($uniqueUserNames);

        return $uniqueUserNames;
    }

}
