<?php


namespace App\Entities\Filial;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FilialBlindNote extends Model
{
    protected $table = 'filial_blind_note';

    protected $fillable = [
        'ordem_movimento',
        'numero_movimento',
        'data_entrada',
        'status_argos',
        'status_wms',
        'cod_fornecedor',
        'nome_fornecedor',
        'nome_almoxarifado',
        'operacao',
        'cod_filial',
        'total',
    ];

    const
        STATUS_NAO_CONFERIDO = "Não Conferido",
        STATUS_EM_CONFERENCIA = "Em conferência",
        STATUS_CONFERIDO = "Conferido";

    public function items()
    {
        return FilialBlindNoteItems::where('ordem_movimento', $this['ordem_movimento'])->get();
    }

    public function updateBlindNoteConferenceStatus($status): void
    {
        $sql = "UPDATE Brazil_Inform.dbo.MOVIMENTO
                SET Brazil_Inform.dbo.MOVIMENTO.CAMPOLIVREA1='".$status."'
                WHERE Brazil_Inform.dbo.MOVIMENTO.ORDEMMOVIMENTO='".$this->attributes['ordem_movimento']."' ";

        DB::connection('sqlsrv')->statement($sql);
    }

}
