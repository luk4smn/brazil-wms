<?php


namespace App\Entities\Filial;


use App\Entities\Entity;

class FilialRecountItems extends Entity
{
    protected $table='filial_recount_items';

    protected $fillable=[
        'cod_mat',
        'volumes',
        'quantity_confer',
        'quantity_damage',
        'recount_id',
        'user_id'
    ];

}
