<?php


namespace App\Entities\Filial;


use App\Entities\Company;
use App\Entities\Entity;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class FilialNote extends Entity
{
    protected $table = 'filial_notes';

    protected $fillable = [
        'moviment_order',
        'nfe',
        'cod_provider',
        'name_provider',
        'input_date',
        'status_argos',
        'total',
        'status',
        'volumes',
        'operation',
        'recount_id',
        'company_id',
        'origin_moviment'
    ];

    protected $dates = ['input_date'];

    const
        ARGOS_STATUS_NAO_CONFERIDO  = "Não Conferido",
        ARGOS_STATUS_EM_CONFERENCIA = "Em conferência",
        ARGOS_STATUS_CONFERIDO      = "Conferido";

    public function items(): HasMany
    {
        return $this->hasMany(FilialNoteItems::class, 'note_id');
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function recount(): BelongsTo
    {
        return $this->belongsTo(FilialRecount::class, 'recount_id', 'id');
    }

    public function getStatusAttribute()
    {
        return $this->recount->status ?? FilialRecount::STATUS_PENDING;
    }

    public function getStatusLabelAttribute()
    {
        return $this->recount->status_label ?? FilialRecount::getStatusLabel(FilialRecount::STATUS_PENDING);
    }

    public function getItems()
    {
       return $this->items->each(function ($item, $key){
            $item->description =  $item->product()->descricao ?? '';
            $item->cod_aux =  $item->product()->cod_aux ?? '';
            return $item;
        });
    }

    public function updateNoteConferenceStatus($status): void
    {
        $sql = "UPDATE Brazil_Inform.dbo.MOVIMENTO
                SET Brazil_Inform.dbo.MOVIMENTO.CAMPOLIVREA1='{$status}'
                WHERE Brazil_Inform.dbo.MOVIMENTO.ORDEMMOVIMENTO='{$this->attributes['moviment_order']}' ";

        DB::connection('sqlsrv')->statement($sql);
    }

}
