<?php


namespace App\Entities\Filial;


use App\Entities\Product;
use App\Helper\Helper;

class FilialRecountProducts extends Product
{
    protected $table = 'filial_recount_products';

    protected $fillable = [
        'cod_mat',
        'cod_aux',
        'unity',
        'quantity',
        'recount_id',
        'description',
        'reference',
        'unity_exit',
        'total_quantity_confer',
        'total_quantity_damage',
        'total_volumes',
        'estoque_matriz',
        'estoque_inartel',
        'estoque_bayeux',
        'estoque_bayeux_jp',
        'estoque_loja_avaria',
        'estoque_jp',
        'user_ids'
    ];

    public function getFormattedResponse(): array
    {
        return [
            'id'            => $this->id,
            'cod_mat'       => $this->cod_mat ?? '',
            'cod_aux'       => $this->cod_aux ?? '',
            'quantity'      => (int) $this->quantity ?? 0,
            'description'   => Helper::formatString($this->description),
            'reference'     => $this->reference ?? '',
            'unity'         => Helper::formatString($this->unity),
            'unity_exit'    => Helper::formatString($this->unity_exit),
            'estoque_total'       => Helper::formatFloat($this->estoque_total),
            'estoque_matriz'      => Helper::formatFloat($this->estoque_matriz),
            'estoque_inartel'     => Helper::formatFloat($this->estoque_inartel),
            'estoque_bayeux'      => Helper::formatFloat($this->estoque_bayeux),
            'estoque_bayeux_jp'   => Helper::formatFloat($this->estoque_bayeux_jp),
            'estoque_jp'          => Helper::formatFloat($this->estoque_jp),
            'estoque_loja_avaria' => Helper::formatFloat($this->estoque_loja_avaria),
            'total_quantity_confer' => Helper::formatFloat($this->total_quantity_confer),
            'total_quantity_damage' => Helper::formatFloat($this->total_quantity_damage),
            'total_volumes'         => Helper::formatFloat($this->total_volumes),
            'diff'                  => round(($this->total_quantity_confer ?? 0 - $this->quantity ?? 0) ,2),
            'product_code_bar'      => Helper::formatArray($this->productCodeBar,'cod_bar')
        ];
    }

}
