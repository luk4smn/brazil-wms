<?php


namespace App\Entities\Filial;


use App\Entities\Entity;
use App\Entities\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FilialNoteItems extends Entity
{
    protected $table = 'filial_note_items';

    protected $fillable = [
        'moviment_order',
        'cod_mat',
        'quantity',
        'factor',
        'quantity_nfe',
        'unity',
        'note_id',
    ];

    public function note(): BelongsTo
    {
        return $this->belongsTo(FilialNote::class, 'note_id');
    }

    public function product(){
        return Product::where('cod_mat', $this['cod_mat'])->get()->first();
    }

}
