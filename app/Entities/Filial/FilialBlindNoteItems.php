<?php


namespace App\Entities\Filial;


use Illuminate\Database\Eloquent\Model;

class FilialBlindNoteItems extends Model
{
    protected $table = 'filial_blind_note_items';

    protected $fillable = [
        'ordem_movimento',
        'cod_mat',
        'quantidade',
        'quantidade_faturada',
        'unidade',
        'fator'
    ];

    public function blindNote()
    {
        return FilialBlindNote::where('ordem_movimento', $this['ordem_movimento'])->get();
    }

}
