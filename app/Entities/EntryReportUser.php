<?php

namespace App\Entities;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;

class EntryReportUser extends Model
{
    use Searchable;

    protected $fillable = [
        'entry_report_id',
        'user_id',
        'role',
    ];

    const ROLE_SEPARATOR = 1, ROLE_RECEIVER  = 2;

    protected static array $locals = ['BRINQUEDOS', 'ÉPOCAS', 'EXPANSÃO', 'PAPELARIA', 'UTILIDADES', 'VARIEDADES', 'OUTRO'];

    protected static array $roles = [
        self::ROLE_SEPARATOR => 'Separador',
        self::ROLE_RECEIVER  => 'Recebedor',
    ];

}
