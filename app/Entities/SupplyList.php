<?php


namespace App\Entities;


use App\Helper\Helper;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SupplyList extends Entity
{
    protected $table = 'supply_list';

    const
        STATUS_TYPING = 1,
        STATUS_PENDING = 2,
        STATUS_IN_PROGRESS = 3,
        STATUS_COMPLETED = 4;
    const
        TYPE_1 = 'REPOSIÇÃO',
        TYPE_2 = 'FORA DE LOJA',
        TYPE_3 = 'PEDIDO',
        TYPE_4 = 'OUTRO';

    protected static $status = [
        self::STATUS_TYPING => 'Digitando',
        self::STATUS_PENDING => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Separação',
        self::STATUS_COMPLETED => 'Concluído'
    ];

    protected $fillable = [
        'local',
        'status',
        'user_id',
        'separator_id',
        'notes',
        'type',
        'initiated_at',
        'completed_at'
    ];

    protected $casts = [
        'initiated_at' => 'datetime',
        'completed_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public static function getStatus(): array
    {
        return self::$status;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function separator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'separator_id', 'id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(SupplyListItems::class, 'supply_list_id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(SupplyListProducts::class, 'supply_list_id');
    }

    public static function getStatusLabel(int $status): string
    {
        return self::$status[$status] ?? 'Desconhecido';
    }

    public function getStatusLabelAttribute(): string
    {
        return self::getStatusLabel((int) $this->status);
    }

    public function getCustomTimestamp($date)
    {
        return $date ? $date->format('Y-m-d\TH:i') : '';
    }

    public function getWorktime()
    {
        if ($this->completed_at && $this->initiated_at) {
            return $this->completed_at->diffInMinutes($this->initiated_at);
        } else {
            return 'N/D';
        }
    }

    public function getFormattedResponse(): array
    {
        return [
            'id' => $this->id,
            'local' => (string) $this->local,
            'type'  => (string) $this->type,
            'status' => (string) $this->status,
            'user_id' => (string) $this->user_id,
            'updated_at' => $this->serializeDate(Helper::getDate($this->updated_at)),
            'created_at' => $this->serializeDate(Helper::getDate($this->created_at)),
            'initiated_at' => $this->initiated_at,
            'completed_at' => $this->completed_at,
        ];
    }

}
