<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BlindNote extends Model
{
    protected $table = 'blind_note';

    protected $fillable = [
        'ordem_movimento',
        'numero_movimento',
        'data_entrada',
        'status_argos',
        'status_wms',
        'cod_fornecedor',
        'nome_fornecedor',
        'operacao',
        'cod_filial',
        'total',
        'movimento_origem',
    ];

    const
        STATUS_NAO_CONFERIDO = "Não Conferido",
        STATUS_EM_CONFERENCIA = "Em conferência",
        STATUS_CONFERIDO = "Conferido";

    public function items()
    {
        return BlindNoteItems::where('ordem_movimento', $this['ordem_movimento'])->get();
    }

    public function getMovimentoOrigemAttribute(): ?string
    {
        $queryResult = $this->findOriginalMoviment($this->attributes['ordem_movimento']);
        $lastResult = null;

        while($queryResult <> null){
            $lastResult  = $queryResult;
            $queryResult = $this->findOriginalMoviment($queryResult->ORDEMMOVIMENTO);
        }

        if($lastResult && ($this->attributes['cod_fornecedor'] <> trim($lastResult->CODIGOPARCEIRO))){
            return trim($lastResult->NUMEROMOVIMENTO).' - '.$lastResult->DESCRICAO;
        }

        return null;
    }

    public function findOriginalMoviment($order)
    {
        $data =  DB::select("
            SELECT
                DISTINCT D.ORDEMMOVIMENTO, E.NUMEROMOVIMENTO, F.DESCRICAO, F.CODIGOPARCEIRO
            FROM Brazil_Inform.dbo.MOVIMENTO A
            INNER JOIN Brazil_Inform.dbo.MOVIMENTOITEM B ON B.ORDEMMOVIMENTO = A.ORDEMMOVIMENTO
            INNER JOIN Brazil_Inform.dbo.MOVIMENTOTRANSFORMACAO C ON B.NUMEROITEM = C.NUMEROITEMDESTINO
            INNER JOIN Brazil_Inform.dbo.MOVIMENTOITEM D ON D.NUMEROITEM = C.NUMEROITEMORIGEM
            INNER JOIN Brazil_Inform.dbo.MOVIMENTO E ON E.ORDEMMOVIMENTO = D.ORDEMMOVIMENTO
            INNER JOIN Brazil_Inform.dbo.CADPARCEIRO F ON F.CODIGOPARCEIRO = A.CODIGOPARCEIRO
            WHERE A.ORDEMMOVIMENTO IN ('0', $order)
            AND E.STATUS <> 'X'
            AND E.CODIGOOPERACAO NOT IN ('1101')
            ORDER BY D.ORDEMMOVIMENTO
        ");

        return collect($data)->first();
    }

    public function updateBlindNoteConferenceStatus($status): void
    {
        $sql = "UPDATE Brazil_Inform.dbo.MOVIMENTO
                SET Brazil_Inform.dbo.MOVIMENTO.CAMPOLIVREA1='{$status}'
                WHERE Brazil_Inform.dbo.MOVIMENTO.ORDEMMOVIMENTO='{$this->attributes['ordem_movimento']}' ";

        DB::connection('sqlsrv')->statement($sql);
    }


}
