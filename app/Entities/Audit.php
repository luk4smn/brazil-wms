<?php


namespace App\Entities;

use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Audit extends Entity
{
    protected $table = 'audits';

    protected $fillable = [
        'description',
        'table_name',
        'record_id',
        'action',
        'changes',
        'obs',
        'user_id',
    ];

    protected $casts = [
        'changes' => 'array', // Decodifica o JSON automaticamente para array associativo
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getTranslatedActionAttribute(): string
    {
        $translations = [
            'create' => 'Criar',
            'update' => 'Atualizar',
            'delete' => 'Excluir',
            'restore' => 'Restaurar',
            'force_delete' => 'Excluir Permanentemente',
        ];

        return $translations[$this->action] ?? ucfirst($this->action);
    }

}
