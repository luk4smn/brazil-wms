<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SupplyListItems extends Entity
{
    protected $table = 'supply_list_items';

    protected $fillable = [
        'supply_list_id',
        'cod_aux',
        'quantity',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'cod_aux', 'cod_aux');
    }

}
