<?php


namespace App\Entities;


use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Entity
{
    protected $table = 'companies';

    protected $fillable = [
        'name',
        'fantasy_name',
        'cnpj',
        'quantity_role'
    ];

    const
        WL_MATRIZ = 1,
        BRAZIL_JP = 2;

    // Filial para importação de nota cega (OBS: Operações novas tem que ser inclusas na view)
    const FILIALS = [
        '00001' => self::WL_MATRIZ,
        '00002' => self::WL_MATRIZ,
        '00003' => self::WL_MATRIZ,
        '00004' => self::WL_MATRIZ,
        '00005' => self::WL_MATRIZ,
        '10001' => self::BRAZIL_JP,
        '10002' => self::BRAZIL_JP,
        '10010' => self::BRAZIL_JP,
    ];

    // Codigos de almoxarifado para obter ultimo custo do produto
    const CODIGOALMOXARIFADO = [
        'matriz'  => '(00001, 00002, 00003, 00004, 00005)',
        'bayeux'  => '(00011, 00012)',
        'jp'      => '(10001, 10010, 10019)',
        'bayeux_jp' => '(10002)',
        'matriz_isolated' => '(00001)',
    ];

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'company_id');
    }

    public static function getCompanyCode($filialCode): int
    {
        return self::FILIALS[$filialCode] ?? self::WL_MATRIZ;
    }

}
