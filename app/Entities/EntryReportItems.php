<?php

namespace App\Entities;

use App\Traits\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EntryReportItems extends Model
{
    use Searchable;

    //protected $dateFormat = 'd-m-Y H:i:s';

    protected $table = 'entry_report_items';

    protected $appends = ['status_label'];

    protected $fillable = [
        'cod_mat',
        'cod_aux',
        'status',
        'local',
        'entry_report_id',
        'separator_id',
        'receiver_id',
        'issue_flag'
    ];

    const
        STATUS_NOT_FOUND    = 1,
        STATUS_FOUND        = 2,
        STATUS_FORWARD      = 3,
        STATUS_NOT_RECEIVED = 4,
        STATUS_RECEIVED     = 5;

    protected static array $status = [
        self::STATUS_NOT_FOUND    => 'Não Encontrado Estoque',
        self::STATUS_FOUND        => 'Encontrado Estoque',
        self::STATUS_FORWARD      => 'Encaminhado Recebimento',
        self::STATUS_NOT_RECEIVED => 'Não Recebido Loja',
        self::STATUS_RECEIVED     => 'Recebido Loja',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'cod_aux', 'cod_aux');
    }

    public function separator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'separator_id', 'id');
    }

    public function receiver(): BelongsTo
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public static function getStatusLabel(int $status): string
    {
        return self::$status[$status] ?? 'Desconhecido';
    }

    public function getStatusLabelAttribute(): string
    {
        return self::getStatusLabel((int) $this->status);
    }

}
