<?php

namespace App\Entities;

use App\Entities\Filial\FilialRecount;
use App\Traits\Searchable;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class EntryReport extends Model
{
    use Searchable;

    //protected $dateFormat = 'd-m-Y H:i:s';

    protected $table = 'entry_reports';

    protected $appends = ['status_label', 'table_label', 'usernames'];

    protected $fillable = [
        'table',
        'reference_id',
        'status',
        'observation',
        'type'
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_FORWARD     = 3,
        STATUS_RECEIVING   = 4,
        STATUS_COMPLETED   = 5,
        STATUS_COMPLETED_WITH_ISSUES = 6,
        STATUS_CANCELLED   = 7;

    const
        TYPE_ESTOQUE = 1,
        TYPE_LOJA = 2;

    protected static array $status = [
        self::STATUS_PENDING     => 'Pendente',    //status quando é criado o registro
        self::STATUS_IN_PROGRESS => 'Separando',   //status quando o separador inicia
        self::STATUS_FORWARD     => 'Encaminhado', //Ultimo status para o tipo 1
        self::STATUS_RECEIVING   => 'Recebendo',   //status quando o recebedor inicia
        self::STATUS_COMPLETED   => 'Concluído',   //status quando o relatorio tem todos os itens checados
        self::STATUS_COMPLETED_WITH_ISSUES => 'C/Pendências' //status quando o relatório passar mais de 2 dias parado
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'entry_report_users', 'entry_report_id', 'user_id');
    }

    public function related(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'table', 'reference_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(EntryReportItems::class, 'entry_report_id');
    }

    public function itemsNotIssued(): HasMany
    {
        return $this->hasMany(EntryReportItems::class, 'entry_report_id')
            ->where('issue_flag', false);
    }

    public static function getRelatedClass($table): ?string
    {
        $tableClassMap = [
            (new Recount)->getTable() => Recount::class,
            (new FilialRecount)->getTable() => FilialRecount::class,
        ];

        return $tableClassMap[$table] ?? null;
    }

    public static function getStatusLabel(int $status): string
    {
        return self::$status[$status] ?? 'Desconhecido';
    }

    public function getStatusLabelAttribute(): string
    {
        return self::getStatusLabel((int) $this->status);
    }

    public function getTableLabelAttribute(): string
    {
        return $this['table'] == Recount::class ? "Fornecedor" : "Filial";
    }

    public function getTypeColumnAttribute(): string
    {
        return match ((int) $this->type) {
            EntryReport::TYPE_ESTOQUE   => 'separator_id',
            EntryReport::TYPE_LOJA      => 'receiver_id',
            default => throw new Exception('Unexpected value'),
        };
    }

    public function getTypeRoleAttribute(): int
    {
        return match ((int) $this->type) {
            EntryReport::TYPE_ESTOQUE   => EntryReportUser::ROLE_SEPARATOR,
            EntryReport::TYPE_LOJA      => EntryReportUser::ROLE_RECEIVER,
            default => throw new Exception('Unexpected value'),
        };
    }

    public function getUsernamesAttribute(): string
    {
        $userNames = $this->users->pluck('name')->toArray();
        return implode(', ', $userNames) ?? '';
    }

    public function updateEntryReportByType(): void
    {
        if ($this->type == self::TYPE_ESTOQUE) {
            $this->status = EntryReport::STATUS_IN_PROGRESS;
        } elseif ($this->type == self::TYPE_LOJA) {
            $this->status = EntryReport::STATUS_RECEIVING;
        } else {
            throw new Exception('Unexpected value');
        }

        $this->save();
    }

    public function finalizeEntryReportByType(): void
    {
        if ($this->type == self::TYPE_ESTOQUE) {
            $this->items->each(function ($item) {
                if (!is_null($item->{$this->type_column})) {
                    $item->status = EntryReportItems::STATUS_FORWARD;
                    $item->save();
                }
            });

        } elseif ($this->type == self::TYPE_LOJA) {
            $this->items->each(function ($item) {
                if (is_null($item->{$this->type_column})) {
                    if ($item->status == EntryReportItems::STATUS_FORWARD) {
                        $item->status = EntryReportItems::STATUS_NOT_RECEIVED;
                    }
                    $this->status = self::STATUS_COMPLETED_WITH_ISSUES;
                } else {
                    $item->status = EntryReportItems::STATUS_RECEIVED;
                }
                $item->save(); // Salva a alteração de status
            });
        } else {
            throw new Exception('Unexpected value');
        }

        $this->save(); // Salva o EntryReport com o novo status
    }

    public function rollbackEntryReportByType(): void
    {
        if ($this->type == self::TYPE_ESTOQUE && $this->status == self::STATUS_FORWARD) {
            $this->items->each(function ($item) {
                if (!is_null($item->{$this->type_column} && $item->status == EntryReportItems::STATUS_FORWARD)) {
                    $item->status = EntryReportItems::STATUS_FOUND;
                    $item->save();
                }
            });

            $this->status = self::STATUS_IN_PROGRESS;

        } elseif ($this->type == self::TYPE_LOJA && $this->status >= self::STATUS_COMPLETED) {
            $this->items->each(function ($item) {
                if ($item->status == EntryReportItems::STATUS_NOT_RECEIVED) {
                    $item->status = EntryReportItems::STATUS_FORWARD;
                    $item->local = 'ESTOQUE';
                    $item->{$this->type_column} = null;
                }

                $item->save();
            });

            $this->status = self::STATUS_RECEIVING;
        } else {
            throw new Exception('Unexpected value');
        }

        $this->save(); // Salva o EntryReport com o novo status
    }



}
