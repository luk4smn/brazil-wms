<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NoteItems extends Entity
{
    protected $table = 'note_items';

    protected $fillable = [
        'moviment_order',
        'cod_mat',
        'quantity',
        'factor',
        'quantity_nfe',
        'unity',
        'note_id',
    ];

    public function note(): BelongsTo
    {
        return $this->belongsTo(Note::class, 'note_id');
    }

    public function product(){
        return Product::where('cod_mat', $this['cod_mat'])->get()->first();
    }

}
