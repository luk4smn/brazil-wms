<?php

namespace App\Observers;

use App\Entities\Audit;
use App\Entities\Recount;


class RecountObserver
{
    /**
     * Handle the Recount "created" event.
     *
     * @param  Recount  $recount
     * @return void
     */
    public function created(Recount $recount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Nova conferência criada",
            'table_name'  => $recount->getTable(),
            'record_id'   => $recount->id,
            'action'      => 'create',
            'changes'     => json_encode(['new' => $recount->getAttributes()]),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the Recount "updated" event.
     *
     * @param  Recount  $recount
     * @return void
     */
    public function updated(Recount $recount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();
        $changes = [];
        foreach ($recount->getChanges() as $field => $newValue) {
            $originalValue = $recount->getOriginal($field);
            $changes[$field] = ['old' => $originalValue, 'new' => $newValue];
        }

        Audit::create([
            'description' => "Conferência atualizada",
            'table_name'  => $recount->getTable(),
            'record_id'   => $recount->id,
            'action'      => 'update',
            'changes'     => json_encode($changes),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the Recount "deleted" event.
     *
     * @param  Recount  $recount
     * @return void
     */
    public function deleted(Recount $recount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência removida",
            'table_name'  => $recount->getTable(),
            'record_id'   => $recount->id,
            'action'      => 'delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the Recount "restored" event.
     *
     * @param  Recount  $recount
     * @return void
     */
    public function restored(Recount $recount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência restaurada",
            'table_name'  => $recount->getTable(),
            'record_id'   => $recount->id,
            'action'      => 'restore',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the Recount "force deleted" event.
     *
     * @param  Recount  $recount
     * @return void
     */
    public function forceDeleted(Recount $recount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência excluída permanentemente",
            'table_name'  => $recount->getTable(),
            'record_id'   => $recount->id,
            'action'      => 'force_delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }
}
