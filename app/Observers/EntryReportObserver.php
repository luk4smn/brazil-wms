<?php

namespace App\Observers;


use App\Entities\Audit;
use App\Entities\EntryReport;

class EntryReportObserver
{
    /**
     * Handle the EntryReport "created" event.
     *
     * @param  EntryReport  $entryReport
     * @return void
     */
    public function created(EntryReport $entryReport): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Relatório de entrada criado",
            'table_name'  => $entryReport->getTable(),
            'record_id'   => $entryReport->id,
            'action'      => 'create',
            'changes'     => json_encode(['new' => $entryReport->getAttributes()]),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the EntryReport "updated" event.
     *
     * @param  EntryReport  $entryReport
     * @return void
     */
    public function updated(EntryReport $entryReport): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();
        $changes = [];
        foreach ($entryReport->getChanges() as $field => $newValue) {
            $originalValue = $entryReport->getOriginal($field);
            $changes[$field] = ['old' => $originalValue, 'new' => $newValue];
        }

        Audit::create([
            'description' => "Relatório de entrada atualizado",
            'table_name'  => $entryReport->getTable(),
            'record_id'   => $entryReport->id,
            'action'      => 'update',
            'changes'     => json_encode($changes),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the EntryReport "deleted" event.
     *
     * @param  EntryReport  $entryReport
     * @return void
     */
    public function deleted(EntryReport $entryReport): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Relatório de entrada removido",
            'table_name'  => $entryReport->getTable(),
            'record_id'   => $entryReport->id,
            'action'      => 'delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the EntryReport "restored" event.
     *
     * @param  EntryReport  $entryReport
     * @return void
     */
    public function restored(EntryReport $entryReport): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Relatório de entrada restaurado",
            'table_name'  => $entryReport->getTable(),
            'record_id'   => $entryReport->id,
            'action'      => 'restore',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the EntryReport "force deleted" event.
     *
     * @param  EntryReport  $entryReport
     * @return void
     */
    public function forceDeleted(EntryReport $entryReport): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Relatório de entrada excluído permanentemente",
            'table_name'  => $entryReport->getTable(),
            'record_id'   => $entryReport->id,
            'action'      => 'force_delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }
}
