<?php

namespace App\Observers;


use App\Entities\Audit;
use App\Entities\Filial\FilialRecount;

class FilialRecountObserver
{
    /**
     * Handle the FilialRecount "created" event.
     *
     * @param  FilialRecount  $filialRecount
     * @return void
     */
    public function created(FilialRecount $filialRecount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência de filial criada",
            'table_name'  => $filialRecount->getTable(),
            'record_id'   => $filialRecount->id,
            'action'      => 'create',
            'changes'     => json_encode(['new' => $filialRecount->getAttributes()]),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the FilialRecount "updated" event.
     *
     * @param  FilialRecount  $filialRecount
     * @return void
     */
    public function updated(FilialRecount $filialRecount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();
        $changes = [];
        foreach ($filialRecount->getChanges() as $field => $newValue) {
            $originalValue = $filialRecount->getOriginal($field);
            $changes[$field] = ['old' => $originalValue, 'new' => $newValue];
        }

        Audit::create([
            'description' => "Conferência de filial atualizada",
            'table_name'  => $filialRecount->getTable(),
            'record_id'   => $filialRecount->id,
            'action'      => 'update',
            'changes'     => json_encode($changes),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the FilialRecount "deleted" event.
     *
     * @param  FilialRecount  $filialRecount
     * @return void
     */
    public function deleted(FilialRecount $filialRecount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência de filial removida",
            'table_name'  => $filialRecount->getTable(),
            'record_id'   => $filialRecount->id,
            'action'      => 'delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the FilialRecount "restored" event.
     *
     * @param  FilialRecount  $filialRecount
     * @return void
     */
    public function restored(FilialRecount $filialRecount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência de filial restaurada",
            'table_name'  => $filialRecount->getTable(),
            'record_id'   => $filialRecount->id,
            'action'      => 'restore',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the FilialRecount "force deleted" event.
     *
     * @param  FilialRecount  $filialRecount
     * @return void
     */
    public function forceDeleted(FilialRecount $filialRecount): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Conferência de filial excluída permanentemente",
            'table_name'  => $filialRecount->getTable(),
            'record_id'   => $filialRecount->id,
            'action'      => 'force_delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }
}
