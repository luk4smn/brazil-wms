<?php

namespace App\Observers;

use App\Entities\Audit;
use App\Entities\SupplyList;

class SupplyListObserver
{
    /**
     * Handle the SupplyList "created" event.
     *
     * @param  SupplyList  $supplyList
     * @return void
     */
    public function created(SupplyList $supplyList): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Lista de abastecimento criada",
            'table_name'  => $supplyList->getTable(),
            'record_id'   => $supplyList->id,
            'action'      => 'create',
            'changes'     => json_encode(['new' => $supplyList->getAttributes()]),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the SupplyList "updated" event.
     *
     * @param  SupplyList  $supplyList
     * @return void
     */
    public function updated(SupplyList $supplyList): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();
        $changes = [];
        foreach ($supplyList->getChanges() as $field => $newValue) {
            $originalValue = $supplyList->getOriginal($field);
            $changes[$field] = ['old' => $originalValue, 'new' => $newValue];
        }

        Audit::create([
            'description' => "Lista de abastecimento atualizada",
            'table_name'  => $supplyList->getTable(),
            'record_id'   => $supplyList->id,
            'action'      => 'update',
            'changes'     => json_encode($changes),
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the SupplyList "deleted" event.
     *
     * @param  SupplyList  $supplyList
     * @return void
     */
    public function deleted(SupplyList $supplyList): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Lista de abastecimento removida",
            'table_name'  => $supplyList->getTable(),
            'record_id'   => $supplyList->id,
            'action'      => 'delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the SupplyList "restored" event.
     *
     * @param  SupplyList  $supplyList
     * @return void
     */
    public function restored(SupplyList $supplyList): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Lista de abastecimento restaurada",
            'table_name'  => $supplyList->getTable(),
            'record_id'   => $supplyList->id,
            'action'      => 'restore',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }

    /**
     * Handle the SupplyList "force deleted" event.
     *
     * @param  SupplyList  $supplyList
     * @return void
     */
    public function forceDeleted(SupplyList $supplyList): void
    {
        $user = auth()->user() ?? auth()->guard('api')->user();

        Audit::create([
            'description' => "Lista de abastecimento excluída permanentemente",
            'table_name'  => $supplyList->getTable(),
            'record_id'   => $supplyList->id,
            'action'      => 'force_delete',
            'changes'     => null,
            'user_id'     => $user?->id,
        ]);
    }
}
