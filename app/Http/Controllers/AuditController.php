<?php

namespace App\Http\Controllers;

use App\Entities\Audit;
use App\Helper\Helper;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class AuditController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $validated = $request->validate([
            'q' => ['nullable', 'string'],
            'table_name' => ['nullable', 'string'],
            'user_id' => ['nullable', 'integer'],
            'action' => ['nullable', 'string'],
            'date_from' => ['nullable', 'date'],
            'date_to' => ['nullable', 'date'],
        ]);

        $auditsQuery = Audit::query()
            ->whereLike('user.company_id', auth()->user()->company_id)
            ->when($validated['q'] ?? null, fn($query, $q) => $query->where('record_id', $q))
            ->when($validated['table_name'] ?? null, fn($query, $table) => $query->where('table_name', $table))
            ->when($validated['user_id'] ?? null, fn($query, $userId) => $query->where('user_id', $userId))
            ->when($validated['action'] ?? null, fn($query, $action) => $query->where('action', $action))
            ->when($validated['date_from'] ?? null, fn($query, $dateFrom) => $query->whereDate('created_at', '>=', $dateFrom))
            ->when($validated['date_to'] ?? null, fn($query, $dateTo) => $query->whereDate('created_at', '<=', $dateTo))
            ->latest();

        $audits = $auditsQuery->paginate(10);

        return view('audits.index', [
            'audits' => $audits,
            'tableNames' => Audit::distinct()->orderBy('table_name')->pluck('table_name'),
            'actions' => Helper::getTranslatedActions(),
            'users' => User::where('company_id', auth()->user()->company_id)->orderBy('name')->get(),
        ]);
    }

}
