<?php


namespace App\Http\Controllers;


use App\Entities\Filial\FilialBlindNote;
use App\Entities\Filial\FilialNote;
use App\Entities\Filial\FilialRecount;
use App\Events\AtualizaNotaCegaEvent;
use App\Helper\Helper;
use App\Jobs\ImportFilialBlindNotesJob;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class FilialRecountController
{
    public function index(Request $request): View|Factory|Application
    {
        $validated = $request->validate([
            'status' => ['nullable', 'string'],
            'q' => ['nullable', 'string']
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $checksQuery = FilialRecount::with('entryReport')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->withCount('products')
            ->when($status, fn($query) => $query->where('status', $status))
            ->when($search, fn($query) => $query->search($search, ['id', 'status', 'user.name', 'notes.nfe', 'notes.name_provider']))
            ->orderBy('status', 'asc')
            ->orderByDesc('id');

        $checks = $checksQuery->paginate();

        return view('filial_recount.index', compact('checks'));
    }

    public function edit($id)
    {
        return FilialRecount::findOrFail($id);
    }

    public function import(): array
    {
        try {
            ImportFilialBlindNotesJob::dispatch()->onQueue('import');
            return Helper::getResponse('success', 'Importação assíncrona em andamento....', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Erro ao importar!', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function noteIndex(Request $request): View|Factory|Application
    {
        $validated = $request->validate([
            'status' => ['nullable', 'string'],
            'q' => ['nullable', 'string']
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $notesQuery = FilialNote::with(['recount'])
            ->where('company_id', auth()->user()->company_id)
            ->withCount('items')
            ->when($status, function ($query, $status) {
                $query->where(function ($q) use ($status) {
                    $q->when($status === FilialRecount::STATUS_PENDING, function ($q) {
                        $q->whereNull('recount_id')
                            ->orWhereHas('recount', fn($subQ) => $subQ->where('status', FilialRecount::STATUS_PENDING));
                    }, function ($q) use ($status) {
                        $q->whereHas('recount', fn($subQ) => $subQ->where('status', $status));
                    });
                });
            })
            ->when($search, fn($query) => $query->search($search, ['nfe', 'name_provider', 'moviment_order']))
            ->orderByDesc('id');

        $notes = $notesQuery->paginate();

        return view('filial_note.index', compact('notes'));
    }

    public function noteItems($id)
    {
        $note = FilialNote::findOrFail($id);

        return $note->getItems();
    }

    public function noteDestroy($id): array
    {
        try {
            $note = FilialNote::findOrFail($id);

            DB::transaction(function () use ($note) {
                $note->items()->delete();
                $note->delete();

                $check = FilialRecount::find($note->recount_id);
                if ($check) {
                    $check->delete();
                }
            });
            return Helper::getResponse('success', 'Ação realizada com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $exception) {
            return Helper::getResponse('error', 'Não é possível realizar ação!', $exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id): array
    {
        $recount = FilialRecount::findOrFail($id);

        try {
            $recount->items->each(function ($item) {
                $item->delete();
            });

            $recount->notes->each(function ($note) {
                $note->recount_id = null;
                $note->save();
            });

            event(new AtualizaNotaCegaEvent($recount->notes, FilialBlindNote::class, FilialBlindNote::STATUS_NAO_CONFERIDO));

            if (isset($recount->entryReport)) $recount->entryReport->delete();

            DB::table('filial_recount_users')->where('recount_id', $recount->id)->delete();

            $recount->delete();

            return Helper::getResponse('success', 'Ação realizada com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $exception) {
            return Helper::getResponse('error', 'Não é possível realizar ação!', $exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


}
