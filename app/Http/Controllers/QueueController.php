<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class QueueController extends Controller
{
    public function checkForUpdates(): JsonResponse
    {
        $lastUpdateTimestamp = request('last_update') ?? 0;
        $currentTimestamp = now()->timestamp;

        // Contar filas específicas
        $import = Queue::size('import');
        $locals = Queue::size('locals');
        $default = Queue::size('default');

        // Formatar os dados
        $queues = [
            ['queue' => 'Importação de notas', 'count' => $import],
            ['queue' => 'Locais de estoque', 'count' => $locals],
            ['queue' => 'Fila default', 'count' => $default],
        ];

        // Gerar um hash para comparar mudanças
        $currentHash = md5(json_encode($queues));

        // Comparar com o último hash
        $updated = Cache::get('queue_last_hash') !== $currentHash;

        if ($updated) {
            Cache::put('queue_last_hash', $currentHash);
        }

        return response()->json([
            'updated' => $updated,
            'data' => $queues,
            'timestamp' => $currentTimestamp,
        ]);
    }
}
