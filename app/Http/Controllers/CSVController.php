<?php

namespace App\Http\Controllers;

use App\Entities\Filial\FilialRecountProducts;
use App\Entities\RecountProducts;
use App\Entities\SupplyList;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CSVController extends Controller
{

    public function exportRecount($type, $id): ?StreamedResponse
    {
        switch ($type) {
            case 'recount':
                $products = RecountProducts::where('recount_id', $id)->get();
                break;
            case 'filial-recount':
                $products = FilialRecountProducts::where('recount_id', $id)->get();
                break;
            default :
                return abort(500);
        }

        $columns = ['Codigo', 'Saida', 'Quantidade', 'Custo'];
        $rows = [];

        if (!empty($products)) {
            foreach ($products as $key => $recountProduct) {
                $rows[$key] = [
                    $recountProduct->cod_mat ?? '',
                    $recountProduct->unity ?? '',
                    $recountProduct->quantity ?? 0,
                    number_format(round($recountProduct->cost, 2), 2, ',', '')
                ];
            }
        }

        return $this->toCsv($columns, $rows, $type, $id);
    }

    public function toCsv($columns, $rows, $type, $id = null): StreamedResponse
    {
        $fileName = ("Conferência " . $id ?? "Arquivo") . '.csv';

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function () use ($rows, $columns) {
            $file = fopen('php://output', 'w');
            //put header in file
            fputcsv($file, $columns, ";");

            foreach ($rows as $row) {
                //put row in file
                fputcsv($file, $row, ";");
            }
            fclose($file);
        };

        return $type == "supply" ? response()->stream($callback, 200, $headers) : response()->streamDownload($callback, $fileName, $headers);
    }

    public function exportList(Request $request): StreamedResponse
    {
        $ids = $request->input('data');

        $lists = SupplyList::whereIn('id', $ids)->get();

        $columns = ['Codigo', 'Saida', 'Quantidade', 'Custo'];

        $outputs[] = $lists->map(function ($list) {
            $outputRows = [];
            foreach ($list->products as $supplyProduct) {
                $outputRows[] = array(
                    $supplyProduct->product->cod_mat ?? '',
                    $supplyProduct->product->unidade_saida ?? '',
                    $supplyProduct->quantity ?? 0,
                    number_format(round($supplyProduct->product->cost, 2), 2, ',', '')
                );
            }
            return $outputRows;
        });

        foreach ($outputs as $arr_uni) {
            foreach ($arr_uni as $values) {
                foreach ($values as $value) {
                    $rows[] = $value;
                }
            }
        }

        return $this->toCsv($columns, $rows ?? [], "supply", null);
    }

}
