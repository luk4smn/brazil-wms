<?php

namespace App\Http\Controllers;

use App\Entities\EntryReport;
use App\Entities\EntryReportItems;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class EntryReportController extends Controller
{

    public function index($type, Request $request): View|Factory|Application
    {
        $validated = $request->merge(['type' => $type])->validate([
            'type'   => ['required', 'integer'],
            'status' => ['nullable', 'string'],
            'q'      => ['nullable', 'string']
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $entryReportsQuery = EntryReport::with('related')
            ->withCount('items')
            ->where('type', $type)
            ->whereLike(['related.user.company_id'], auth()->user()->company_id)
            ->when($status, fn($query) => $query->where('status', $status))
            ->when($search, fn($query) => $query->search($search, ['id', 'status', 'related.user.name', 'related.notes.nfe', 'related.notes.name_provider']))
            ->orderBy('status', 'asc')
            ->orderByDesc('id');

        $entryReports = $entryReportsQuery->paginate();

        return view("entry_report.{$type}.index", compact('entryReports'));
    }

    public function store($reference_id, $table, Request $request): array
    {
        try{
            DB::transaction(function () use ($reference_id, $table, $request) {
                $class = EntryReport::getRelatedClass($table);

                $entryReport = EntryReport::updateOrCreate(
                    ['reference_id' => $reference_id, 'table' => $class],
                    ['status' => EntryReport::STATUS_PENDING, 'type' => EntryReport::TYPE_ESTOQUE]
                );

                $recount = $entryReport->related;

                $recount->products->each(function ($product) use ($entryReport) {
                    $quantity = is_null($product->total_quantity_confer) || $product->total_quantity_confer === "0" ? 0 : (int) $product->total_quantity_confer;
                    $issueFlag = $quantity <= 0;

                    $entryReport->items()->updateOrCreate(
                        ['cod_mat' => $product->cod_mat, 'entry_report_id' => $entryReport->id],
                        [
                            'status' => EntryReportItems::STATUS_NOT_FOUND,
                            'cod_aux' => $product->cod_aux,
                            'separator_id' => null,
                            'receiver_id' => null,
                            'issue_flag' => $issueFlag
                        ]
                    );
                });
            });
            return Helper::getResponse('success', 'Criado com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id): array
    {
        try {
            $entryReport = EntryReport::findOrFail($id);
            $entryReport->items->each(function ($item) {
                $item->delete();
            });

            $entryReport->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function rollback($id): array
    {
        try {
            $entryReport = EntryReport::findOrFail($id);
            $entryReport->rollbackEntryReportByType();
            return Helper::getResponse('success', 'Alterado com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


}
