<?php


namespace App\Http\Controllers;


use App\Entities\SupplyList;
use App\Entities\SupplyListProducts;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SupplyController extends Controller
{

    public function index(Request $request): View|Factory|Application
    {
        // Validação dos parâmetros de entrada
        $validated = $request->validate([
            'status' => ['nullable', 'string'],
            'q' => ['nullable', 'string']
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $supplyListsQuery = SupplyList::withCount('products')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->when($status, fn($query) => $query->where('status', $status))
            ->when($search, fn($query) => $query->search($search, ['id', 'type', 'local', 'status', 'user_id', 'user.name', 'separator.name']));

        $supplyLists = $supplyListsQuery
            ->orderBy('status', 'asc')
            ->orderByDesc('id')
            ->paginate();

        return view('supply.index', compact('supplyLists'));
    }

    public function status(): array
    {
        return SupplyList::getStatus();
    }

    public function edit($id)
    {
        $supplylist = SupplyList::findOrFail($id);

        $supplylist->initiated_at_conv = $supplylist->getCustomTimestamp($supplylist->initiated_at);
        $supplylist->completed_at_conv = $supplylist->getCustomTimestamp($supplylist->completed_at);

        return $supplylist;
    }

    public function items($id)
    {
        $supplyList = SupplyList::findOrFail($id);

        return $supplyList->items;
    }

    public function update($id, Request $request): RedirectResponse
    {
        $supplyList = SupplyList::findOrfail($id);

        $request->validate([
            'status' => 'required',
            'separator_id' => 'required',
        ]);

        $supplyList->update($request->all());

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return back()->withInput();
    }

    public function multiList(Request $request): ?array
    {
        $ids = $request->input('data');

        $lists = SupplyList::selectRaw('user_id, local')
            ->whereIn('id', $ids)->get();

        $arrayLists = array_merge(['lists' => $lists->toArray()], ['ids' => collect($ids)->implode(' ,')]);

        $items = SupplyListProducts::multiListCollectProductsByLocal(collect($ids));

        $data = json_encode(array_merge($arrayLists, ['items' => $items]));

        return PDFController::staticDownload('multi_supply_list', 'multisupply', $data);
    }

    public function destroy($id): array
    {
        try {
            $supplyList = SupplyList::findOrFail($id);
            $supplyList->items->each(function ($item) {
                $item->forceDelete();
            });

            $supplyList->forceDelete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
