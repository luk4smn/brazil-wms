<?php


namespace App\Http\Controllers\Api;


use App\Entities\Filial\FilialBlindNote;
use App\Entities\Filial\FilialNote;
use App\Entities\Filial\FilialRecount;
use App\Entities\Filial\FilialRecountItems;
use App\Events\AtualizaNotaCegaEvent;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class FilialRecountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.api.custom');
    }

    public function index(Request $request): JsonResponse
    {
        try {
            $validated = $request->validate([
                'status' => 'nullable|int|min:1|max:3',
                'search' => 'nullable|string',
            ]);

            $user = auth()->guard('api')->user();
            $status = $validated['status'] ?? null;
            $search = $validated['search'] ?? null;

            // Query principal com filtros aplicados
            $queryNotas = FilialNote::with(['recount'])
                ->withCount('items')
                ->where(function ($query) use ($status) {
                    if ($status == FilialRecount::STATUS_PENDING) {
                        $query->whereNull('recount_id')
                            ->orWhereHas('recount', function ($query) {
                                $query->where('status', FilialRecount::STATUS_PENDING);
                            });
                    } else if ($status) {
                        $query->whereHas('recount', function ($query) use ($status) {
                            $query->where('status', $status);
                        });
                    }
                });

            if ($search) {
                $queryNotas->where(function ($query) use ($search) {
                    $query->whereLike(['name_provider'], $search)
                        ->orWhereIn('nfe', explode(',', $search)); // Supondo que 'nfe' são números separados por vírgula
                });
            }

            //Execução da query
            $notes = $queryNotas
                ->where('company_id', $user->company_id)
                ->orderBy('id', 'desc')
                ->take(100)
                ->get();

            // Escondendo dados desnecessários em 'recount'
            $notes->each(function ($note) {
                if($note->recount) {
                    $note->recount->is_filial = true;
                    $note->recount->makeHidden(['users', 'notes']);
                }
            });

            // Retorno da resposta
            return count($notes) ? response()->json(['notes' => $notes], Response::HTTP_OK) : response()->json([], Response::HTTP_NOT_FOUND);

        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'ids' => 'required|array|min:1|max:60|distinct|exists:filial_notes,id',
            ]);

            $user = auth()->guard('api')->user();

            $notesIds = $request->input('ids', []);

            (new FilialRecount())->withoutRelations()->create([
                'user_id' => $user->id,
                'status' => FilialRecount::STATUS_IN_PROGRESS,
            ]);

            $recount = FilialRecount::where('user_id', $user->id)->orderBy('id', 'desc')->first();

            $notes = FilialNote::whereIn('id', $notesIds)
                ->where('recount_id', null)
                ->update(['recount_id' => $recount->id]);

            event(new AtualizaNotaCegaEvent($recount->notes, FilialBlindNote::class, FilialBlindNote::STATUS_EM_CONFERENCIA));

            return response()->json(['message' => 'Conferencia criada com sucesso', 'recount_id' => $recount->id], Response::HTTP_CREATED);

        } catch (Exception|ValidationException $e){
            Log::info("FilialRecountController [create] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addItem($id, Request $request): JsonResponse
    {
        $request->merge(['recount_id' => $id]);

        try {
            $request->validate([
                'recount_id'        => 'required|exists:filial_recounts,id',
                'cod_mat'           => 'required|numeric|min:0',
                'volumes'           => 'required|numeric',
                'quantity_confer'   => 'required|numeric',
                'quantity_damage'   => 'required|numeric',
            ]);

            $user = auth()->guard('api')->user();

            $recount = FilialRecount::findOrFail($request->input('recount_id', null));

            if ($recount->status != FilialRecount::STATUS_IN_PROGRESS){
                throw new Exception("Conferência não está em andamento");
            }

            $item = FilialRecountItems::create([
                'recount_id'      => $request->input('recount_id', null),
                'cod_mat'         => $request->input('cod_mat', null),
                'volumes'         => $request->input('volumes', 0),
                'quantity_confer' => $request->input('quantity_confer', 0),
                'quantity_damage' => $request->input('quantity_damage', 0),
                'user_id'         => $user->id
            ]);

            $recount->users()->syncWithoutDetaching([
                $user->id => ['recount_id' => $recount->id]
            ]);

            return response()->json(['message' => 'Item adicionado com sucesso', 'id' => $item->id], Response::HTTP_CREATED);
        } catch (Exception|ValidationException $e){
            Log::info("FilialRecountController [addItem] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function viewItems($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['recount_id' => $id]);

            $request->validate([
                'recount_id' => 'required|integer|exists:filial_recounts,id',
            ]);

            $user = auth()->guard('api')->user();

            $recount = FilialRecount::with('products')->findOrFail($id);
            $sortedProducts = $recount->products->sortBy('description')->values();

            $formattedProducts = $sortedProducts->transform(function ($item) {
                return $item->getFormattedResponse();
            });

            return response()->json(['id' => $recount->id, 'products' => $formattedProducts,], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function finalize($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['recount_id' => $id]);

            $request->validate([
                'recount_id' => 'required|integer|exists:filial_recounts,id',
            ]);

            $user = auth()->guard('api')->user();

            $recount = FilialRecount::findOrFail($request->input('recount_id', null));

            if ($recount->status != FilialRecount::STATUS_IN_PROGRESS){
                throw new Exception("Conferência não está em andamento");
            }

            $recount->status = FilialRecount::STATUS_COMPLETED;
            $recount->save();

            $recount->users()->syncWithoutDetaching([
                $user->id => ['recount_id' => $recount->id]
            ]);

            event(new AtualizaNotaCegaEvent($recount->notes, FilialBlindNote::class, FilialBlindNote::STATUS_CONFERIDO));

            return response()->json(['message' => 'Conferência finalizada com sucesso!'], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            Log::info("FilialRecountController [finalize] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

}
