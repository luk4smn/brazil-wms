<?php


namespace App\Http\Controllers\Api;


use App\Entities\Audit;
use App\Entities\Product;
use App\Http\Controllers\Controller;
use App\Jobs\RemoveLocalJob;
use App\Jobs\TransferLocalJob;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.api.custom');
    }

    public function search(Request $request): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            $code = trim($request->query('q'));
            $local = trim($request->query('l'));

            if($code){
                if (strlen($code) <= 10) {
                    $product = Product::where('cod_aux', $code)->first();
                } else {
                    $product = Product::whereHas('productCodeBar', function($query) use ($code) {
                        $query->where('cod_bar', $code);
                    })->first();
                }

                if ($product) return response()->json($product->getFormattedResponse(), Response::HTTP_OK);
            }

            if($local && strlen($local) >= 3){
                $field = Product::getLocalFieldName($user->company_id);
                $products = Product::whereLike($field, $local)
                    ->orderBy('descricao', 'asc')
                    ->get()
                    ->filter(function ($product) use ($local, $field) {
                        return preg_match("/\b$local\b/", $product->$field); // Regex para buscar correspondências exatas
                    })
                    ->values()
                    ->transform(function($product) use ($field) {
                        return $product->getFormattedResponse();
                    });

                if (count($products ?? [])) return response()->json(['products' => $products,], Response::HTTP_OK);
            }

            return response()->json(['error' => ['message' => 'Produto não encontrado']], Response::HTTP_NOT_FOUND);

        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addLocal(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'cod_mat' => 'required|string',
                'local'   => 'required|string|min:1',
            ]);

            $user = auth()->guard('api')->user();
            $codMat = $request->input('cod_mat');
            $localRequest = $request->input('local', null);
            $viewField = Product::getLocalFieldName($user->company_id);
            $realField = Product::getRealFieldName($user->company_id);

            $product = Product::where('cod_mat', $codMat)->firstOrFail();
            $locals = str_replace(' ', '', ($product[$viewField].', '.$localRequest));
            $locals = explode(",", $locals);

            // Remove duplicados, valores inválidos e ordena
            $locals = array_filter(array_unique($locals), function ($local) {
                return !empty(trim($local));
            });
            asort($locals);

            if(count($locals) > 0){
                foreach ($locals as $key => $local){
                    if($local == ',' || $local == ', '|| $local == ' ,'|| $local == "" || $local == " ")
                        unset($locals[$key]);
                }

                $finalString = implode(", ", $locals);

                // Verifica o comprimento antes do update
                if (strlen($finalString) > 100) {
                    throw new Exception("Erro: número de caracteres excedido");
                }

                $sql = "UPDATE Brazil_Inform.dbo.CADMATERIAL SET {$realField}='{$finalString}' WHERE CODIGOMATERIAL='{$codMat}'";
                DB::connection('sqlsrv')->statement($sql);

                Audit::create([
                    'description' => "Local de estoque adicionado",
                    'table_name'  => $product->getTable(),
                    'record_id'   => $product['cod_aux'],
                    'action'      => 'create',
                    'changes'     => json_encode(['old' => $product[$viewField], 'new' => $finalString]),
                    'user_id'     => $user?->id,
                ]);
            }

            return response()->json(['message' => "Local adicionado com sucesso"], Response::HTTP_CREATED);

        } catch (Exception|ValidationException $e){
            Log::info("ProductController [addLocal] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function removeLocal(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'cod_mat' => 'string|nullable',
                'local'   => 'required|string|min:1',
            ]);

            $user = auth()->guard('api')->user();
            $codMat = $request->input('cod_mat', null);
            $localRequest = $request->input('local');
            $viewField = Product::getLocalFieldName($user->company_id);
            $realField = Product::getRealFieldName($user->company_id);

            if (!$codMat) {
                RemoveLocalJob::dispatch($codMat, $localRequest, $realField, $viewField, $user)->onQueue('locals');
            } else {
                $product = Product::where('cod_mat', $codMat)->first();
                if ($product) {
                    RemoveLocalJob::updateRealField($localRequest, $product, $realField, $viewField, $user);
                }
            }

            return response()->json(['message' => "Remoção solicitada \nAguardando processamento"], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            Log::info("ProductController [removeLocal] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function transferLocal(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'old_local' => 'string|required',
                'new_local' => 'string|required',
            ]);

            $user = auth()->guard('api')->user();

            $oldLocal = $request->input('old_local');
            $newLocal = $request->input('new_local');
            $viewField = Product::getLocalFieldName($user->company_id);
            $realField = Product::getRealFieldName($user->company_id);

            TransferLocalJob::dispatch($oldLocal, $newLocal, $realField, $viewField, $user)->onQueue('locals');

            return response()->json(['message' => "Transferência solicitada \nAguardando processamento"], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            Log::info("ProductController [transferLocal] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

}
