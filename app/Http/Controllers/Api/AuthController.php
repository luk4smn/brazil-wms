<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.api.custom')->except(["login"]);
    }

    public function login(Request $request): JsonResponse
    {
        try {
            $this->validate($request, [
                'username'    => 'required|string',
                'password' => 'required|string',
            ]);

            $credentials = $request->only('username', 'password');

            $user = User::where('local_login', $credentials['username'])
                ->where('local_password', $credentials['password'])
                ->where('is_active', true)
                ->first();

            if (!$user) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized',
                ], Response::HTTP_UNAUTHORIZED);
            }

            $user->company;
            $token = auth()->guard('api')->login($user);

            return response()->json([
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function logout(): JsonResponse
    {
        try {
            auth()->guard('api')->logout();
            return response()->json(['message' => 'Desconectado com sucesso'], Response::HTTP_OK);
        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }

    }

    public function refresh(): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            auth()->guard('api')->logout();
            $token = auth()->guard('api')->login($user);
            $user->company;

            return response()->json([
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ], Response::HTTP_OK);
        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function userData(): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            $request = Request::createFromGlobals();
            $token = $request->bearerToken();
            $user->company;

            return response()->json([
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ], Response::HTTP_OK);
        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }
}
