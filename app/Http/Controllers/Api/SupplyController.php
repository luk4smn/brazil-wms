<?php


namespace App\Http\Controllers\Api;


use App\Entities\SupplyList;
use App\Entities\SupplyListItems;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class SupplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.api.custom');
    }

    public function index(Request $request): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            $supplyLists = $user->supplyLists
                ->sortByDesc('status')
                ->sortByDesc('id')
                ->take(5)
                ->each(function ($supplyList) {
                    $supplyList->status_name = $supplyList->status_label;
                    $supplyList->count_items = $supplyList->products->count();
                    $supplyList->unsetRelation('products');
                });

            return count($supplyLists) ? response()->json(['supplyLists' => $supplyLists,], Response::HTTP_OK)
                : response()->json([], Response::HTTP_NOT_FOUND);

        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function check(Request $request): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            $supplyList = $user->supplyLists->where('status', SupplyList::STATUS_TYPING)->first();
            return $supplyList ? response()->json($supplyList, Response::HTTP_OK) : response()->json([], Response::HTTP_NOT_FOUND);
        } catch (Exception $e){
            return response()->json(['error' => ['message' => $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'local' => 'required|string',
                'type'  => 'required|string',
            ]);

            $user = auth()->guard('api')->user();
            $supplyList = $user->supplyLists->where('status', SupplyList::STATUS_TYPING)->first();

            if(!$supplyList){
                $supplyList = $user->supplyLists()->create(array_merge($request->all(), ['status' => SupplyList::STATUS_TYPING]));
                return response()->json($supplyList->getFormattedResponse(), Response::HTTP_CREATED);
            } else {
                return response()->json([], Response::HTTP_CONFLICT);
            }
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addItem($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['supply_list_id' => $id]);

            $request->validate([
                'quantity'  => 'required|integer|min:1',
                'cod_aux'   => 'required|string',
                'supply_list_id' => 'required|integer|exists:supply_list,id',
            ]);

            $user = auth()->guard('api')->user();
            $supplyList = SupplyList::find($id);
            if($supplyList->type == SupplyList::TYPE_1 && $supplyList->products->count() >= 30){
                throw new Exception("Limite de itens atingido");
            }

            $item = SupplyListItems::create($request->all());
            return response()->json(['message' => 'Item adicionado'], Response::HTTP_CREATED);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function viewItems($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['supply_list_id' => $id]);

            $request->validate([
                'supply_list_id' => 'required|integer|exists:supply_list,id',
            ]);

            $user = auth()->guard('api')->user();
            $supplyList = $user->supplyLists()->with('products.product')->findOrFail($id);

            $formattedProducts = $supplyList->products->transform(function ($item) {
                return [
                    'id'                  => $item->last_insert_id,
                    'supply_list_id'      => $item->supply_list_id,
                    'cod_aux'             => $item->cod_aux,
                    'quantity'            => $item->quantity,
                    'reference'           => $item->product->referencia ?? null,
                    'product_description' => $item->product->descricao ?? null,
                    'unity'               => $item->product->unidade_saida ?? null,
                ];
            })->sortByDesc('id')->values();

            return response()->json(['supply_list_id' => $supplyList->id, 'supply_list_products' => $formattedProducts,], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function removeItem($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['supply_list_id' => $id]);

            $request->validate([
                'supply_list_id' => 'required|integer|exists:supply_list,id',
                'cod_aux'        => 'required|string',
            ]);

            $user = auth()->guard('api')->user();
            $supplyList = $user->supplyLists()->with('products.product')->findOrFail($id);

            if($supplyList->status == !SupplyList::STATUS_TYPING){
                throw new Exception("Lista não está em andamento");
            }

            $supplyList->items->where('cod_aux', $request->input('cod_aux', null))->each(function ($item) {
                $item->delete();
            });

            return response()->json(['message' => 'Item removido'], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function finalize(int $id, Request $request): JsonResponse
    {
        try {
            $user = auth()->guard('api')->user();
            $supplyList = SupplyList::with('items')->findOrFail($id);

            if ($supplyList->items->isNotEmpty()) {
                $supplyList->update(['status' => SupplyList::STATUS_PENDING]);
            } else {
                $supplyList->forceDelete();
            }

            return response()->json(['message' => 'Finalizado com sucesso'], Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => ['message' => 'Lista de Abastecimento não encontrada']], Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }


}
