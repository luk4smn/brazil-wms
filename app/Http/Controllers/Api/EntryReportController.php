<?php


namespace App\Http\Controllers\Api;


use App\Entities\EntryReport;
use App\Entities\EntryReportItems;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class EntryReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.api.custom');
    }

    public function index(Request $request): JsonResponse
    {
        try {
            $validated = $request->validate([
                'type'   => 'required|int|min:1|max:2',
                'status' => 'required|string',
                'search' => 'nullable|string',
            ]);

            $user = auth()->guard('api')->user();
            $type = $validated['type'] ?? null;
            $status = explode(',', $validated['status'] ?? null);
            $search = $validated['search'] ?? null;

            // Query principal com filtros aplicados
            $queryEntryReport = EntryReport::with(['related'])
                ->withCount('itemsNotIssued as items_count')
                //->where('type', $type)
                ->whereIn('status', $status);

            if ($search) {
                $queryEntryReport->where(function ($query) use ($search) {
                    $query->whereLike(['related.notes.name_provider'], $search)
                        ->orWhere(function ($query) use ($search) {
                            $query->whereLike(['related.notes.nfe'], $search);
                        });
                });
            }

            //Execução da query
            $entryReports = $queryEntryReport
                ->whereLike(['related.user.company_id'], $user->company_id)
                ->orderBy('id', 'desc')
                ->take(20)
                ->get();

            // Escondendo dados desnecessários em 'recount'
            $entryReports->each(function ($report) {
                if ($report->related) {
                    $report->quantity_role = (int) $report->related->user->company->quantity_role ?? 0;
                    $report->related->makeHidden(['user', 'users', 'notes']);
                }
            });

            // Retorno da resposta
            return count($entryReports) ? response()->json(['entryReports' => $entryReports], Response::HTTP_OK) : response()->json([], Response::HTTP_NOT_FOUND);

        } catch (Exception|ValidationException $e) {
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateItem($id, Request $request): JsonResponse
    {
        $request->merge(['entry_report_id' => $id]);

        try {
            $request->validate([
                'entry_report_id'   => 'required|exists:entry_reports,id',
                'cod_mat'           => 'required|numeric|min:0',
                'local'             => 'required|string',
                'type'              => 'required|int|min:1|max:2',
                'status_item'       => 'required|int|min:1|max:5',
            ]);

            $user = auth()->guard('api')->user();

            $entryReport = EntryReport::with('items')->findOrFail($id);

            if($entryReport->type == EntryReport::TYPE_ESTOQUE && $entryReport->status == EntryReport::STATUS_FORWARD){
                ++$entryReport->type;
                $entryReport->save();
            }

            if ($entryReport->type != $request->get('type')) {
                throw new Exception("Ação não permitida");
            }

            $item = EntryReportItems::where('cod_mat', $request->get('cod_mat'))
                ->where('entry_report_id', $request->get('entry_report_id'))
                ->whereNull($entryReport->type_column)
                ->first();

            if ($item) {
                $item->update([
                    $entryReport->type_column => $user->id,
                    'local' => $request->get('local'),
                    'status' => $request->get('status_item'),
                ]);

                $entryReport->users()->syncWithoutDetaching([
                    $user->id => ['entry_report_id' => $entryReport->id, 'role' => $entryReport->type_role]
                ]);

                $entryReport->updateEntryReportByType();
            }
            else {
                throw new Exception("Item não encontrado, ou já confirmado");
            }

            return response()->json(['message' => 'Item adicionado com sucesso!', 'id' => $item->id], Response::HTTP_CREATED);
        } catch (Exception|ValidationException $e){
            Log::info("EntryReportController [updateItem] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function viewItems($id, Request $request): JsonResponse
    {
        try {
            $request->merge(['entry_report_id' => $id]);

            $request->validate([
                'entry_report_id' => 'required|integer|exists:entry_reports,id',
            ]);

            $user = auth()->guard('api')->user();

            $entryReport = EntryReport::with('items')->findOrFail($id);

            $formattedProducts = $entryReport->items
                ->filter(fn($item) => !$item->issue_flag)                                     // Filtra itens sem problemas
                ->map(fn($item) => $item->product->mergeWithItem($item, 'entry_report_item')) // Merge com produto
                ->sortBy('product_local')                                                     // Ordenação por local
                ->sortBy('descricao')                                                         // Ordenação por descricao
                ->values()                                                                    // Reindexa os índices
                ->transform(fn($item) => $item->getFormattedResponse());                      // Formata o retorno

            return response()->json(['id' => $entryReport->id, 'type' => $entryReport->type, 'products' => $formattedProducts,], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

    public function finalize($id, $status, Request $request): JsonResponse
    {
        try {
            $request->merge(['entry_report_id' => $id, 'status' => $status]);

            $request->validate([
                'entry_report_id' => 'required|integer|exists:entry_reports,id',
                'status'          => 'required|integer|min:1|max:7',
            ]);

            $user = auth()->guard('api')->user();

            $entryReport = EntryReport::findOrFail($request->get('entry_report_id'));

            if (!collect([EntryReport::STATUS_IN_PROGRESS, EntryReport::STATUS_RECEIVING])->contains($entryReport->status)){
                throw new Exception("Relatório não está em andamento");
            }

            $entryReport->status = $request->get('status');
            $entryReport->finalizeEntryReportByType();

            $entryReport->users()->syncWithoutDetaching([
                $user->id => ['entry_report_id' => $entryReport->id, 'role' => $entryReport->type_role]
            ]);

            return response()->json(['message' => 'Relatório finalizado com sucesso!'], Response::HTTP_OK);
        } catch (Exception|ValidationException $e){
            Log::info("EntryReportController [finalize] - Erro ao realizar requisição : {$e->getMessage()}");
            return response()->json(['error' => ['message' => $e instanceof ValidationException ? $e->errors() : $e->getMessage(),]], Response::HTTP_BAD_REQUEST);
        }
    }

}
