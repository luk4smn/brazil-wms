<?php


namespace App\Http\Controllers;


use App\Entities\EntryReport;
use App\Entities\Filial\FilialRecount;
use App\Entities\Recount;
use App\Entities\SupplyList;
use App\Helper\Helper;
use Barryvdh\DomPDF\Facade as PDF;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PDFController extends Controller
{
    public function generate($view, Request $request): Response
    {
        $data = $this->fillData($view, $request);

        $view = str_replace("filial_", '', $view);
        $view = 'pdf.' . $view;
        $pdf = PDF::loadView($view, json_decode($data, true) ?? []);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream();
    }

    public function download($view, Request $request): ?array
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        try {
            $data = $this->fillData($view, $request);
            $view = str_replace("filial_", '', $view);
            $view = 'pdf.' . $view;
            $filename = $request['filename'] ?? 'file';

            $pdf = PDF::loadView($view, json_decode($data, true) ?? []);
            $pdf->setPaper('A4', 'landscape');
            //$pdf->setOptions(['isHtml5ParserEnabled' => true]);

            // save the generated PDF to Browser
            file_put_contents("reports/{$filename}.pdf", $pdf->output());

            return Helper::getResponse('success', 'Arquivo gerado com sucesso !', 'Recarregando em 2 segundos...', ResponseAlias::HTTP_NO_CONTENT, 'file_url', asset("reports/{$filename}.pdf"));
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Não é possível realizar ação!', $ex->getMessage(), ResponseAlias::HTTP_BAD_REQUEST);
        }
    }

    private function fillData($view, $request)
    {
        switch ($view) {
            case 'recount':
                $recount = Recount::with(['notes' => function ($query) {
                    $query->withCount('items');
                }, 'user', 'users', 'products'])->withCount(['totalNoteItems as total_notes_items_count'])->findOrFail($request->input('data') ?? null);
                return json_encode($recount->toArray());

            case 'filial_recount':
                $recount = FilialRecount::with(['notes' => function ($query) {
                    $query->withCount('items');
                }, 'user', 'users', 'products'])->withCount(['totalNoteItems as total_notes_items_count'])->findOrFail($request->input('data') ?? null);
                return json_encode($recount->toArray());

            case 'supply_list_by_local':
                $list = SupplyList::with(['products', 'user', 'separator', 'products'])->findOrFail($request->input('data') ?? null);
                return json_encode($list->toArray());

            case 'entry_report':
                $entryReport = EntryReport::with([
                    'items' => function ($query) {
                        $query->with('product', 'separator', 'receiver');
                    },
                    'related' => function ($query) {
                        $query->without('users', 'notes');
                    },
                    'users',
                    ])
                    ->withCount('items')
                    ->findOrFail($request->input('data') ?? null);

                return json_encode($entryReport->toArray());

            default:
                return $request['data'];
        }
    }

    /* -----------------------CHAMADAS ESTATICAS -----------------------------*/
    public static function staticGenerate($view, $data): Response
    {
        $view = 'pdf.' . $view;
        $pdf = PDF::loadView($view, $data ?? []);

        return $pdf->stream();
    }

    public static function staticDownload($view, $filename, $data): ?array
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $view = 'pdf.' . $view;

        try {
            $pdf = PDF::loadView($view, json_decode($data, true) ?? []);
            $pdf->setPaper('A4', 'landscape');
            //$pdf->setOptions(['isHtml5ParserEnabled' => true]);

            // save the generated PDF to Browser
            file_put_contents("reports/{$filename}.pdf", $pdf->output());

            return Helper::getResponse('success', 'Arquivo gerado com sucesso !', 'Recarregando em 2 segundos...', ResponseAlias::HTTP_NO_CONTENT, 'file_url', asset("reports/{$filename}.pdf"));
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Não é possível realizar ação!', $ex->getMessage(), ResponseAlias::HTTP_BAD_REQUEST);
        }
    }

}
