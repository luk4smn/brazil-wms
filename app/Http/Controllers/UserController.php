<?php

namespace App\Http\Controllers;

use App\Entities\Company;
use App\Helper\Helper;
use App\Http\Requests\UserRequest;
use App\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function index(Request $request, User $model): View|Factory|Application
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['name','email','local_login']);
        }

        return view('users.index', ['users' => $model->orderBy('name')->paginate()]);
    }


    public function create(): View|Factory|Application
    {
        $companies = Company::pluck('name','id');

        return view('users.create', compact('companies'));
    }

    public function store(UserRequest $request, User $model)
    {

        $request->get('admin') == 'on' ? $request['admin'] = true : $request['admin'] = false;
        $request->get('separator') == 'on' ? $request['separator'] = true : $request['separator'] = false;

        if($request->get('password')){
            $hash = Hash::make($request->get('password'));
            $request->merge(['password' => $hash]);
        }

        $model->create($request->all());

        return redirect()->route('users.index')->withStatus(__('Usuário criado com sucesso.'));
    }

    public function edit(User $user): View|Factory|Application
    {
        $companies = Company::pluck('name','id');

        return view('users.edit', compact('user','companies'));
    }

    public function show(User $user): View|Factory|Application
    {
        return view('users.show', compact('user'));
    }

    public function update(UserRequest $request, User  $user)
    {
        $excludeFromRequest = null;

        $request->get('admin') == 'on' ? $request['admin'] = true : $request['admin'] = false;
        $request->get('separator') == 'on' ? $request['separator'] = true : $request['separator'] = false;
        $request->get('password') == "" ? $excludeFromRequest = 'password' : null;

        if($request->get('password')){
            $hash = Hash::make($request->get('password'));
            $request->merge(['password' => $hash]);
        }

        $user->update(
            $request
            ->except(
                $excludeFromRequest,
                $request->get('local_password') == null ? 'local_password': null)
            );

        return redirect()->route('users.index')->withStatus(__('Usuário atualizado com sucesso.'));
    }

    public function destroy($id): array
    {
        try {
            $user = User::findOrFail($id);

            if ($user->recounts()->exists() || $user->supplyLists()->exists() || $user->filialRecounts()->exists()) {
                throw new Exception("Usuário tem outros registros associados");
            }

            $user->delete();

            return Helper::getResponse('success', 'Ação realizada com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $exception) {
            return Helper::getResponse('error', 'Não é possível realizar ação!', $exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function getSeparators()
    {
        return User::where('separator',true)
            ->where('is_active', true)
            ->orderBy('name')
            ->pluck('name', 'id');
    }

    public function toggleStatus($id): RedirectResponse
    {
        $user = User::findOrFail($id);
        $user->is_active = !$user->is_active;
        $user->save();

        return redirect()->back()->with('status', 'Status atualizado com sucesso.');
    }
}
