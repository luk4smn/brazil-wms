<?php


namespace App\Http\Controllers;


use App\Entities\BlindNote;
use App\Entities\Note;
use App\Entities\Recount;
use App\Events\AtualizaNotaCegaEvent;
use App\Helper\Helper;
use App\Jobs\ImportBlindNotesJob;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class RecountController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        // Validação dos parâmetros de entrada
        $validated = $request->validate([
            'status' => ['nullable', 'string'],
            'q' => ['nullable', 'string'],
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $checksQuery = Recount::with('entryReport')
            ->whereLike(['user.company_id'], auth()->user()->company_id)
            ->withCount('products')
            ->when($status, function ($query, $status) {
                $query->where('status', $status);
            })
            ->when($search, function ($query, $search) {
                $query->search($search, ['id', 'status', 'user.name', 'notes.nfe', 'notes.name_provider']);
            });

        $checks = $checksQuery
            ->orderBy('status', 'asc')
            ->orderByDesc('id')
            ->paginate(15);

        return view('recount.index', compact('checks'));
    }

    public function edit($id)
    {
        return Recount::findOrFail($id);
    }

    public function import(): array
    {
        try {
            ImportBlindNotesJob::dispatch()->onQueue('import');
            return Helper::getResponse('success', 'Importação assíncrona em andamento....', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Não é possível importar!', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function noteIndex(Request $request): View|Factory|Application
    {
        // Validação dos parâmetros de entrada
        $validated = $request->validate([
            'status' => ['nullable', 'string'],
            'q' => ['nullable', 'string'],
        ]);

        $status = $validated['status'] ?? null;
        $search = $validated['q'] ?? null;

        $notesQuery = Note::query()
            ->where('company_id', auth()->user()->company_id)
            ->withCount('items')
            ->when($status, function ($query, $status) {
                $query->with(['recount'])
                    ->where(function ($q) use ($status) {
                        $q->when($status === Recount::STATUS_PENDING, function ($q) {
                            $q->whereNull('recount_id')
                                ->orWhereHas('recount', fn($subQ) => $subQ->where('status', Recount::STATUS_PENDING));
                        }, function ($q) use ($status) {
                            $q->whereHas('recount', fn($subQ) => $subQ->where('status', $status));
                        });
                    });
            })
            ->when($search, function ($query, $search) {
                $query->search($search, ['id', 'nfe', 'name_provider', 'moviment_order']);
            });

        $notes = $notesQuery->orderByDesc('id')->paginate(15);

        return view('note.index', compact('notes'));
    }

    public function noteItems($id)
    {
        $note = Note::findOrFail($id);

        return $note->getItems();
    }

    public function noteDestroy($id): array
    {
        try {
            $note = Note::findOrFail($id);
            $note->items->each(function ($item) {
                $item->delete();
            });

            $check = Recount::where('id', $note->recount_id)->get()->first();
            $note->delete();

            if ($check) {
                $check->delete();
            }

            return Helper::getResponse('success', 'Nota excluída com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id): array
    {
        try {
            $recount = Recount::findOrFail($id);

            $recount->items->each(function ($item) {
                $item->delete();
            });

            $recount->notes->each(function ($note) {
                $note->recount_id = null;
                $note->save();
            });

            event(new AtualizaNotaCegaEvent($recount->notes, BlindNote::class, BlindNote::STATUS_NAO_CONFERIDO));

            if (isset($recount->entryReport)) $recount->entryReport->delete();

            DB::table('recount_users')->where('recount_id', $recount->id)->delete();

            $recount->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            return Helper::getResponse('error', 'Erro ao realizar ação', $ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

}
