<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Symfony\Component\HttpFoundation\Response;
use Closure;

class ApiAuthenticate extends Middleware
{
    protected function unauthenticated($request, array $guards): void
    {
        $token = $request->header('Authorization');
        $user = auth()->guard('api')->user();

        if (!$token || !$user) {
            abort(response()->json([
                "message" => "Usuário não autenticado"
            ], Response::HTTP_UNAUTHORIZED));
        }
    }
}
