<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

trait Searchable {

    public function scopeSearch(Builder $query, $search, array $params = []): Builder
    {
        if (empty($search)) {
            return $query;
        }

        // Tenta converter o termo de busca em uma data válida
        $parsedDate = $this->parseSearchDate($search);

        $query->where(function ($query) use ($search, $params, $parsedDate) {
            // Filtro para campos específicos
            $query->whereLike($params, $search);

            // Filtros por data, se a data for válida
            if ($parsedDate) {
                foreach (['created_at', 'input_date'] as $column) {
                    if (Schema::hasColumn($this->getTable(), $column)) {
                        $query->orWhereDate($column, '=', $parsedDate);
                    }
                }
            }
        });

        // Aplica filtro SoftDeletes, se a tabela possui a coluna `deleted_at`
        if (Schema::hasColumn($this->getTable(), 'deleted_at')) {
            $query->whereNull('deleted_at');
        }

        return $query;
    }

    /**
     * Converte o termo de busca em uma data, se possível.
     */
    protected function parseSearchDate(string $search): ?string
    {
        try {
            return Carbon::parse(str_replace('/', '-', $search))->format('Y-m-d');
        } catch (\Exception $e) {
            return null; // Retorna null se não for uma data válida
        }
    }



}
