<?php

namespace App\Providers;

use App\Entities\EntryReport;
use App\Entities\Filial\FilialRecount;
use App\Entities\Recount;
use App\Entities\SupplyList;
use App\Observers\EntryReportObserver;
use App\Observers\FilialRecountObserver;
use App\Observers\RecountObserver;
use App\Observers\SupplyListObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //------------------------------  Observers  ----------------------------------------------------------------//
        Recount::observe(RecountObserver::class);
        SupplyList::observe(SupplyListObserver::class);
        FilialRecount::observe(FilialRecountObserver::class);
        EntryReport::observe(EntryReportObserver::class);

        //------------------------------  Configurações Blade  ------------------------------------------------------//
        Blade::directive('money', function ($amount) {
            return "<?php echo 'R$ ' . number_format($amount, 2, ',', '.'); ?>";
        });

        //------------------------------  Configurações Builder (whereLike multi-camadas) ---------------------------//
        Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            $this->where(function (Builder $query) use ($attributes, $searchTerm) {
                foreach (Arr::wrap($attributes) as $attribute) {
                    $query->when(
                        str_contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $segments = explode('.', $attribute);
                            $relationName = array_shift($segments);
                            $relationAttribute = implode('.', $segments);

                            $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $searchTerm) {
                                if (str_contains($relationAttribute, '.')) {
                                    $query->whereLike($relationAttribute, $searchTerm);
                                } else {
                                    $query->where($relationAttribute, 'LIKE', "%{$searchTerm}%");
                                }
                            });
                        },
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $query->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
                        }
                    );
                }
            });

            return $this;
        });


    }
}
