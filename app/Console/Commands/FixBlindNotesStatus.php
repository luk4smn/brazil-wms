<?php

namespace App\Console\Commands;

use App\Entities\BlindNote;
use App\Entities\Filial\FilialBlindNote;
use App\Entities\Filial\FilialRecount;
use App\Entities\Recount;
use App\Events\AtualizaNotaCegaEvent;
use Exception;
use Illuminate\Console\Command;

class FixBlindNotesStatus extends Command
{

    protected $signature = 'command:fix-blind-notes-status';


    protected $description = 'Ajuste do status de processamento das notas cegas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle(): void
    {
        $this->info('Comando iniciado!');

        Recount::all()->each(function (Recount $recount) {
            $status = match ((int) $recount->status) {
                Recount::STATUS_PENDING => BlindNote::STATUS_NAO_CONFERIDO,
                Recount::STATUS_IN_PROGRESS => BlindNote::STATUS_EM_CONFERENCIA,
                Recount::STATUS_COMPLETED => BlindNote::STATUS_CONFERIDO,
                default => throw new Exception('Unexpected value'),
            };

            event(new AtualizaNotaCegaEvent($recount->notes, BlindNote::class, $status));
        });

        FilialRecount::all()->each(function (FilialRecount $recount) {
            $status = match ((int) $recount->status) {
                FilialRecount::STATUS_PENDING => FilialBlindNote::STATUS_NAO_CONFERIDO,
                FilialRecount::STATUS_IN_PROGRESS => FilialBlindNote::STATUS_EM_CONFERENCIA,
                FilialRecount::STATUS_COMPLETED => FilialBlindNote::STATUS_CONFERIDO,
                default => throw new Exception('Unexpected value'),
            };

            event(new AtualizaNotaCegaEvent($recount->notes, FilialBlindNote::class, $status));
        });

        $this->info('Ajuste finalizado com sucesso!');
    }
}
