<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('cache:clear')
            ->twiceDaily(8, 18);

        // Processar a fila "default"
        $schedule->command('queue:work --queue=default --tries=2 --max-jobs=100 --max-time=3600 --memory=512 --stop-when-empty')
            ->everyMinute()
            ->withoutOverlapping();

        // Processar a fila "locals"
        $schedule->command('queue:work --queue=locals --tries=2 --max-jobs=100 --max-time=3600 --memory=512 --stop-when-empty')
            ->everyMinute()
            ->withoutOverlapping();

        // Processar a fila "import"
        $schedule->command('queue:work --queue=import --tries=2 --max-jobs=100 --max-time=3600 --memory=512 --stop-when-empty')
            ->everyMinute()
            ->withoutOverlapping();
    }


    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
