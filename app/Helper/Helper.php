<?php

namespace App\Helper;


use App\User;

class Helper
{
    static function getDate($date): \DateTime|\DateTimeInterface
    {
        try {
            return $date instanceof \DateTimeInterface ? $date : new \DateTime($date);
        } catch (\Exception $e) {
            return new \DateTime('now');
        }
    }

    static function highlightDifferences($string1, $string2, $style): mixed
    {
        // Quebrar as strings em palavras/frases
        $words1 = explode(',', str_replace(' ', '', ($string1)));
        $words2 = explode(',', str_replace(' ', '', ($string2)));

        // Identificar diferenças
        $diff = array_diff($words1, $words2);

        // Destacar as palavras que estão diferentes
        foreach ($diff as $word) {
            $string1 = str_replace($word, "<span style=\"$style\">$word</span>", $string1);
        }

        return $string1;
    }

    public static function getRecountUsersNames($item): array
    {
        if (empty($item['user_ids'])) {
            return [];
        }

        // Explode a string de IDs e remove duplicatas
        $userIds = explode(', ', $item['user_ids']);
        $uniqueUserIds = array_unique($userIds);

        // Buscar os nomes dos usuários no banco
        $userNames = User::whereIn('id', $uniqueUserIds)->pluck('name')->toArray();

        // Garantir que os nomes sejam únicos e ordenados
        $uniqueUserNames = array_unique($userNames);
        sort($uniqueUserNames);

        return $uniqueUserNames;
    }

    public static function formatString(?string $value): string
    {
        return trim($value ?? '');
    }

    public static function formatFloat(?float $value): float
    {
        return $value ? round($value, 2) : 0;
    }

    public static function formatArray($relation, string $column): array
    {
        return $relation?->where('status', 'A')->pluck($column)->toArray() ?? [];
    }

    public static function getTranslatedTableName($tableName): string
    {
        $translations = [
            'product' => 'Produtos',
            'entry_reports' => 'Relatório de Entrada',
            'filial_recounts' => 'Conferências de Filial',
            'recounts' => ' Conferências de fornecedor',
            'supply_list' => 'Lista de Abastecimento'
        ];

        return $translations[$tableName] ?? $tableName;
    }

    public static function getTranslatedActions(): array
    {
        return [
            'create' => 'Criar',
            'update' => 'Atualizar',
            'delete' => 'Excluir',
            'restore' => 'Restaurar',
            'force_delete' => 'Excluir Permanentemente',
        ];
    }

    public static function getResponse(string $status, string $message, string $info, int $code, $optionalKey = null, $optionalValue = null): array
    {
        return [
            'data' => [
                'status'  => $status,
                'message' => $message,
                'info'    => $info,
                'code'    => $code,
                "{$optionalKey}" => $optionalValue,
            ]
        ];
    }

}
