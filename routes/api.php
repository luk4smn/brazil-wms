<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::get('/', function (){
        return response()->json(['status' => 'success', 'message' => 'hi']);
    });

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user-data', 'AuthController@userData');

    Route::get('supply-lists', 'SupplyController@index');
    Route::get('supply-lists/check', 'SupplyController@check');
    Route::post('supply-lists/create', 'SupplyController@create');
    Route::get('supply-lists/{id}/view-items', 'SupplyController@viewItems');
    Route::post('supply-lists/{id}/add-item', 'SupplyController@addItem');
    Route::post('supply-lists/{id}/remove-item', 'SupplyController@removeItem');
    Route::post('supply-lists/{id}/finalize', 'SupplyController@finalize');

    Route::get('products/search', 'ProductController@search');
    Route::post('products/add-local', 'ProductController@addLocal');
    Route::post('products/remove-local', 'ProductController@removeLocal');
    Route::post('products/transfer-local', 'ProductController@transferLocal');

    Route::get('recounts/notes', 'RecountController@index');
    Route::post('recounts/create', 'RecountController@create');
    Route::post('recounts/{id}/add-item', 'RecountController@addItem');
    Route::get('recounts/{id}/view-items', 'RecountController@viewItems');
    Route::post('recounts/{id}/finalize', 'RecountController@finalize');

    Route::get('filial-recounts/notes', 'FilialRecountController@index');
    Route::post('filial-recounts/create', 'FilialRecountController@create');
    Route::post('filial-recounts/{id}/add-item', 'FilialRecountController@addItem');
    Route::get('filial-recounts/{id}/view-items', 'FilialRecountController@viewItems');
    Route::post('filial-recounts/{id}/finalize', 'FilialRecountController@finalize');

    Route::get('entry-reports', 'EntryReportController@index');
    Route::post('entry-reports/{id}/update-item', 'EntryReportController@updateItem');
    Route::get('entry-reports/{id}/view-items', 'EntryReportController@viewItems');
    Route::post('entry-reports/{id}/{status}/finalize', 'EntryReportController@finalize');
});
