<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/',  url('/home'));

Route::group(['middleware' => ['auth']], function () {
    // HOME ROUTE
    Route::get('/home', 'HomeController@index')->name('home');

    //SUPPLY ROUTE
    Route::get('/supply/status', 'SupplyController@status')->name('supply.status');
    Route::post('/supply/multi-list', 'SupplyController@multiList')->name('supply.multi');

    //PROFILE ROUTES
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    //USERS ROUTES
    Route::get('/users/separators', 'UserController@getSeparators')->name('users.separators');
    Route::post('/users/{id}/toggle-status', 'UserController@toggleStatus')->name('users.toggleStatus');

    //PDF ROUTES
    Route::post('/pdf/{view}/', 'PDFController@generate')->name('pdf.generate');
    Route::post('/pdf/download/{view}', 'PDFController@download')->name('pdf.download');

    //CSV ROUTES
    Route::get('/csv/{type}/{id}/export', 'CSVController@exportRecount')->name('csv.export-recount');
    Route::post('/csv/list/export', 'CSVController@exportList')->name('csv.export-list');

    //NOTES ROUTES
    Route::get('/notes/', 'RecountController@noteIndex')->name('notes.index');
    Route::get('/notes/import', 'RecountController@import')->name('notes.import');
    Route::get('/notes/{id}/items/', 'RecountController@noteItems')->name('notes.items');
    Route::delete('/notes/{id}/destroy/', 'RecountController@noteDestroy')->name('notes.destroy');

    //FILIAL NOTES ROUTES
    Route::get('/filial-notes/', 'FilialRecountController@noteIndex')->name('filial-notes.index');
    Route::get('/filial-notes/import', 'FilialRecountController@import')->name('filial-notes.import');
    Route::get('/filial-notes/{id}/items/', 'FilialRecountController@noteItems')->name('filial-notes.items');
    Route::delete('/filial-notes/{id}/destroy/', 'FilialRecountController@noteDestroy')->name('filial-notes.destroy');

    //ENTRY REPORT ROUTES
    Route::get('/entry-reports/{type}/index', 'EntryReportController@index')->name('entry-reports.index');
    Route::post('/entry-reports/{reference_id}/{table}/store', 'EntryReportController@store')->name('entry-reports.store');
    Route::delete('/entry-reports/{id}/destroy/', 'EntryReportController@destroy')->name('entry-reports.destroy');
    Route::delete('/entry-reports/{id}/rollback/', 'EntryReportController@rollback')->name('entry-reports.rollback');

    //AUDIT ROUTES
    Route::get('/audits', 'AuditController@index')->name('audits.index');

    // CRUD ROUTES
    Route::resources([
        'supply'            => 'SupplyController' ,
        'users'      		=> 'UserController',
        'recounts'          => 'RecountController',
        'filial-recounts'   => 'FilialRecountController'
    ]);

    //QUEUES ROUTES
    Route::get('/queue/updates', 'QueueController@checkForUpdates')->name('queue.updates');

});

